#include "F28x_Project.h"
#include "AdcDriver.h"
#include "FunctionCommon.h"
#include "math.h"


extern void motor_vfcontrol();
extern Uint16 read_IsrFreq(void);
extern Uint16 get_OutFreq(void);
extern void Set_IPM_protect(void);
extern bool read_shot_IPM_fault(void);


/*******************************************************************************************
Function Name:   MtrCtrlISR
Version:         V1.0
Input:
Output:
Return:
Description:    PWM 中断程序
History:
*******************************************************************************************/
void MtrCtrl(void)
{

    static   BOOL   cal_excursion_over = FALSE;             //零漂计算是否完成
             Uint32 sum;
             Uint32 freq;
             BOOL   quik_protec_flag = FALSE;
    static unsigned long int delay_n = 0;
   // Set_IPM_protect();

    quik_protec_flag = read_shot_IPM_fault();


    //启机给与4S延时，防止零漂运算出错
    if(delay_n<60000)
    {
      delay_n++;
      return;
    }

    if(FALSE == cal_excursion_over )
    {
        cal_excursion_over = GetCurExcursion(TRUE);
    }
    else
    {
        GetUVWInfo();                                 //从ADC处获取暂态信息
        freq = get_OutFreq();                         //获取实时有效值信息

        if(freq>200)
        {
            sum = (Uint32)read_IsrFreq();             //读取中断频率
            sum *=200;                                //中断频率的200倍
            sum /=freq;                               //freq放大100倍，sum等于输出两个周期
        }
        else
        {
            sum = 2000;
        }
     //   GetRealOut();
          GetCurrentabs(sum);                           //获取2个周期的点取均方根值
     //   Getvoltageabs(sum);
    }

    if(!(runFlag.bit.run))                            //停机与启动判断
    {
        runFlag.bit.run = 0;
        PwmOff();
    }
    else if( (runFlag.bit.run)&&(TRUE==cal_excursion_over )&&(!quik_protec_flag) )
    {
        PwmOn();
        motor_vfcontrol();
    }
    else
    {
        runFlag.bit.run = 0;
        PwmOff();
    }

    EPwm1Regs.ETCLR.bit.INT = 1;
    PieCtrlRegs.PIEACK.all = PIEACK_GROUP3;


}
