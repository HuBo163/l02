/*
 * motor_protect.c
 *
 *  Created on: 2020年4月11日
 *      Author: dfs_h
 */
#include"motor_protect.h"
#include"calvalue.h"

#include"f_data_enterchange.h"
#include"inc/port.h"
#include"f_funcCode.h"
#include"f_run.h"

#define REG_ERROR_HEAD  71

extern USHORT usRegHoldingBuf[REG_HOLDING_NREGS];  //保持寄存器,modbus协议既能修改该寄存器,又能读取该寄存器

ERROE_DATA error_data;

static OVRCUR_PROTECT_STRUCT ovrcur_protect = DEFAULT_OVERCUR_PROTECT;
static LOSEPHA_PROTECT_STRUCT phase_protect = DEFAULT_LOSPHA_PROTECT;
static SHOT_PROTECT_STRUCT shot_protect = DEFAULT_SHOT_protect;

const Uint16 g_default_overcurpar[] = DEFAULT_OVERCUR_PROTECT;
const Uint16 g_default_losphapar[] = DEFAULT_LOSPHA_PROTECT;
const Uint16 g_default_shot[] = DEFAULT_SHOT_protect;

static bool gipm_flag = false;
static bool gshot_flag = false;
static bool tempre_flag = false;
static error_type gerror = no_error;

uint16  ncoff = 8560;

void  Set_protectType(Uint16 flag)
{
    switch(flag)
    {
        case 0:
            gIExcursionInfo.Coff   = 4280;
            gIExcursionInfo.Coffdc = 1400;
            ovrcur_protect.maxcur1 = 2500; // 1.2倍过载
            ovrcur_protect.maxcur2 = 3000;  //1.5倍过载
        break;
        case 1:
            gIExcursionInfo.Coff=ncoff;
            gIExcursionInfo.Coffdc=2800;
            ovrcur_protect.maxcur1 = 3000;  // 1.5倍过载
            ovrcur_protect.maxcur2 = 4000;  // 2倍过载
        break;
        default:
         break;
    }
}

error_type read_error_flag(void)
{
    return gerror;
}

//保存故障数据
void Save_errordata(void)
{
    Uint16* save_byte;
    save_byte=&(error_data.first_error);
    unsigned char length=sizeof(error_data);
    unsigned char i;

    for(i=0;i<length;i++)
    {
        usRegHoldingBuf[REG_ERROR_HEAD+i]=*save_byte++;
    }
}


void add_errordata()
{
    Uint16* data_pointer = error_data.errorScene1.all;
    Uint16  error_datasize = sizeof(struct ERROR_SCENE_STRUCT);
    Uint16  addr_new,addr_old,temp,i;

    for(i=0;i<error_datasize;i++)
    {
        addr_old = i;
        addr_new = error_datasize+i;
        *(data_pointer+addr_old) = *(data_pointer+addr_new);  //第一次的数据等于第二次的数据
        temp = addr_new;
        addr_new = 2*error_datasize+i;
        addr_old = temp;
        *(data_pointer+addr_old) = *(data_pointer+addr_new); //第二次的数据等于第三次的数据
    }

}

//获取故障数据
void get_errordata(error_type error)
{

    error_data.first_error  = error_data.second_error;
    error_data.second_error = error_data.latest_error;

    gerror = error;

    if((MOTOR_RUN==runFlag.bit.run)||(error<com_fault))
    {
        add_errordata();                     //数据顺序递增第一次故障被擦除
        error_data.latest_error = error;
        error_data.errorScene3.all[0] = Get_VfSpeed();
        error_data.errorScene3.all[1] = gIUVW.dc;
        error_data.errorScene3.all[2] = gUUVW.dc;
        error_data.errorScene3.all[3] = gRealOut.I;
        error_data.errorScene3.all[4] = gRealOut.U;
        error_data.errorScene3.all[5] = gIUVWabs.U;
        error_data.errorScene3.all[6] = gIUVWabs.V;
        error_data.errorScene3.all[7]=  gIUVWabs.W;

        Save_errordata();
    }
/*    else
    {
        error_data.errorScene3.all[1] = gIUVW.dc;
        error_data.errorScene3.all[2] = gUUVW.dc;
        error_data.errorScene3.all[3] = gRealOut.I;
        error_data.errorScene3.all[4] = gRealOut.U;
        error_data.errorScene3.all[5] = gIUVWabs.U;
        error_data.errorScene3.all[6] = gIUVWabs.V;
        error_data.errorScene3.all[7]=  gIUVWabs.W;
    }*/
}

extern void pfc_reset_error(void);
//清除现在故障
void clear_errordata(void)
{
    Uint16 i=0;
    Uint16* pointer;
/*    error_data.latest_error=no_error;
    error_data.errorScene3.all[0] = Get_VfSpeed();
    error_data.errorScene3.all[1] = gIUVW.dc;
    error_data.errorScene3.all[2] = gUUVW.dc;
    error_data.errorScene3.all[3] = gRealOut.I;
    error_data.errorScene3.all[4] = gRealOut.U;
    error_data.errorScene3.all[5] = gIUVWabs.U;
    error_data.errorScene3.all[6] = gIUVWabs.V;
    error_data.errorScene3.all[7]= gIUVWabs.W;
*/
    Save_errordata();
    pfc_reset_error();
    //解除过流保护
    pointer = &ovrcur_protect.time;
    for(i=0;i<sizeof(OVRCUR_PROTECT_STRUCT)-2;i++)
    {
        *(pointer+i) = g_default_overcurpar[i];
    }

    //解除缺相保护
    pointer =  &phase_protect.reback_time;
    for(i=0;i<sizeof(LOSEPHA_PROTECT_STRUCT);i++)
    {
         *(pointer+i) = g_default_losphapar[i];
    }

    //解除短路保护
  /*  pointer =  &shot_protect.reback_time;
    for(i=0;i<sizeof(SHOT_PROTECT_STRUCT);i++)
    {
          *(pointer+i) =0; //g_default_shot[i];
    }*/
    shot_protect.lock = 0;
    shot_protect.reback_time = 0;
    shot_protect.start_time = 0;
    shot_protect.statue = 0;
    shot_protect.times = 0;

    gshot_flag = false;
    gipm_flag = false; //解除IPM保护

    //清除过问保护
    tempre_flag=false;


}


/************************************************************
函数输入:无
函数输出:true表示有故障，fase表示无故障
调用位置:运行状态下2ms循环
调用条件:
函数功能:输入欠压保护
************************************************************/
bool Overudc_Protect(void)
{
    static unsigned int times;          //故障发生次数，用以做简单的滤波
    if(MOTOR_RUN==runFlag.bit.run)
    {
        if( (gUUVW.dc> MAX_UDC)||(gUUVW.dc< MIN_UDC) )
        {
            times++;
            if(times>500)               //连续超过100次，说明有故障发生
            {
                times=500;

                if(gUUVW.dc> MAX_UDC)
                get_errordata(udc_over);
                else
                get_errordata(udc_low);

                return true;
            }
        }
        else
        {
            times=0;
        }
    }
    else
    {
        if( (gUUVW.dc> MAX_UDC1)||(gUUVW.dc< MIN_UDC1) )
        {
            times++;
            if(times>500)                    //连续超过100次，说明有故障发生
            {
                times=501;
                return true;
            }
        }
        else
        {
            times=0;
        }
    }
    return false;
}

/************************************************************
函数输入:无
函数输出:true表示有故障，fase表示无故障
调用位置:运行状态下2ms循环
调用条件:
函数功能:输出过流保护
************************************************************/
bool OverCurrent_Protect(void)
{
    static unsigned int times=0;          //故障发生次数，用以做简单的滤波
    static unsigned int check_flag=0;     //测试周期

    check_flag++;
    if(check_flag==50)
    {
      check_flag=0;
    }
    else  return (bool)(ovrcur_protect.statue|ovrcur_protect.lock);            //50*2 = 100ms做处理

    if( (ovrcur_protect.lock==false) )            //无故障指示时，才进行相应的操作
    {
        if(ovrcur_protect.statue==false)
        {
            if( (gIUVWabs.U>ovrcur_protect.maxcur2)||(gIUVWabs.V>ovrcur_protect.maxcur2)||(gIUVWabs.W>ovrcur_protect.maxcur2) )        //2.5倍过载，立马保护
            {
                get_errordata(Iac_over);
                ovrcur_protect.statue = true;                //故障发生
                ovrcur_protect.times++;                      //故障次数加1

                if( ovrcur_protect.times >=OVR_CUR_TIMES )   //故障大于指定次数，故障锁死
                {
                    ovrcur_protect.lock = true;
                }

                ovrcur_protect.reback_time=0;                 //缺相保护故障次数清除时间
                ovrcur_protect.start_time=0;                  //缺相保护重启时间

                return true;                                   //返回故障
            }
            else if( (gIUVWabs.U>ovrcur_protect.maxcur1)||(gIUVWabs.V>ovrcur_protect.maxcur1)||(gIUVWabs.W>ovrcur_protect.maxcur1) )   //超过2倍过载，软件采取逐波限流的方式
            {
                times++;
                if(times>2)                                      //连续超过两次，说明有故障发生
                {
                  get_errordata(Iac_over);
                  ovrcur_protect.time++;                         //故障持续时间增加
                  if( ovrcur_protect.time>MAX_OVRCUR1_TIME )
                  {
                     ovrcur_protect.statue = true;                //故障发生
                     ovrcur_protect.times++;                      //故障次数加1

                     if( ovrcur_protect.times >=OVR_CUR_TIMES )   //故障大于指定次数，故障锁死
                         ovrcur_protect.lock = true;

                     ovrcur_protect.start_time=0;

                     return true;                                 //返回故障
                  }
                   times=3;

                  ovrcur_protect.reback_time=0;
                  return false;                                   //故障时间未到1min
                }
            }
            else
            {
                ovrcur_protect.reback_time++;

                if(ovrcur_protect.reback_time>OVR_CURBACK_TIME)    //30min无故障发生，清除故障次数
                {
                    ovrcur_protect.reback_time=0;
                    ovrcur_protect.times=0;
                }
                times=0;
            }
        }
        else
        {
            ovrcur_protect.start_time++;
            if(ovrcur_protect.start_time>OVR_CURSTART_TIME)
            {
                ovrcur_protect.statue = false;
                ovrcur_protect.start_time=0;
            }
            return true;
        }
    }
    else
    {
        return true;
    }

   return false;
}

#define ABS(A,B)  ( A>B?(A-B):(B-A) )

/************************************************************
函数输入:无
函数输出:true表示有故障，fase表示无故障
调用位置:运行状态下2ms循环
调用条件:
函数功能:输出缺相保护
************************************************************/
bool LosePhase_Protect(void)
{
   static unsigned int times=0;          //故障发生次数，用以做简单的滤波
   static unsigned int check_flag=0;     //测试周期
          unsigned int out_cur=0;        //输出电流个数
          unsigned int error=0;          //两相之间误差
                  bool unbalance=false;  //负载不平衡

   check_flag++;
   if(check_flag==50)
   {
       check_flag=0;
   }
   else  return phase_protect.statue|phase_protect.lock;     //50*2 = 100ms做处理


   if(gIUVWabs.U>400)                     //默认小于2A电流不做缺相保护
       out_cur+=1;
   if(gIUVWabs.V>400)
       out_cur+=1;
   if(gIUVWabs.W>400)
       out_cur+=1;

   error=ABS(gIUVWabs.U,gIUVWabs.V);
   if(error>600)                           //默认相间电流小于4A 不做相间不平衡测试
   {
       unbalance=true;
   }

   error=ABS(gIUVWabs.U,gIUVWabs.W);
   if(error>600)
   {
       unbalance=true;
   }

   if(phase_protect.lock==false)             //检测缺相保护是否锁死
   {
       if( ( (out_cur>0)&&(unbalance==true) )&&(phase_protect.statue == false) )     //有电流输出且小于三相
       {
           times++;
           //连续超过100次，说明有故障发生 ，10*100 = 1000ms,缺点：频率小于5Hz的时候检测不了满周期，频率高的时候响应速度又不够快
           if(times>50)
           {
               get_errordata(lose_phase);                 //保存缺相保护数据
               phase_protect.times++;                     //故障次数加1
               phase_protect.statue=true;                 //更新故障状态
               if(phase_protect.times>=LOSE_PHASE_TIMES)  //缺相保护大于固定的次数就会锁死
               {
                   phase_protect.lock=true;               //缺相保护锁死状态
               }
               phase_protect.reback_time=0;               //缺相保护故障次数清除时间
               phase_protect.start_time=0;                //缺相保护重启时间
               times=0;

               return true;
           }
       }
       else if(phase_protect.statue==true)
       {
           phase_protect.start_time++;
           if(phase_protect.start_time>LOSE_PHASTART_TIME)
           {
               phase_protect.statue=false;
               phase_protect.start_time=0;
               phase_protect.reback_time=0;
           }
           times=0;
           return true;
       }
       else
       {
           phase_protect.statue = false;                //清除故障标志位
           phase_protect.reback_time++;

          if(phase_protect.reback_time>OVR_CURBACK_TIME) //30min无故障发生，清除故障次数
          {
              phase_protect.reback_time=0;
              phase_protect.times=0;
          }

           times=0;
       }
   }
   else
   {
       return true;
   }
   return false;
}


/************************************************************
函数输入:无
函数输出:true表示有故障，fase表示无故障
调用位置:运行状态下50ms循环
调用条件:
函数功能:输出过温保护
************************************************************/
bool OverTemp_Protect(void)
{
    if( (gTemperature.Temp>MAX_TEMP)&&(tempre_flag==false) )
    {
        get_errordata(over_temp);
        tempre_flag=true;
    }
    else if(gTemperature.Temp<MIN_TEMP)
    {
        tempre_flag = false;
    }
    return tempre_flag;
}


bool read_shot_IPM_fault(void)
{
    return (gipm_flag|gshot_flag);
}

/************************************************************
函数输入:无
函数输出:true表示有故障，fase表示无故障
调用位置:运行状态下2ms循环
调用条件:
函数功能:输出短路保护
************************************************************/
bool Shot_Protect(void)
{
    if(shot_protect.lock==false)
       {
           if( (gshot_flag==true)&&(shot_protect.statue == false) )
           {
                shot_protect.times++;
                shot_protect.statue=true;
                if(shot_protect.times>=SHORT_TIMES)
                {
                 //   get_errordata(shot);
                   shot_protect.lock=true;
                }
                else
                {
                   // get_errordata(shot);
                }
                shot_protect.reback_time=0;
                shot_protect.start_time=0;


                return true;

           }
           else if(shot_protect.statue==true)
           {
               shot_protect.start_time++;
               if((shot_protect.start_time>SHOT_START_TIME))
               {
                   shot_protect.statue=false;
                   gshot_flag=false;                  //复位短路信号
                   shot_protect.start_time=0;
                   shot_protect.reback_time=0;
               }

               return true;
           }
           else
           {
               shot_protect.statue = false;                //清除故障标志位
               shot_protect.reback_time++;

              if(shot_protect.reback_time>SHORT_BACK_TIME) //5min无故障发生，清除故障次数
              {
                  shot_protect.reback_time=0;
                  shot_protect.times=0;
              }

              return false;
           }
       }
       else
       {
           return true;
       }
    //   return true;
}


void Set_IPM_protect(void)
{
    if( (0==READ_FAULT_H)||(0==READ_FAULT_L) )
    {
        gipm_flag = true;
        PwmOff();
    }
}

/************************************************************
函数输入:无
函数输出:true表示有故障，fase表示无故障
调用位置:运行状态下50ms循环
调用条件:
函数功能:IPM保护
************************************************************/
bool IPM_protect(void)
{
    if( true==gipm_flag )
    {
        get_errordata(IPM_fault);
        return true;
    }
    else
    {
        return false;
    }
}


extern Uint16 read_stopbit(void);
/************************************************************
函数输入:无
函数输出:true表示有故障，fase表示无故障
调用位置:运行状态下2ms循环
调用条件:
函数功能:逻辑保护处理函数
************************************************************/

bool logic_protect(void)
{

    bool logic=false;
   // logic |= Overudc_Protect();           //只有运行的时候才会做数据记录
    logic = OverCurrent_Protect();
    logic |= LosePhase_Protect();
    logic |= OverTemp_Protect();
    logic |= Shot_Protect();
   // logic |= IPM_protect();


  /*  if(logic==true)
    {
        if(0==READ_RUN )  //启动信号关掉清除所有故障
        clear_errordata();

    }*/

    return logic;
}



void reset_errornow(Uint16 logic)
{
  if(!logic)
    {
	gerror = no_error;
    }
}
/*******************************************************************************************
Function Name:   INV_IGBTFault
Version:         V1.0
Input:
Output:
Return:
Description:     设置逆变器IGBT故障标志位
History:
*******************************************************************************************/
void Set_INVFault_flag(void)
{
    gshot_flag = true;
    stop_power();
    get_errordata(inv_shot);

}

void Set_PFCFault_flag(void)
{
    gshot_flag = true;
    stop_power();
    get_errordata(pfc_shot);

}

void Set_SoftShot_flag(void)
{
   gshot_flag = true;
   stop_power();
   get_errordata(soft_shot);
}
