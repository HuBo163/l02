#include"motorVf.h"

#ifdef S13
VF_INFO_STRUCT          gVFPar={.Down_freq=100,.Up_freq=100,.VFLineType=0,
                                .VFLineFreq=0,.VFLineFreq1=0,.VFLineFreq2=0,
                                .VFLineFreq3=0,.VFLineVolt1=0,.VFLineVolt2=0,
                                .VFLineVolt3=0};     //VF参数初始化

#else
VF_INFO_STRUCT          gVFPar={.Down_freq=50,.Up_freq=50,.VFLineType=2,
                                .VFLineFreq=0,.VFLineFreq1=2000,.VFLineFreq2=3000,.VFLineFreq3=5000,.VFLineVolt1=16800,.VFLineVolt2=26300,
                                .VFLineVolt3=44000};     //VF参数初始化
#endif



/************************************************************
函数输入:无
函数输出:无
调用位置:运行在中断中
调用条件:
函数功能:VF控制，通过输入频率计算出输出电压
************************************************************/
uint16  CalOutVotInVFStatus(uint16  freq)
{
    Uint32 mVolt=0;

    if( freq>gVFPar.VFLineFreq3 )
    {
        freq = gVFPar.VFLineFreq3;
    }

    if( (gVFPar.VFLineType == 1) )                  //线性VF控制
    {
         mVolt=(Uint32)freq*(Uint32)gVFPar.VFLineFreq/gVFPar.VFLineFreq;
    }
    else if( (gVFPar.VFLineType == 2) )                   //多点VF控制，这里对应三点VF控制，只能通过485控制
     {
         if( freq <= gVFPar.VFLineFreq1)
         {
             mVolt=(Uint32)freq*(Uint32)gVFPar.VFLineVolt1/gVFPar.VFLineFreq1;
         }
         else if(freq <= gVFPar.VFLineFreq2)
         {
            mVolt=(Uint32)(freq-gVFPar.VFLineFreq1)*(Uint32)(gVFPar.VFLineVolt2-gVFPar.VFLineVolt1);
            mVolt/=(Uint32)(gVFPar.VFLineFreq2-gVFPar.VFLineFreq1);
            mVolt+=gVFPar.VFLineVolt1;
         }
         else if(freq <= gVFPar.VFLineFreq3)
         {
             mVolt=(Uint32)(freq-gVFPar.VFLineFreq2)*(Uint32)(gVFPar.VFLineVolt3-gVFPar.VFLineVolt2);
             mVolt/=(Uint32)(gVFPar.VFLineFreq3-gVFPar.VFLineFreq2);
             mVolt+=gVFPar.VFLineVolt2;
         }
     }
    return mVolt;
}
