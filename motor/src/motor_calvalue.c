/*********************************************************************
 * initCpuBase.c
 *
 *  Created on:       2020年3月12日
 *      Author:       dfs_hb
 * Description:       计算参数
 *     History:       VER:1.0
 *
 *********************************************************************/
#include "calvalue.h"
#include "motor_math.h"
#include "pfc_control.h"

#define  C_MAX_PER       40950       //12位采样的最大值


//正弦中点电压以及零漂，默认上电检测2000次，检测时间 2000/6k(载波也就是中断频率)
UVW_EXCURSION_STRUCT  gIExcursionInfo={ .Erru=MOTOR_ADFULL_HALF, .Errv=MOTOR_ADFULL_HALF, .Errw=MOTOR_ADFULL_HALF,
                                        .Count=2000,.Coff=4280,.Coffdc=1400
                                      }; //1285

UVW_EXCURSION_STRUCT  gUExcursionInfo={  .Erru=MOTOR_ADFULL_HALF, .Errv=MOTOR_ADFULL_HALF, .Errw=MOTOR_ADFULL_HALF,
                                         .Count=2000,.Coff=8540,.Coffdc=1838
                                      };


UVW_STRUCT_ABS        gIUVWabs = {.NUM=0};
UVW_STRUCT_ABS        gUUVWabs = {.NUM=0};  //求取均方根值用
UVW_STRUCT            gUUVW,gIUVW;
StructADCResult       gADCResult = {0,0,0,0,0,0,0,0,0};
TEMPLETURE_STRUCT     gTemperature;
REALOUT_STRUCT        gRealOut;
DISPLAY_STRUCT        gdisplay = {1,1,1,1,1,1,1,1,1,1};
Uint16                gIdc = 0;

static bool           gmotor_excursion_flag = false;


extern ABC_ab_dq_DEF      gpfc_iac;
extern uint16 get_out_voltage(void);


Uint16 get_motorexcur_flag(void)
{
    return (Uint16)gmotor_excursion_flag;
}


void update_display()
{
  //  gdisplay.Udc  = (gpfc_iac.d*1000);//Filter8(gUUVW.dc,gdisplay.Udc);
  //  gdisplay.Idc  = Filter8(gIdc,gdisplay.Idc);
    gdisplay.Temp = Filter8(gTemperature.Temp,gdisplay.Temp);
    gdisplay.Ia   = Filter8(gIUVWabs.U,gdisplay.Ia);
    gdisplay.Ib   = Filter8(gIUVWabs.V,gdisplay.Ib);
    gdisplay.Ic   = Filter8(gIUVWabs.W,gdisplay.Ic);
   // gdisplay.Ua   = Filter8(gUUVWabs.U,gdisplay.Ua);
  //  gdisplay.Ub   = Filter8(gUUVWabs.V,gdisplay.Ub);
   // gdisplay.Uc   = Filter8(gUUVWabs.W,gdisplay.Uc);
    gdisplay.U    = gdisplay.U = get_out_voltage();//Filter8(gRealOut.U,gdisplay.U);
 //   gdisplay.I    = Filter8(gRealOut.I,gdisplay.I);
}

void GetUVWInfo(void)
{
    int64  m_Iu, m_Iv, m_Iw;
 //   int64  m_Uu, m_Uv, m_Uw;
 //   int64  m_Udc,m_Idc;
    float  m_temp,motor_temp;

    CmdVoltADCSample(&gADCResult);

    m_Iu = (int64)gADCResult.IuAD - (int64)gIExcursionInfo.Erru;      //减去零漂
    m_Iv = (int64)gADCResult.IvAD - (int64)gIExcursionInfo.Errv;
    m_Iw = (int64)gADCResult.IwAD - (int64)gIExcursionInfo.Errw;

//    m_Uu = (int64)gADCResult.UuAD - (int64)gUExcursionInfo.Erru;
//    m_Uv = (int64)gADCResult.UvAD - (int64)gUExcursionInfo.Errv;
//    m_Uw = (int64)gADCResult.UwAD - (int64)gUExcursionInfo.Errw;
//
//
//    m_Udc = (int64)gADCResult.VdcAD - (int64)gUExcursionInfo.Errdc;
//    m_Idc = (int64)gADCResult.IdcAD - (int64)gIExcursionInfo.Errdc;

 //   m_temp = (int64)gADCResult.TempAD - (int64)gTemperature.Err;

    m_Iu = ( m_Iu * (int64)gIExcursionInfo.Coff) >> 10;           //计算运放放大系数
    m_Iv = ( m_Iv * (int64)gIExcursionInfo.Coff) >> 10;
    m_Iw = ( m_Iw * (int64)gIExcursionInfo.Coff) >> 10;

//    m_Uu = ( m_Uu * (int64)gUExcursionInfo.Coff) >> 10;
//    m_Uv = ( m_Uv * (int64)gUExcursionInfo.Coff) >> 10;
//    m_Uw = ( m_Uw * (int64)gUExcursionInfo.Coff) >> 10;

//    m_Udc = (abs(m_Udc) * (int64)gUExcursionInfo.Coffdc) >> 10;
//    m_Idc = (abs(m_Idc) * (int64)gIExcursionInfo.Coffdc) >> 10;

    motor_temp = (float)gADCResult.TempAD;

    m_Iu = sat(m_Iu, C_MAX_PER, -C_MAX_PER);
    m_Iv = sat(m_Iv, C_MAX_PER, -C_MAX_PER);
    m_Iw = sat(m_Iw, C_MAX_PER, -C_MAX_PER);

 //   m_Uu = sat(m_Uu, C_MAX_PER, -C_MAX_PER);
 //   m_Uv = sat(m_Uv, C_MAX_PER, -C_MAX_PER);
 //   m_Uw = sat(m_Uw, C_MAX_PER, -C_MAX_PER);

 //   m_Udc = sat(m_Udc, C_MAX_PER, 0);
 //   m_Idc = sat(m_Idc, C_MAX_PER, 0);

  //  m_temp= sat(m_temp, C_MAX_PER, -C_MAX_PER);


   //gUUVW.U = m_Uu ;
   //gUUVW.V = m_Uv ;
   //gUUVW.W = m_Uw ;
   //gUUVW.dc = m_Udc;

    gIUVW.U  = m_Iu;
    gIUVW.V  = m_Iv;
    gIUVW.W  = m_Iw;
  //  gIUVW.dc = m_Idc;

    if(motor_temp<180)
      m_temp = 0.2398*motor_temp-18.155;
    else if(motor_temp<800)
      m_temp = 0.0784*motor_temp+12.852;
    else if(motor_temp<2300)
      m_temp = 0.0327*motor_temp+50.304;
    else
      m_temp = 0.0357*motor_temp+42.257;

    if(m_temp<0)
      motor_temp = 0;
    gTemperature.Temp =  (int32)(m_temp+0.5);


    update_display();
}


extern BOOL ckeck_data_normal(void);
/************************************************************
    计算零漂程序
************************************************************/
BOOL GetCurExcursion(BOOL flag)
{
    static BOOL power_on_check=FALSE;
    static BOOL cal_over = FALSE;
    static uint16 times = 0;

    CmdVoltADCSample(&gADCResult);
    power_on_check = ckeck_data_normal();

    if( power_on_check && (!cal_over) )
    {
        times ++ ;
        if( times > gIExcursionInfo.Count )
        {
            times = 0;

            gIExcursionInfo.Erru  = gIExcursionInfo.u/gIExcursionInfo.Count;
            gIExcursionInfo.Errv  = gIExcursionInfo.v/gIExcursionInfo.Count;
            gIExcursionInfo.Errw  = gIExcursionInfo.w/gIExcursionInfo.Count;

            if( (gIExcursionInfo.Erru<MOTOR_ADEXCURSION_MIN)||(gIExcursionInfo.Erru>MOTOR_ADEXCURSION_MAX)||
        	(gIExcursionInfo.Errv<MOTOR_ADEXCURSION_MIN)||(gIExcursionInfo.Errv>MOTOR_ADEXCURSION_MAX)||
		(gIExcursionInfo.Errw<MOTOR_ADEXCURSION_MIN)||(gIExcursionInfo.Errw>MOTOR_ADEXCURSION_MAX) )
	    {
        	gIExcursionInfo.u  = 0;
		gIExcursionInfo.v  = 0;
		gIExcursionInfo.w  = 0;

		gIExcursionInfo.Erru  = MOTOR_ADFULL_HALF;
		gIExcursionInfo.Errv  = MOTOR_ADFULL_HALF;
		gIExcursionInfo.Errw  = MOTOR_ADFULL_HALF;

		cal_over = FALSE;
	    }
            else
            {
        	cal_over = TRUE;
            }
        }
        gIExcursionInfo.u  += gADCResult.IuAD;
        gIExcursionInfo.v  += gADCResult.IvAD;
        gIExcursionInfo.w  += gADCResult.IwAD;

    }
    if(FALSE==power_on_check)
    {
	gIExcursionInfo.u  = 0;
	gIExcursionInfo.v  = 0;
	gIExcursionInfo.w  = 0;

	gIExcursionInfo.Erru  = MOTOR_ADFULL_HALF;
	gIExcursionInfo.Errv  = MOTOR_ADFULL_HALF;
	gIExcursionInfo.Errw  = MOTOR_ADFULL_HALF;
    }

    gmotor_excursion_flag=cal_over;
    return cal_over;
}

int16 a[200];
int16 b[200];
int16 c[200];

int16 x[200];
int16 y[200];
int16 z[200];
Uint16 head=0;

void change_displaydata(void)
{
    uint16 i,j=head;
    for(i=0;i<200;i++)
    {
        x[i]=a[j];
        y[i]=b[j];
        z[i]=a[j]-b[j];
        j++;
        if(j>=200)
            j=0;
    }

}

//获取实时输出电压电流数据
void GetRealOut(void)
{
//    uint32  tempU, tempI;
//    uint32  tempU1, tempI1;
      static uint32 n=0;

//    tempU = (uint32)(gUUVW.U*gUUVW.U + gUUVW.V*gUUVW.V + gUUVW.W*gUUVW.W);
//    tempI = (uint32)(gIUVW.U*gIUVW.U + gIUVW.V*gIUVW.V + gIUVW.W*gIUVW.W);
//
//    tempU1 = SqrtFix(tempU*2/3);
//    tempI1 = SqrtFix(tempI*2/3);

 //   t++;
    {
        n++;
        if(n>=200)
            n=0;
        head = n;

        a[n] = gIUVW.U;
        b[n] = gIUVW.V;
        c[n] = gIUVW.W;
    }
//    if((0 == gRealOut.I)||(0 == gRealOut.U) )
//    {
//        gRealOut.U = tempU1;
//        gRealOut.I = tempI1;
//    }
//    else
//    {
//        gRealOut.U = Filter8(tempU1,gRealOut.U);  //抑制毛刺
//        gRealOut.I = Filter8(tempI1,gRealOut.I);
//    }
}


//以绝对值平均值代替正弦波有效值
void GetCurrentabs(unsigned int sum)
{
//    static Uint32 Idc_sum = 0;
//    static Uint32 Idc;

    gIUVWabs.Sum_U += gIUVW.U*gIUVW.U;
    gIUVWabs.Sum_V += gIUVW.V*gIUVW.V ;
    gIUVWabs.Sum_W += gIUVW.W*gIUVW.W ;

//    Idc = Filter8(gIUVW.dc,Idc);
//    Idc_sum += Idc*Idc;

    gIUVWabs.NUM++;

    if(gIUVWabs.NUM<0)
           gIUVWabs.NUM=0;

    if(gIUVWabs.NUM>sum)
    {
        gIUVWabs.U = sqrt(gIUVWabs.Sum_U/gIUVWabs.NUM) ;
        gIUVWabs.V = sqrt(gIUVWabs.Sum_V/gIUVWabs.NUM) ;
        gIUVWabs.W = sqrt(gIUVWabs.Sum_W/gIUVWabs.NUM) ;
//      gIdc       = sqrt(Idc_sum/gIUVWabs.NUM) ;
        gIUVWabs.Sum_U=0;
        gIUVWabs.Sum_V=0;
        gIUVWabs.Sum_W=0;
        gIUVWabs.NUM=0;
//        Idc_sum = 0;
    }
}


//以绝对值平均值代替正弦波有效值
void Getvoltageabs(unsigned int sum)
{

    gUUVWabs.Sum_U += gUUVW.U*gUUVW.U;
    gUUVWabs.Sum_V += gUUVW.V*gUUVW.V;
    gUUVWabs.Sum_W += gUUVW.W*gUUVW.W;

    gUUVWabs.NUM++;

    if(gUUVWabs.NUM<0)
        gUUVWabs.NUM=0;

    if(gUUVWabs.NUM>sum)
    {
        gUUVWabs.U = sqrt(gUUVWabs.Sum_U/gUUVWabs.NUM) ;
        gUUVWabs.V = sqrt(gUUVWabs.Sum_V/gUUVWabs.NUM) ;
        gUUVWabs.W = sqrt(gUUVWabs.Sum_W/gUUVWabs.NUM) ;
        gUUVWabs.Sum_U=0;
        gUUVWabs.Sum_V=0;
        gUUVWabs.Sum_W=0;
        gUUVWabs.NUM=0;
    }
}

