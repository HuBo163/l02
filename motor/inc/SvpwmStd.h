#ifndef  __SVPWM_H
#define __SVPWM_H

#include "F28x_Project.h"

#define MAX_OVMODUL   120        //放大100倍，最大过调制比为1.2


typedef struct
{
    uint16  Gain;                // Input: reference gain voltage (pu)
    uint16  Freq;                // Input: reference frequency (pu)
    uint16  FreqMax;             // 最大输出频率
    Uint16  modul_max;           //过调制能力
    uint16  StepAngle;           // 步进
} SVGENMF;

/*-----------------------------------------------------------------------------
Default initalizer for the SVGENMF object.
-----------------------------------------------------------------------------*/
#define SVGENMF_DEFAULTS { 0,0,100,120,0 }       //默认最大输出频率为100Hz,过调制比为1.2


typedef enum
{
    SECTION_7MODE,
    SECTION_5MODE
} pwm_57section_enum_TypeDef;


typedef struct
{
    uint16      pwm_period;             /*定时器周期值*/

    uint16      ta;
    uint16      tb;
    uint16      tc;

    uint16      u_cmp;                  /*U桥臂比较值*/
    uint16      v_cmp;                  /*V桥臂比较值*/
    uint16      w_cmp;                  /*W桥臂比较值*/

    pwm_57section_enum_TypeDef  pwm_57section_mode; /*  5,7段式       */

} pwm_timer_TypeDef;
extern pwm_timer_TypeDef pwm_timer;


void pwm_timer_run_init(pwm_timer_TypeDef *p);
void set_svpwm_section_mode(pwm_timer_TypeDef * p, pwm_57section_enum_TypeDef pwm_section);
void  SvpwmStd(int32 u_alpha, int32 u_beta, pwm_timer_TypeDef *p);

#endif  /*__SVPWM_H*/


