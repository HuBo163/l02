/*********************************************************************
 * initCpuBase.c
 *
 *  Created on:       2020年3月12日
 *      Author:       dfs_hb
 * Description:       计算参数
 *     History:       VER:1.0
 *
 *********************************************************************/

#ifndef MOTOR_INC_CALVALUE_H_
#define MOTOR_INC_CALVALUE_H_

#include "F28x_Project.h"
#include "FunctionCommon.h"
#include "motor_math.h"

#define MOTOR_ADEXCURSION_MAX   2100
#define MOTOR_ADEXCURSION_MIN   1700

#define MOTOR_ADFULL_HALF        2017


// 结构体定义，用于计算零漂以及斜率
typedef struct UVW_EXCURSION_STRUCT_DEF{
    int32     u;
    int32     v;
    int32     w;
    int32     dc;
    int16     Erru;                          //U相零漂大小
    int16     Errv;                          //V相零漂大小
    int16     Errw;                          //W相零漂大小
    int16     Errdc;                         //dc相零漂大小
    int16     Count;
    int32     Coff;
    int32     Coffdc;
}UVW_EXCURSION_STRUCT;


//求取电压电流均方根值
typedef struct UVW_STRUCT_DEF_ABS{
    int32    U;
    int32    V;
    int32    W;
    int32    NUM;
    int64    Sum_U;
    int64    Sum_V;
    int64    Sum_W;
    int32    coff;
}UVW_STRUCT_ABS;                  //定子三相坐标轴电压电流


// 用来计算
typedef struct UVW_SAMPLING_STRUCT_DEF{
    int32    U;
    int32    V;
    int32    W;
    int32    UErr;               //U相毛刺滤波
    int32    VErr;               //V相毛刺滤波
    int32    WErr;               //V相毛刺滤波
    int32    Coff;               //采样电流转换为标么值电流的系数
}UVW_SAMPLING_STRUCT;


//求取三相电压电流瞬时值
typedef struct UVW_STRUCT_DEF{
    int32    U;
    int32    V;
    int32    W;
    int32    dc;
}UVW_STRUCT;                                   //三相坐标电压电流值



typedef struct TEMPLETURE_STRUCT_DEF{
    int32     Temp;                           //用度表示的实际温度值
    int32     Err;
    int32     Coff;
}TEMPLETURE_STRUCT;                           //和变频器温度相关的数据结构


//实时计算输出电压电流的瞬时值
typedef struct REALOUT_STRUCT_DEF{
    Uint16     U;
    Uint16     I;
}REALOUT_STRUCT;


typedef struct DISPLAY_STRUCT_DEF{
    Uint16     Udc;                           //用度表示的实际温度值
    Uint16     Idc;
    Uint16     U;
    Uint16     I;

    Uint16     Ia;
    Uint16     Ib;
    Uint16     Ic;
    Uint16     Ua;
    Uint16     Ub;
    Uint16     Uc;
    Uint16     Temp;
}DISPLAY_STRUCT;


extern UVW_EXCURSION_STRUCT  gIExcursionInfo;
extern UVW_EXCURSION_STRUCT  gUExcursionInfo;

extern UVW_STRUCT_ABS        gIUVWabs, gUUVWabs;
extern UVW_SAMPLING_STRUCT   gCurSamp, gVolSamp;
extern UVW_STRUCT            gUUVW,gIUVW;
extern TEMPLETURE_STRUCT     gTemperature;
extern REALOUT_STRUCT        gRealOut;


extern void GetUVWInfo(void);
extern void GetRealOut(void);
extern void GetCurrentabs(unsigned int sum);
extern BOOL GetCurExcursion(BOOL flag);
extern void Getvoltageabs(unsigned int sum);

#endif /* MOTOR_INC_CALVALUE_H_ */
