/*********************************************************************
 * initCpuBase.c
 *
 *  Created on:       2020年3月18日
 *      Author:       dfs_hb
 * Description:       电机数学计算h文件
 *     History:       VER:1.0
 *
 *********************************************************************/

#ifndef MOTOR_INC_MOTOR_MATH_H_
#define MOTOR_INC_MOTOR_MATH_H_

#include "F28x_Project.h"
#include "FunctionCommon.h"

//#define FilterN(x,y) (y + (x-y)/N)   一阶滤波函数， 有静差
#define Filter1(x,total)   (x)
#define Filter2(x,total)   (( (((uint64)total)<<16) + (((uint64)x)<<15) - (((uint64)total)<<15) )>>16)
#define Filter4(x,total)   (( (((uint64)total)<<16) + (((uint64)x)<<14) - (((uint64)total)<<14) )>>16)
#define Filter8(x,total)   (( (((uint64)total)<<16) + (((uint64)x)<<13) - (((uint64)total)<<13) )>>16)
#define Filter16(x,total)  (( (((uint64)total)<<16) + (((uint64)x)<<12) - (((uint64)total)<<12) )>>16)
#define Filter32(x,total)  (( (((uint64)total)<<16) + (((uint64)x)<<11) - (((uint64)total)<<11) )>>16)
#define Filter64(x,total)  (( (((uint64)total)<<16) + (((uint64)x)<<10) - (((uint64)total)<<10) )>>16)
#define Filter128(x,total) (( (((uint64)total)<<16) + (((uint64)x)<<9 ) - (((uint64)total)<<9 ) )>>16)
#define Filter256(x,total) (( (((uint64)total)<<16) + (((uint64)x)<<8 ) - (((uint64)total)<<8 ) )>>16)

#define ABS_INT16(a) (((a) >= 0) ? (a) : (-(a)))
#define ABS_INT32(a) (((a) >= 0L) ? (a) : (-(a)))
#define ABS_INT64(a) (((a) >= (int64)0) ? (a) : (-(a)))

#define  GetMax( x, y ) ( ((x) > (y)) ? (x) : (y) )
#define  GetMin( x, y ) ( ((x) < (y)) ? (x) : (y) )
#define  MAX(A,B)       (A>B?A:B)
#define  MIN(A,C)       (A<C?A:C)
#define  sat(A,B,C)     MAX(MIN(A,B),C)

typedef struct
{
    int32   U;
    int32   V;
    int32   W;
}StructPhase;
extern StructPhase  IPhase;


typedef struct
{
    int32   Alpha;
    int32   Beta;
}StructAlphaBeta;


extern StructAlphaBeta  IAlphaBeta;
extern StructAlphaBeta  VAlphaBeta;
extern StructAlphaBeta  VAlphaBetaPWM;


typedef struct
{
    int32   D;
    int32   Q;
}StructDQ;
extern StructDQ IDQ;
extern StructDQ VDQ;
extern StructDQ VdqOut;


typedef struct
{
    int32   Amp;
    uint16  Theta;
}StructAmpTheta;
extern StructAmpTheta   AmpTheta;


void UVWToAlphaBeta(StructPhase *pPhase, StructAlphaBeta *pAlphaBeta);
void AlphaBetaToDQ(StructAlphaBeta *pAlphaBeta, uint16 Angle, StructDQ *pDQ);
void DQToAmplitudeTheta(StructDQ *pDQ,StructAmpTheta *pAmpTheta);
void DQToAlphaBeta(StructDQ *pDQ, StructAlphaBeta *pAlphaBeta, uint16 Angle);
uint16 AtanCalc(int16 X, int16 Y);
uint16 SqrtFix(uint32 SqrtIn);




#endif /* MOTOR_INC_MOTOR_MATH_H_ */
