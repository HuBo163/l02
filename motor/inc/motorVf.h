/***************************************************************
文件功能：和VF控制相关的数据结构定义，变量申明
文件版本：
最新更新：
************************************************************/
#ifndef MOTOR_VF_INCLUDE_H
#define MOTOR_VF_INCLUDE_H

#ifdef __cplusplus
extern "C" {
#endif

#include "F28x_Project.h"
#include "FunctionCommon.h"


typedef struct VF_INFO_STRUCT_DEF {
    uint16    VFLineType;             //VF曲线选择
//  uint16    VFTorqueUp;             //VF转矩提升增益
//  uint16    VFTorqueUpLim;          //VF转矩提升截至频率
//  uint16    VFOverExc;              //VF过励磁增益
//  uint16    VFWsComp;               //VF转差补偿增益
//  uint16    VFOvShock;              //VF抑制振荡增益
    uint16    VFLineFreq1;            //多点VF频率1
    uint16    VFLineVolt1;            //多点VF电压1
    uint16    VFLineFreq2;            //多点VF频率2
    uint16    VFLineVolt2;            //多点VF电压2
    uint16    VFLineFreq3;            //多点VF频率3
    uint16    VFLineVolt3;            //多点VF电压3
    uint16    VFLineFreq;             //线性VF频率
    uint16    VFLineVolt;             //线性VF电压
    uint16    Up_freq;                //增频速率
    uint16    Down_freq;              //减频速率
//  uint16    SVCTorqueUp;            //svc转矩提升增益
//  uint16    SVCTorqueUpLim;         //svc转矩提升截至频率

//  uint16    vfResComp;      // 低频电阻压降是否重新分解电流
//  uint16    ovGain;         // 过压失速增益
//  uint16    ovPoint;        // 过压失速抑制点
//  uint16    ovPointCoff;
//  uint16    ocGain;         // 过流失速增益
//  uint16    ocPoint;        // 过流失速点
//  uint16    FreqApply;      // vf 失速控制产生的频率

//  uint16    vfMode;         // Vf失速控制模式，0:280模式，1:新模式
//  uint16    tpLst;
}VF_INFO_STRUCT;              //VF参数设置数据结构



#endif


