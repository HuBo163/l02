#ifndef _MOTOR_PROTECT_H
#define _MOTOR_PROTECT_H


#include "F28x_Project.h"
#include "FunctionCommon.h"
#include "f_funcCode.h"

#define READ_FAULT_H      (GpioDataRegs.GPCDAT.bit.GPIO74)
#define READ_FAULT_L      (GpioDataRegs.GPCDAT.bit.GPIO75)

#define NUM_ERROR_SAVED   3

#define MAX_UDC           6800              //输入最大电压
#define MIN_UDC           2000              //输入最小电压

#define MAX_UDC1          6500              //最大电压恢复值
#define MIN_UDC1          2300              //最小电压恢复值

#define MAX_CUR1          500              //1.2倍过载
#define MAX_CUR2          700              //1.5倍过载立马保护

#define LOSE_CUR          100               //输出缺相判断电压值


//过流保护与缺相保护 30min解锁
#define  MAX_OVRCUR1_TIME       600       //1min =100ms*600
#define  OVR_CURBACK_TIME       1800      //30min=600*3=1800
#define  OVR_CURSTART_TIME      100        //6s=60
#define  OVR_CUR_TIMES          3         //3次故障锁死

#define  LOSE_PHASE_CHECK_TIME  50        //5S 检测一次缺相故障
#define  LOSE_PHASE_TIMES       3          //缺相3次锁死
#define  LOSE_PHABACK_TIME      600        //10min=600*10=6000    10min内无故障，清除缺相次数
#define  LOSE_PHASTART_TIME     100        //10S = 100    缺相保护4s钟后重启

#define  MAX_TEMP               125    //系统允许最高温度
#define  MIN_TEMP               70     //系统允许最低温度

//短路保护5min解锁
#define  SHORT_TIMES             3          //3次短路锁死
#define  SHORT_BACK_TIME         150000    //5min=500*5*60=150000
#define  SHOT_START_TIME         5000      //10S = 5000*2  短路保护10s钟后重启

//过流保护结构体
typedef struct OVRCUR_PROTECT_STRUCT_DEF{
   Uint16   time;             //故障时间
   Uint16   start_time;       //故障发生后重启时间
   Uint16   reback_time;      //故障恢复时间
   Uint16   times;            //故障次数
   bool     lock;             //故障是否锁死标志位
   bool     statue;           //故障状态标志位，当前是否有故障发生
   Uint16   maxcur1;          //1.5倍过载
   Uint16   maxcur2;          //2倍过载
}OVRCUR_PROTECT_STRUCT;       //输出缺相判断程序


//缺相保护结构体
typedef struct LOSEPHA_PROTECT_STRUCT_DEF{
    Uint16 reback_time;       //故障恢复时间
    Uint16 start_time;        //故障发生后重启时间
    Uint16 times;             //故障次数
    bool   lock;              //故障锁死
    bool   statue;            //故障状态
}LOSEPHA_PROTECT_STRUCT;


//短路保护结构体
typedef struct SHOT_PROTECT_STRUCT_DEF{
    Uint32 reback_time;       //故障恢复时间
    Uint16 start_time;        //故障发生后重启时间
    Uint16 times;             //故障次数
    bool lock;                //故障锁死
    bool statue;              //故障状态
}SHOT_PROTECT_STRUCT;

typedef enum {
    no_error=0,            //无故障
    uin_low,               //输入欠压
    uin_over,              //输入过压
    uin_unlock,            //输入锁相失败
    uin_overfreq,          //输入频率超范围
    uin_lossphase,         //输入缺相
    uin_overcurrent,       //输入过流
    udc_over,              //母线过压
    udc_low,               //母线欠压
    uac_over,              //输出过压
    uac_low,               //输出欠压
    Idc_over,              //母线过流
    Iac_over,              //输出过流
    lose_phase,            //缺相
    over_temp,             //过温
    pfc_shot,              //pfc短路
    inv_shot,              //inv短路
    soft_shot,             //软件短路
    IPM_fault,             //IPM故障
    com_fault,             //通讯故障
    pre_charge_fault,      //预充电故障
}error_type;

//故障存储数组
typedef struct ERROE_DATA_DEF{
    error_type first_error;           //第一次故障
    error_type second_error;          //第二次故障
    error_type latest_error;          //第三次故障
    union ERROR_SCENE errorScene1;    //第一次故障
    union ERROR_SCENE errorScene2;    //第二次故障
    union ERROR_SCENE errorScene3;    //第三次故障
}ERROE_DATA;


#define DEFAULT_OVERCUR_PROTECT   {false,0,false,0,0,0,4000,4500}
#define DEFAULT_LOSPHA_PROTECT    {0,0,0,false,false}
#define DEFAULT_SHOT_protect      {false,0,0,false,0}

void reset_errornow(Uint16 logic);
void Set_PFCFault_flag(void);
void Set_SoftShot_flag(void);
void Set_INVFault_flag(void);
extern __interrupt void IGBTFaultISR(void);

#endif
