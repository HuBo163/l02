//定时器配置部分相关函数
/* ----------------------- Platform includes --------------------------------*/
#include <inc/port.h>
#include "mb.h"
#include "mbport.h"
USHORT temp;

/* ----------------------- static functions ---------------------------------*/
void prvvTIMERExpiredISR( void );

/* ----------------------- Start implementation -----------------------------*/
BOOL xMBPortTimersInit( USHORT usTim1Timerout50us )
{
	    //使能时钟
	temp = usTim1Timerout50us*50;

    /**--------以下代码是初始化CPU定时器0---------**/
 //   InitCpuTimers();//初始化CPU定时器
    // CPU Timer 0
    // Initialize address pointers to respective timer registers:
    CpuTimer0.RegsAddr = &CpuTimer0Regs;
    // Initialize timer period to maximum:
    CpuTimer0Regs.PRD.all  = 0xFFFFFFFF;
    // Initialize pre-scale counter to divide by 1 (SYSCLKOUT):
    CpuTimer0Regs.TPR.all  = 0;
    CpuTimer0Regs.TPRH.all = 0;
    // Make sure timer is stopped:
    CpuTimer0Regs.TCR.bit.TSS = 1;
    // Reload all counter register with period value:
    CpuTimer0Regs.TCR.bit.TRB = 1;
    // Reset interrupt counters:
    CpuTimer0.InterruptCount = 0;


    ConfigCpuTimer(&CpuTimer0,120,temp);

    CpuTimer0Regs.TCR.bit.FREE = 0x01;//仿真控制位，当仿真暂停时，定时器的工作状态
    CpuTimer0Regs.TCR.bit.SOFT = 0x00;//FREE和SOFT位配置仿真控制
    CpuTimer0Regs.TCR.bit.TRB  = 0x01;//定时器重新装载位,写1表示将发生预分频寄存器和周期寄存器重新装载事件
    CpuTimer0Regs.TCR.bit.TIF  = 0x01;//清除中断标志位
    CpuTimer0Regs.TCR.bit.TIE  = 0x01;//使能定时器中断
    CpuTimer0Regs.TCR.bit.TSS  = 0x01;//定时器的启动和停止位，写1停止定时器，写0启动定时器
    CpuTimer0Regs.TIM.all      = 0;   //计数器清零
	return TRUE;
}


void vMBPortTimersEnable(void)
{

   PieCtrlRegs.PIEACK.bit.ACK1 = 1;
   CpuTimer0Regs.TIM.all      = 0;   //计数器清零
   CpuTimer0Regs.TCR.bit.TIF  = 0x01;//清除中断标志位
   CpuTimer0Regs.TCR.bit.TIE  = 0x01;//使能定时器中断
   ReloadCpuTimer0();
   StartCpuTimer0();
   PieCtrlRegs.PIEIER1.bit.INTx7 = 1;//使能CPU定时器0的中断


}

void vMBPortTimersDisable(void)
{
    /* Disable any pending timers. */

	CpuTimer0Regs.TCR.bit.TIE  = 0;   //使能定时器中断
	CpuTimer0Regs.TIM.all      = 0;   //计数器清零
	CpuTimer0Regs.TCR.bit.TIF  = 1;   //清除中断标志位
	StopCpuTimer0();
}

/* Create an ISR which is called whenever the timer has expired. This function
 * must then call pxMBPortCBTimerExpired( ) to notify the protocol stack that
 * the timer has expired.
 */
void prvvTIMERExpiredISR( void )
{
    ( void )pxMBPortCBTimerExpired( );
}

/**
  *****************************************************************************
  * @Name   : CPU定时器0中断函数
  *
  * @Brief  : none
  *
  * @Input  : none
  *
  * @Output : none
  *
  * @Return : none
  *****************************************************************************
**/
__interrupt void cpu_timer0_isr(void)
{
  volatile uint16 TempPIEIER1 = PieCtrlRegs.PIEIER1.all;
  volatile uint16 TempPIEIER12 = PieCtrlRegs.PIEIER12.all;

  IER = M_INT1|M_INT12;
  PieCtrlRegs.PIEIER1.all &= 0x0018;                  // Set "group"  priority
  PieCtrlRegs.PIEIER12.all &= 0x0007;                  // Set "group"  priority
  PieCtrlRegs.PIEACK.all = 0xFFFF;                    // Enable PIE interrupts
  EINT;

  prvvTIMERExpiredISR();
  CpuTimer0Regs.TCR.bit.TIF  = 0x01;//清除中断标志位
  PieCtrlRegs.PIEACK.bit.ACK1 = 1;

  DINT;
  PieCtrlRegs.PIEIER1.all = TempPIEIER1;
  PieCtrlRegs.PIEIER12.all = TempPIEIER12;
}
