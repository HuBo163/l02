#include <inc/port.h>
#include "mb.h"
#include "mbport.h"
unsigned char test[8];
unsigned char test_cnt = 0x00;
unsigned char tx_temp[10];
unsigned char tx_cnt = 0x00;


/* ----------------------- static functions ---------------------------------*/
void prvvUARTTxReadyISR( void );
void prvvUARTRxISR( void );


/* ----------------------- Start implementation -----------------------------*/
void vMBPortSerialEnable( BOOL xRxEnable, BOOL xTxEnable )
{
    /* If xRXEnable enable serial receive interrupts. If xTxENable enable
     * transmitter empty interrupts.
     */
	
	if (xRxEnable)  //接收使能
	{
		PieCtrlRegs.PIEACK.bit.ACK9=1;
		PieCtrlRegs.PIEIER9.bit.INTx3 = 1;//SCI_C_Rx

		//接收
#if USER_RS485_Port
		RX_EN;
#endif
	}
	else  //失能
	{
		PieCtrlRegs.PIEACK.bit.ACK9=1;
		PieCtrlRegs.PIEIER9.bit.INTx3 = 0;//SCI_C_Rx
		//恢复发送
#if USER_RS485_Port
//		RS485_Pin = TX_EN;
#endif
	}
	
	if (xTxEnable)  //发送使能
	{
		PieCtrlRegs.PIEACK.bit.ACK9=1;
		PieCtrlRegs.PIEIER9.bit.INTx4 = 1;//SCI_C_Tx
#if USER_RS485_Port
		TX_EN;
#endif
	}
	else  //失能
	{
		PieCtrlRegs.PIEACK.bit.ACK9=1;
		PieCtrlRegs.PIEIER9.bit.INTx4 = 0;//SCI_C_Tx
#if USER_RS485_Port
	//	RS485_Pin = RX_EN;
#endif
	}
}



BOOL xMBPortSerialInit( UCHAR ucPORT, ULONG ulBaudRate, UCHAR ucDataBits, eMBParity eParity )
{
	//USHORT temp;
	/*
	 * 关于串口波特率计算的问题
	 * BRR = ((SYS_CLK/4)/(BaudRate*8))-1
	 */
	USHORT BaudRate_Reg_temp;
	(void)ucPORT;      //不修改串口号
	(void)ucDataBits;  //不修改数据位长度
	(void)eParity;     //不修改检验格式
	//(void)ulBaudRate;

	EALLOW;
    PieVectTable.SCIB_RX_INT = &serial_Rx_isr;
    PieVectTable.SCIB_TX_INT = &serial_Tx_isr;
    PieVectTable.TIMER0_INT  = &cpu_timer0_isr;
    EDIS;

	BaudRate_Reg_temp = (USHORT)((200000000/(4*8*ulBaudRate))-1);
	InitScibGpio();                           //使用串口A作为freemodbus的串口

	ScibRegs.SCICCR.bit.ADDRIDLE_MODE = 0;    //选择通信协议，选择空闲模式，此模式兼容RS232协议
	ScibRegs.SCICCR.bit.LOOPBKENA = 0;        //自测试模式使能位:为1使能，为0禁用
	ScibRegs.SCICCR.bit.PARITY = 0;           //奇偶校验位选择位:0为奇校验，1为偶校验
	ScibRegs.SCICCR.bit.PARITYENA = 0;        //奇偶校验位使能:为1使能，为0禁用
	ScibRegs.SCICCR.bit.SCICHAR = 7;          //该配置选择SCI的一个字符的长度可配置为1到8位(单位bit)
	ScibRegs.SCICCR.bit.STOPBITS = 0;         //停止位的配置: 0 1位停止位  1 2位停止位

	//SCIC控制寄存器的配置
	ScibRegs.SCICTL1.bit.RXENA = 1;            //SCI接收器使能位
	ScibRegs.SCICTL1.bit.RXERRINTENA = 0;      //SCI接受中断错误使能
	ScibRegs.SCICTL1.bit.SLEEP = 0;            //睡眠模式使能位
	ScibRegs.SCICTL1.bit.SWRESET = 0;          //SCI软件复位
	ScibRegs.SCICTL1.bit.TXENA = 1;            //SCI发送器使能位
	ScibRegs.SCICTL1.bit.TXWAKE = 0;           //SCI发送器唤醒模式选择位，具体的不是太明白
	ScibRegs.SCICTL2.bit.RXBKINTENA = 0;       //接收中断使能
	ScibRegs.SCICTL2.bit.TXINTENA = 0;

	//波特率计算
	//9600
	//ScibRegs.SCIHBAUD = 0x01;
	//ScibRegs.SCILBAUD = 0xE7;
	ScibRegs.SCIHBAUD.bit.BAUD = (BaudRate_Reg_temp>>8)&0xFF;
	ScibRegs.SCILBAUD.bit.BAUD = BaudRate_Reg_temp&0xFF;
	ScibRegs.SCICTL1.bit.SWRESET = 1;

	//以下代码是配置SCI的接收数据的FIFO和发送数据的FIFO
	//-------------------接收数据的FIFO配置
	ScibRegs.SCIFFTX.bit.SCIRST = 0;          //复位SCIC模块
	ScibRegs.SCIFFTX.bit.SCIRST = 1;          //复位SCIC模块
	ScibRegs.SCIFFTX.bit.SCIFFENA = 1;        //使能FIFO功能
	ScibRegs.SCIFFRX.bit.RXFFIENA = 1;        //使能接收中断
	ScibRegs.SCIFFRX.bit.RXFIFORESET = 0;     //复位接收器的FIFO
	ScibRegs.SCIFFRX.bit.RXFIFORESET = 1;
	ScibRegs.SCIFFRX.bit.RXFFIL = 0x01;       //接受1个字节之后产生一个中断

	//---------------------发送数据的FIFO配置
	ScibRegs.SCIFFTX.bit.SCIRST = 0;
	ScibRegs.SCIFFTX.bit.SCIRST = 1;
	ScibRegs.SCIFFTX.bit.SCIFFENA = 1;
	ScibRegs.SCIFFTX.bit.TXFFIENA = 1;
	ScibRegs.SCIFFTX.bit.TXFIFORESET = 0;
	ScibRegs.SCIFFTX.bit.TXFIFORESET=1;
	ScibRegs.SCIFFTX.bit.TXFFIL = 0x00;        //发送完一个字节产生中断

	return TRUE;
}



BOOL xMBPortSerialPutByte( CHAR ucByte )
{
	 //发送一个字节
	ScibRegs.SCITXBUF.bit.TXDT = ucByte;
	return TRUE;
}


BOOL xMBPortSerialGetByte( CHAR * pucByte )
{
    //接收一个字节
	*pucByte= ScibRegs.SCIRXBUF.bit.SAR;
	return TRUE;
}



/*******************************************************************************
* @Name   : SCIC中断服务函数
*
* @Brief  : none
*
* @Input  : none
*
* @Output : none
*
* @Return : none
*******************************************************************************/
__interrupt void serial_Rx_isr(void)
{
  volatile uint16 TempPIEIER1 = PieCtrlRegs.PIEIER1.all;
  volatile uint16 TempPIEIER12 = PieCtrlRegs.PIEIER12.all;

  IER = M_INT1|M_INT12;
  PieCtrlRegs.PIEIER1.all &= 0x0018;                  // Set "group"  priority
  PieCtrlRegs.PIEIER12.all &= 0x0007;                  // Set "group"  priority
  PieCtrlRegs.PIEACK.all = 0xFFFF;                    // Enable PIE interrupts
  EINT;
  if(ScibRegs.SCIFFRX.bit.RXFFINT == 1)
    {
      pxMBFrameCBByteReceived();
      ScibRegs.SCIFFRX.bit.RXFFINTCLR = 1;
      ScibRegs.SCIFFRX.bit.RXFFOVRCLR = 1;
    }
  PieCtrlRegs.PIEACK.bit.ACK9=1;

  DINT;
  PieCtrlRegs.PIEIER1.all = TempPIEIER1;
  PieCtrlRegs.PIEIER12.all = TempPIEIER12;
}


__interrupt void serial_Tx_isr(void)
{

  volatile uint16 TempPIEIER1 = PieCtrlRegs.PIEIER1.all;
  volatile uint16 TempPIEIER12 = PieCtrlRegs.PIEIER12.all;

  IER = M_INT1|M_INT12;
  PieCtrlRegs.PIEIER1.all &= 0x0018;                  // Set "group"  priority
  PieCtrlRegs.PIEIER12.all &= 0x0007;                  // Set "group"  priority
  PieCtrlRegs.PIEACK.all = 0xFFFF;                    // Enable PIE interrupts
  if(ScibRegs.SCIFFTX.bit.TXFFINT == 1)
    {
      pxMBFrameCBTransmitterEmpty();
      ScibRegs.SCIFFTX.bit.TXFFINTCLR = 1;
    }
  PieCtrlRegs.PIEACK.bit.ACK9= 1;
  DINT;
  PieCtrlRegs.PIEIER1.all = TempPIEIER1;
  PieCtrlRegs.PIEIER12.all = TempPIEIER12;
}
