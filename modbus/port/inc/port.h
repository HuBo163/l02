

#ifndef _PORT_H
#define _PORT_H


#include <inttypes.h>

#include "F28x_Project.h"


#define PR_BEGIN_EXTERN_C           extern "C" {
#define	PR_END_EXTERN_C             }
//#define assert(e) ((e) ? (void)0 :  1)
void __assert(const char * x1, const char * x2, int x3);



/*
GPIO_SetupPinMux(87, GPIO_MUX_CPU1, 5);
GPIO_SetupPinOptions(87, GPIO_INPUT, GPIO_PUSHPULL);
GPIO_SetupPinMux(86, GPIO_MUX_CPU1, 5);
GPIO_SetupPinOptions(86, GPIO_OUTPUT, GPIO_ASYNC);
*/
//485 接口宏定义
#define USER_RS485_Port 1

#define TX_EN     GpioDataRegs.GPBCLEAR.bit.GPIO60 = 1
#define RX_EN     GpioDataRegs.GPBSET.bit.GPIO60   = 1

#define ENTER_CRITICAL_SECTION()   INTX_DISABLE()//__set_PRIMASK(1)  //关总中断
#define EXIT_CRITICAL_SECTION()    INTX_ENABLE() //__set_PRIMASK(0)  //开总中断

#ifndef TRUE
#define TRUE            1
#endif
#ifndef FALSE
#define FALSE           0
#endif

void INTX_DISABLE(void);
void  INTX_ENABLE(void);

__interrupt void serial_Rx_isr(void);
__interrupt void serial_Tx_isr(void);
__interrupt void cpu_timer0_isr(void);

//输入寄存器相关参数
#define REG_INPUT_START                          (USHORT)0x0001  //起始寄存器
#define REG_INPUT_NREGS                          (USHORT)10      //寄存器个数

//保持寄存器相关参数
#define REG_HOLDING_START                        (USHORT)0       //保持寄存器
#define REG_HOLDING_NREGS                        (USHORT)256      //保持寄存器个数

//线圈寄存器相关参数
#define REG_Coils_START                          (USHORT)0     //线圈寄存器起始地址
#define REG_Coils_NREGS                          (USHORT)10      //线圈寄存器个数(最好是8的倍数;

//离散寄存器相关参数
#define REG_Discrete_START						 (USHORT)0
#define REG_Discrete_NREG						 (USHORT)10

extern USHORT usRegInputBuf[REG_INPUT_NREGS];      //输入寄存器,modbus协议只能读取该寄存器,不能修改
extern USHORT usRegHoldingBuf[REG_HOLDING_NREGS];  //保持寄存器,modbus协议既能修改该寄存器,又能读取该寄存器
extern UCHAR CoilsReg_Buf[3];                      //线圈寄存器数组，该数组长度为线圈寄存器的个数除以8,如果余数不为0,加1,该寄存器协议可读可写
extern UCHAR DiscreteReg_Buf[4];

#endif
