#include "F28x_Project.h"

/**********************************************************************************************
** 函数名称: RAM初始化
** 功能描述:
** 输　入:
** 输　出:
** 注  释:
***********************************************************************************************/
void RAM_INIT(void)
{
	long i;
    Uint16 *p;
//--------------初始化全局RAM---------------------------------
    p=(Uint16 *)0x00A800;  //RAM数据初始化清0
    for(i=0;i<0x00a400;i++)
    {
        *p=0x0000;
         p++;
    }
    p=(Uint16 *)0x100000;  //外部RAM数据初始化清0
    for(i=0;i<0x040000;i++)
    {
        *p=0x0000;
         p++;
    }
}
/**********************************************************************************************
** 函数名称:  EMIF初始化
** 功能描述:
** 输　入:
** 输　出:
** 注  释:
***********************************************************************************************/
void EMIF_INIT(void)
{

//---------------------------------------------------------------------
    EALLOW;
    ClkCfgRegs.PERCLKDIVSEL.bit.EMIF1CLKDIV = 0x0; //1 of PLLSYSCLK is selected   200M
    ClkCfgRegs.PERCLKDIVSEL.bit.EPWMCLKDIV  = 0x1; //1/2 of PLLSYSCLK is selected.100M
    EDIS;

    EALLOW;
    Emif1ConfigRegs.EMIF1ACCPROT0.all = 0x0;
    Emif1ConfigRegs.EMIF1COMMIT.all = 0x1;
    Emif1ConfigRegs.EMIF1LOCK.all = 0x1;
    EDIS;

    EALLOW;
    Emif1Regs.ASYNC_CS2_CR.all =  (EMIF_ASYNC_ASIZE_16     | // 16Bit Memory Interface
                                            EMIF_ASYNC_TA_1        | // Turn Around time of 2 Emif Clock
                                            EMIF_ASYNC_RHOLD_1     | // Read Hold time of 1 Emif Clock
                                            EMIF_ASYNC_RSTROBE_8   | // Read Strobe time of 4 Emif Clock  //ensure 45ns
                                            EMIF_ASYNC_RSETUP_1    | // Read Setup time of 1 Emif Clock
                                            EMIF_ASYNC_WHOLD_1     | // Write Hold time of 1 Emif Clock
                                            EMIF_ASYNC_WSTROBE_8   | // Write Strobe time of 1 Emif Clock
                                            EMIF_ASYNC_WSETUP_1    | // Write Setup time of 1 Emif Clock
                                            EMIF_ASYNC_EW_DISABLE  | // Extended Wait Disable.
                                            EMIF_ASYNC_SS_DISABLE    // Strobe Select Mode Disable.
                                           );
    Emif1Regs.ASYNC_CS3_CR.all =  (EMIF_ASYNC_ASIZE_16  | // 16Bit Memory Interface
                                            EMIF_ASYNC_TA_1        | // Turn Around time of 2 Emif Clock
                                            EMIF_ASYNC_RHOLD_1     | // Read Hold time of 1 Emif Clock
                                            EMIF_ASYNC_RSTROBE_12  | // Read Strobe time of 4 Emif Clock  ensure 45ns
                                            EMIF_ASYNC_RSETUP_1    | // Read Setup time of 1 Emif Clock
                                            EMIF_ASYNC_WHOLD_1     | // Write Hold time of 1 Emif Clock
                                            EMIF_ASYNC_WSTROBE_5   | // Write Strobe time of 1 Emif Clock  ensure 30ns
                                            EMIF_ASYNC_WSETUP_1    | // Write Setup time of 1 Emif Clock
                                            EMIF_ASYNC_EW_DISABLE  | // Extended Wait Disable.
                                            EMIF_ASYNC_SS_DISABLE    // Strobe Select Mode Disable.
                                           );
    EDIS;
}
/**********************************************************************************************
** 函数名称:  Timer初始化
** 功能描述:
** 输　入:
** 输　出:
** 注  释:
***********************************************************************************************/
void Timer_INIT(void)
{
//------------------------------------------------------------------
    EALLOW;
//-----------------------------------
    CpuTimer0Regs.PRD.all = 12000;//100us
    CpuTimer0Regs.TPR.all  = 0;
    CpuTimer0Regs.TPRH.all = 0;
    CpuTimer0Regs.TCR.all = 0x4030;
    EDIS;
}
/**********************************************************************************************
** 函数名称:  SCI初始化
** 功能描述:
** 输　入:
** 输　出:
** 注  释:
***********************************************************************************************/
void SCI_INIT(void)
{
//------------------------------------------------------------------
    EALLOW;
//--------------------------------------
    SciaRegs.SCICCR.all =0x0007;   // 1 stop bit,  No loopback
    SciaRegs.SCICTL1.all =0x0003;  // enable TX, RX, internal SCICLK,                                     // Disable RX ERR, SLEEP, TXWAKE
    SciaRegs.SCICTL2.all =0x0000;
    SciaRegs.SCIHBAUD.all =0x0000;
    SciaRegs.SCILBAUD.all =0x0035;  //115200
    SciaRegs.SCIFFTX.all=0xE040;
    SciaRegs.SCIFFRX.all=0x6040;
    SciaRegs.SCIFFCT.all=0x0;
    SciaRegs.SCICTL1.all =0x0023;     // Relinquish SCI from Reset
 //--------------------------------------
	ScidRegs.SCICCR.all =0x0007;   // 1 stop bit,  No loopback
	ScidRegs.SCICTL1.all =0x0003;  // enable TX, RX, internal SCICLK,                                     // Disable RX ERR, SLEEP, TXWAKE
	ScidRegs.SCICTL2.all =0x0000;
//  ScidRegs.SCIHBAUD.all =0x0001;
//  ScidRegs.SCILBAUD.all =0x0045; //19200
	ScidRegs.SCIHBAUD.all =0x0000;
	ScidRegs.SCILBAUD.all =0x0035;  //115200
	ScidRegs.SCIFFTX.all=0xE040;
	ScidRegs.SCIFFRX.all=0x6040;
	ScidRegs.SCIFFCT.all=0x0;
	ScidRegs.SCICTL1.all =0x0023;     // Relinquish SCI from Reset
	EDIS;
}
/**********************************************************************************************
** 函数名称:I2C初始化
** 功能描述:
** 输　入:
** 输　出:
** 注  释:
***********************************************************************************************/
void I2C_INIT(void)
{
	I2caRegs.I2CMDR.bit.IRS =0;
	I2caRegs.I2CSAR.all = 0x0050;        // Slave address - EEPROM control code
	I2caRegs.I2CPSC.all = 19;            // Prescaler - need 7-12 Mhz on module clk
	I2caRegs.I2CCLKL = 8;           // NOTE: must be non zero
	I2caRegs.I2CCLKH = 7;           // NOTE: must be non zero
	I2caRegs.I2CMDR.all = 0x0020;    // Take I2C out of reset	                                    // Stop I2C when suspended
	I2caRegs.I2CFFTX.all = 0x6040;   // Enable FIFO mode and TXFIFO
	I2caRegs.I2CFFRX.all = 0x2040;   // Enable RXFIFO, clear RXFFINT,
}
/**********************************************************************************************
** 函数名称:SPI初始化
** 功能描述:
** 输　入:
** 输　出:
** 注  释:
***********************************************************************************************/
void SPI_INIT(void)
{
	EALLOW;
//--------------------------------------------------
    SpibRegs.SPICCR.all =0x0047;   // Reset on, rising edge, 8-bit char bits
    SpibRegs.SPICTL.all =0x0006;   // Enable master mode, normal phase,
    SpibRegs.SPIPRI.all =0x0000;   // enable talk, and SPI int disabled.
    SpibRegs.SPIBRR.all =0x0000;   //12.5MHZ
    SpibRegs.SPICCR.all =0x00c7;   // Relinquish SPI from Reset
    SpibRegs.SPIPRI.bit.FREE = 1;  // Set so breakpoints don't disturb xmission

    SpiaRegs.SPICCR.all=0x0047;   // reset on, falling edge output, raising edge input, 16-bits
    SpiaRegs.SPICTL.all=0x0006;   //master mode, output enabled, normal opration, no int
    SpiaRegs.SPIPRI.all=0x0000;    // enable talk, and SPI int disabled.// enable talk(Enables transmission For the 4-pin option),disable int.
    SpiaRegs.SPIBRR.all=0x0000;   //12.5MHZ
    SpiaRegs.SPICCR.all=0x00c7;   // SPI exit from Reset ,  SPI High Speed mode enabled
    SpiaRegs.SPIPRI.bit.FREE=1;   // continue SPI operation when suspend occurred
    EDIS;
}
/**********************************************************************************************
** 函数名称:SPI初始化
** 功能描述:
** 输　入:
** 输　出:
** 注  释:
***********************************************************************************************/
void EPWM_INIT(void)
{
	EALLOW;
//---------------------------------------------------
    EPwm1Regs.TBPRD = 0xFFFF;             // Set timer period 801 TBCLKs
    EPwm1Regs.TBPHS.bit.TBPHS = 0x0000;  // Phase is 0
    EPwm1Regs.TBCTR = 0x0000;             // Clear counter
    // Set Compare values
    EPwm1Regs.CMPA.bit.CMPA = 0xFFFF;
    EPwm1Regs.CMPB.bit.CMPB = 0xFFFF;
    // Setup counter mode
    EPwm1Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN;      // Count up and down
    EPwm1Regs.TBCTL.bit.PHSEN = TB_DISABLE;        // Disable phase loading
    EPwm1Regs.TBCTL.bit.PRDLD = TB_SHADOW;      // shadow
    EPwm1Regs.TBCTL.bit.SYNCOSEL = TB_CTR_ZERO;     // CTR=0
    EPwm1Regs.TBCTL.bit.HSPCLKDIV = TB_DIV2;    //50M:100/(2*1)
    EPwm1Regs.TBCTL.bit.CLKDIV = TB_DIV1;
    EPwm1Regs.CMPCTL.bit.SHDWAMODE = 0;   // Shadow mode
    EPwm1Regs.CMPCTL.bit.SHDWBMODE = 0;   // Shadow mode
    EPwm1Regs.CMPCTL.bit.LOADAMODE = 0;   // Load on zero
    EPwm1Regs.CMPCTL.bit.LOADBMODE = 0;   // Load on zero
    EPwm1Regs.AQCTLA.bit.CAU = AQ_SET;
    EPwm1Regs.AQCTLA.bit.CAD = AQ_CLEAR;
    EPwm1Regs.AQCTLA.bit.CBU = AQ_NO_ACTION;
    EPwm1Regs.AQCTLA.bit.CBD = AQ_NO_ACTION;
    EPwm1Regs.AQCTLA.bit.PRD = AQ_NO_ACTION;
    EPwm1Regs.AQCTLA.bit.ZRO = AQ_NO_ACTION;
    EPwm1Regs.AQCTLB.bit.CAU = AQ_CLEAR;
    EPwm1Regs.AQCTLB.bit.CAD = AQ_SET;
    EPwm1Regs.AQCTLB.bit.CBU = AQ_NO_ACTION;
    EPwm1Regs.AQCTLB.bit.CBD = AQ_NO_ACTION;
    EPwm1Regs.AQCTLB.bit.PRD = AQ_NO_ACTION;
    EPwm1Regs.AQCTLB.bit.ZRO = AQ_NO_ACTION;
    EPwm1Regs.DBCTL.bit.OUT_MODE = DB_FULL_ENABLE;
    EPwm1Regs.DBCTL.bit.POLSEL   = DB_ACTV_HIC;
    EPwm1Regs.DBCTL.bit.IN_MODE  = DBA_ALL;
    EPwm1Regs.ETSEL.bit.INTEN = 1;       //1=Enable ePWM Interrupt Generration
    EPwm1Regs.ETSEL.bit.INTSEL = 0x01;   //计数到0触发中断
    EPwm1Regs.ETPS.bit.INTPRD =  0x01;    //Generate an interrupt every event

    EPwm2Regs.TBPRD = 0xFFFF;             // Set timer period 801 TBCLKs
    EPwm2Regs.TBPHS.bit.TBPHS = 0x0000;  // Phase is 0
    EPwm2Regs.TBCTR = 0x0000;             // Clear counter
    EPwm2Regs.CMPA.bit.CMPA = 0xFFFF;    // Set compare A value
    EPwm2Regs.CMPB.bit.CMPB  = 0xFFFF;              // Set compare A value
    EPwm2Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN;      // Count up and down
    EPwm2Regs.TBCTL.bit.PHSEN = TB_ENABLE;        // enable phase loading
    EPwm2Regs.TBCTL.bit.PRDLD = TB_SHADOW;        // shadow
    EPwm2Regs.TBCTL.bit.SYNCOSEL = TB_SYNC_IN;     // SYNCI
    EPwm2Regs.TBCTL.bit.HSPCLKDIV = TB_DIV2;    // Clock ratio to SYSCLKOUT/2
    EPwm2Regs.TBCTL.bit.CLKDIV = TB_DIV1;
    EPwm2Regs.CMPCTL.bit.SHDWAMODE = 0;   // Shadow mode
    EPwm2Regs.CMPCTL.bit.SHDWBMODE = 0;
    EPwm2Regs.CMPCTL.bit.LOADAMODE = 0;   // Load on period
    EPwm2Regs.CMPCTL.bit.LOADBMODE = 0;
    EPwm2Regs.AQCTLA.bit.CAU = AQ_SET;
    EPwm2Regs.AQCTLA.bit.CAD = AQ_CLEAR;
    EPwm2Regs.AQCTLA.bit.CBU = AQ_NO_ACTION;
    EPwm2Regs.AQCTLA.bit.CBD = AQ_NO_ACTION;
    EPwm2Regs.AQCTLA.bit.PRD = AQ_NO_ACTION;
    EPwm2Regs.AQCTLA.bit.ZRO = AQ_NO_ACTION;
    EPwm2Regs.AQCTLB.bit.CAU = AQ_CLEAR;
    EPwm2Regs.AQCTLB.bit.CAD = AQ_SET;
    EPwm2Regs.AQCTLB.bit.CBU = AQ_NO_ACTION;
    EPwm2Regs.AQCTLB.bit.CBD = AQ_NO_ACTION;
    EPwm2Regs.AQCTLB.bit.PRD = AQ_NO_ACTION;
    EPwm2Regs.AQCTLB.bit.ZRO = AQ_NO_ACTION;

    // Active high complementary PWMs - Setup the deadband
    EPwm2Regs.DBCTL.bit.OUT_MODE= DB_FULL_ENABLE;
    EPwm2Regs.DBCTL.bit.POLSEL  = DB_ACTV_HIC;
    EPwm2Regs.DBCTL.bit.IN_MODE = DBA_ALL;

    EPwm3Regs.TBPRD = 0xFFFF;             // Set timer period 801 TBCLKs
    EPwm3Regs.TBPHS.bit.TBPHS = 0x0000;  // Phase is 0
    EPwm3Regs.TBCTR = 0x0000;             // Clear counter
    // Set Compare values
    EPwm3Regs.CMPA.bit.CMPA = 0xFFFF;    // Set compare A value
    EPwm3Regs.CMPB.bit.CMPB  = 0xFFFF;              // Set compare A value
    // Setup counter mode
    EPwm3Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN;      // Count up and down
    EPwm3Regs.TBCTL.bit.PHSEN = TB_ENABLE;        // enable phase loading
    EPwm3Regs.TBCTL.bit.PRDLD = TB_SHADOW;        // shadow
    EPwm3Regs.TBCTL.bit.SYNCOSEL = TB_SYNC_IN;     // SYNCI
    EPwm3Regs.TBCTL.bit.HSPCLKDIV = TB_DIV2;    // Clock ratio to SYSCLKOUT/2
    EPwm3Regs.TBCTL.bit.CLKDIV = TB_DIV1;
    // Setup shadowing
    EPwm3Regs.CMPCTL.bit.SHDWAMODE = 0;   // Shadow mode
    EPwm3Regs.CMPCTL.bit.SHDWBMODE = 0;
    EPwm3Regs.CMPCTL.bit.LOADAMODE = 0;   // Load on period
    EPwm3Regs.CMPCTL.bit.LOADBMODE = 0;
    //Setup compare way
    EPwm3Regs.AQCTLA.bit.CAU =  AQ_SET;
    EPwm3Regs.AQCTLA.bit.CAD =  AQ_CLEAR;
    EPwm3Regs.AQCTLA.bit.CBU =  AQ_NO_ACTION;
    EPwm3Regs.AQCTLA.bit.CBD =  AQ_NO_ACTION;
    EPwm3Regs.AQCTLA.bit.PRD =  AQ_NO_ACTION;
    EPwm3Regs.AQCTLA.bit.ZRO =  AQ_NO_ACTION;
    EPwm3Regs.AQCTLB.bit.CAU =  AQ_CLEAR;
    EPwm3Regs.AQCTLB.bit.CAD =  AQ_SET;
    EPwm3Regs.AQCTLB.bit.CBU =  AQ_NO_ACTION;
    EPwm3Regs.AQCTLB.bit.CBD =  AQ_NO_ACTION;
    EPwm3Regs.AQCTLB.bit.PRD =  AQ_NO_ACTION;
    EPwm3Regs.AQCTLB.bit.ZRO =  AQ_NO_ACTION;

    // Active high complementary PWMs - Setup the deadband
    EPwm3Regs.DBCTL.bit.OUT_MODE= DB_FULL_ENABLE;
    EPwm3Regs.DBCTL.bit.POLSEL  = DB_ACTV_HIC;
    EPwm3Regs.DBCTL.bit.IN_MODE = DBA_ALL;

    EPwm4Regs.TBPRD = 0xFFFF;             // Set timer period 801 TBCLKs
    EPwm4Regs.TBPHS.bit.TBPHS = 0x0000;  // Phase is 0
    EPwm4Regs.TBCTR = 0x0000;             // Clear counter
    // Set Compare values
    EPwm4Regs.CMPA.bit.CMPA = 0xFFFF;    // Set compare A value
    EPwm4Regs.CMPB.bit.CMPB  = 0xFFFF;              // Set compare A value
    // Setup counter mode
    EPwm4Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN;      // Count up and down
    EPwm4Regs.TBCTL.bit.PHSEN = TB_ENABLE;        // enable phase loading
    EPwm4Regs.TBCTL.bit.PRDLD = TB_SHADOW;       // shadow
    EPwm4Regs.TBCTL.bit.SYNCOSEL = TB_SYNC_IN;     // SYNCI
    EPwm4Regs.TBCTL.bit.HSPCLKDIV = TB_DIV2;    // Clock ratio to SYSCLKOUT/2
    EPwm4Regs.TBCTL.bit.CLKDIV = TB_DIV1;
    // Setup shadowing
    EPwm4Regs.CMPCTL.bit.SHDWAMODE = 0;   // Shadow mode
    EPwm4Regs.CMPCTL.bit.SHDWBMODE = 0;
    EPwm4Regs.CMPCTL.bit.LOADAMODE = 0;   // Load on period
    EPwm4Regs.CMPCTL.bit.LOADBMODE = 0;
    //Setup compare way
    EPwm4Regs.AQCTLA.bit.CAU = AQ_SET;
    EPwm4Regs.AQCTLA.bit.CAD = AQ_CLEAR;
    EPwm4Regs.AQCTLA.bit.CBU = AQ_NO_ACTION;
    EPwm4Regs.AQCTLA.bit.CBD = AQ_NO_ACTION;
    EPwm4Regs.AQCTLA.bit.PRD = AQ_NO_ACTION;
    EPwm4Regs.AQCTLA.bit.ZRO = AQ_NO_ACTION;
    EPwm4Regs.AQCTLB.bit.CAU = AQ_CLEAR;
    EPwm4Regs.AQCTLB.bit.CAD = AQ_SET;
    EPwm4Regs.AQCTLB.bit.CBU = AQ_NO_ACTION;
    EPwm4Regs.AQCTLB.bit.CBD = AQ_NO_ACTION;
    EPwm4Regs.AQCTLB.bit.PRD = AQ_NO_ACTION;
    EPwm4Regs.AQCTLB.bit.ZRO = AQ_NO_ACTION;

    // Active high complementary PWMs - Setup the deadband
    EPwm4Regs.DBCTL.bit.OUT_MODE = DB_FULL_ENABLE;
    EPwm4Regs.DBCTL.bit.POLSEL   = DB_ACTV_HIC;
    EPwm4Regs.DBCTL.bit.IN_MODE  = DBA_ALL;

    EPwm5Regs.TBPRD = 0xFFFF;             // Set timer period 801 TBCLKs
    EPwm5Regs.TBPHS.bit.TBPHS = 0x0000;  // Phase is 0
    EPwm5Regs.TBCTR = 0x0000;             // Clear counter
    // Set Compare values
    EPwm5Regs.CMPA.bit.CMPA = 0xFFFF;    // Set compare A value
    EPwm5Regs.CMPB.bit.CMPB  = 0xFFFF;              // Set compare A value
    // Setup counter mode
    EPwm5Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN;      // Count up and down
    EPwm5Regs.TBCTL.bit.PHSEN = TB_ENABLE;        // enable phase loading
    EPwm5Regs.TBCTL.bit.PRDLD = TB_SHADOW;       // shadow
    EPwm5Regs.TBCTL.bit.SYNCOSEL = TB_SYNC_IN;     // SYNCI
    EPwm5Regs.TBCTL.bit.HSPCLKDIV = TB_DIV2;    // Clock ratio to SYSCLKOUT/2
    EPwm5Regs.TBCTL.bit.CLKDIV = TB_DIV1;
    // Setup shadowing
    EPwm5Regs.CMPCTL.bit.SHDWAMODE = 0;   // Shadow mode
    EPwm5Regs.CMPCTL.bit.SHDWBMODE = 0;
    EPwm5Regs.CMPCTL.bit.LOADAMODE = 0;   // Load on period
    EPwm5Regs.CMPCTL.bit.LOADBMODE = 0;
    //Setup compare way
    EPwm5Regs.AQCTLA.bit.CAU =  AQ_SET;
    EPwm5Regs.AQCTLA.bit.CAD =  AQ_CLEAR;
    EPwm5Regs.AQCTLA.bit.CBU =  AQ_NO_ACTION;
    EPwm5Regs.AQCTLA.bit.CBD =  AQ_NO_ACTION;
    EPwm5Regs.AQCTLA.bit.PRD =  AQ_NO_ACTION;
    EPwm5Regs.AQCTLA.bit.ZRO =  AQ_NO_ACTION;
    EPwm5Regs.AQCTLB.bit.CAU =  AQ_CLEAR;
    EPwm5Regs.AQCTLB.bit.CAD =  AQ_SET;
    EPwm5Regs.AQCTLB.bit.CBU =  AQ_NO_ACTION;
    EPwm5Regs.AQCTLB.bit.CBD =  AQ_NO_ACTION;
    EPwm5Regs.AQCTLB.bit.PRD =  AQ_NO_ACTION;
    EPwm5Regs.AQCTLB.bit.ZRO =  AQ_NO_ACTION;

    // Active high complementary PWMs - Setup the deadband
    EPwm5Regs.DBCTL.bit.OUT_MODE= DB_FULL_ENABLE;
    EPwm5Regs.DBCTL.bit.POLSEL  = DB_ACTV_HIC;
    EPwm5Regs.DBCTL.bit.IN_MODE = DBA_ALL;


     EPwm6Regs.TBPRD = 0xFFFF;             // Set timer period 801 TBCLKs
     EPwm6Regs.TBPHS.bit.TBPHS = 0x0000;  // Phase is 0
     EPwm6Regs.TBCTR = 0x0000;             // Clear counter
    // Set Compare values
     EPwm6Regs.CMPA.bit.CMPA = 0xFFFF;    // Set compare A value
     EPwm6Regs.CMPB.bit.CMPB  = 0xFFFF;              // Set compare A value
    // Setup counter mode
     EPwm6Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN;      // Count up and down
     EPwm6Regs.TBCTL.bit.PHSEN = TB_ENABLE;        // enable phase loading
     EPwm6Regs.TBCTL.bit.PRDLD = TB_SHADOW;       // shadow
     EPwm6Regs.TBCTL.bit.SYNCOSEL = TB_SYNC_IN;     // SYNCI
     EPwm6Regs.TBCTL.bit.HSPCLKDIV = TB_DIV2;    // Clock ratio to SYSCLKOUT/2
     EPwm6Regs.TBCTL.bit.CLKDIV = TB_DIV1;
    // Setup shadowing
     EPwm6Regs.CMPCTL.bit.SHDWAMODE = 0;   // Shadow mode
     EPwm6Regs.CMPCTL.bit.SHDWBMODE = 0;
     EPwm6Regs.CMPCTL.bit.LOADAMODE = 0;   // Load on period
     EPwm6Regs.CMPCTL.bit.LOADBMODE = 0;
    //Setup compare way
     EPwm6Regs.AQCTLA.bit.CAU =  AQ_SET;
     EPwm6Regs.AQCTLA.bit.CAD =  AQ_CLEAR;
     EPwm6Regs.AQCTLA.bit.CBU =  AQ_NO_ACTION;
     EPwm6Regs.AQCTLA.bit.CBD =  AQ_NO_ACTION;
     EPwm6Regs.AQCTLA.bit.PRD =  AQ_NO_ACTION;
     EPwm6Regs.AQCTLA.bit.ZRO =  AQ_NO_ACTION;
     EPwm6Regs.AQCTLB.bit.CAU =  AQ_CLEAR;
     EPwm6Regs.AQCTLB.bit.CAD =  AQ_SET;
     EPwm6Regs.AQCTLB.bit.CBU =  AQ_NO_ACTION;
     EPwm6Regs.AQCTLB.bit.CBD =  AQ_NO_ACTION;
     EPwm6Regs.AQCTLB.bit.PRD =  AQ_NO_ACTION;
     EPwm6Regs.AQCTLB.bit.ZRO =  AQ_NO_ACTION;

     EPwm6Regs.DBCTL.bit.OUT_MODE  = DB_FULL_ENABLE;
     EPwm6Regs.DBCTL.bit.POLSEL    = DB_ACTV_HIC;
     EPwm6Regs.DBCTL.bit.IN_MODE   = DBA_ALL;

     EPwm1Regs.AQSFRC.bit.RLDCSF=3;      //load immediately
     EPwm2Regs.AQSFRC.bit.RLDCSF=3;      //load immediately
     EPwm3Regs.AQSFRC.bit.RLDCSF=3;      //load immediately
     EPwm4Regs.AQSFRC.bit.RLDCSF=3;      //load immediately
     EPwm5Regs.AQSFRC.bit.RLDCSF=3;      //load immediately
     EPwm6Regs.AQSFRC.bit.RLDCSF=3;      //load immediately

     EPwm1Regs.AQCSFRC.bit.CSFA = 3;
     EPwm3Regs.AQCSFRC.bit.CSFA = 3;
     EPwm5Regs.AQCSFRC.bit.CSFA = 3;
     EPwm2Regs.AQCSFRC.bit.CSFA = 3;
     EPwm4Regs.AQCSFRC.bit.CSFA = 3;
     EPwm6Regs.AQCSFRC.bit.CSFA = 3;

     EPwm1Regs.TZSEL.bit.OSHT1 = 1;//enable TZ1 as a one-shot trip source for this ePWM module
     EPwm2Regs.TZSEL.bit.OSHT1 = 1;//enable TZ1 as a one-shot trip source for this ePWM module
     EPwm3Regs.TZSEL.bit.OSHT1 = 1;//enable TZ1 as a one-shot trip source for this ePWM module
     EPwm4Regs.TZSEL.bit.OSHT1 = 1;//enable TZ1 as a one-shot trip source for this ePWM module
     EPwm5Regs.TZSEL.bit.OSHT1 = 1;//enable TZ1 as a one-shot trip source for this ePWM module
     EPwm6Regs.TZSEL.bit.OSHT1 = 1;//enable TZ1 as a one-shot trip source for this ePWM module

     EPwm1Regs.TZCTL.bit.TZA = 2;  //T1 when a trip event occurs,force output ePWM A to a low state
     EPwm2Regs.TZCTL.bit.TZB = 2;  //T4 -------------------------------------------B---------------
     EPwm3Regs.TZCTL.bit.TZA = 2;  //T5 when a trip event occurs,force output ePWM A to a low state
     EPwm4Regs.TZCTL.bit.TZB = 2;  //T8 -------------------------------------------B---------------
     EPwm5Regs.TZCTL.bit.TZA = 2;  //T9 when a trip event occurs,force output ePWM A to a low state
     EPwm6Regs.TZCTL.bit.TZB = 2;  //T12 -------------------------------------------B---------------
     EPwm1Regs.TZCTL.bit.TZB = 2;  //T2 -------------------------------------------B---------------
     EPwm2Regs.TZCTL.bit.TZA = 2;  //T3 when a trip event occurs,do nothing to output ePWM
     EPwm3Regs.TZCTL.bit.TZB = 2;  //T6 -------------------------------------------B---------------
     EPwm4Regs.TZCTL.bit.TZA = 2;  //T7 when a trip event occurs,do nothing to output ePWM
     EPwm5Regs.TZCTL.bit.TZB = 2;  //T8 -------------------------------------------B---------------
     EPwm6Regs.TZCTL.bit.TZA = 2;  //T9 when a trip event occurs,do nothing to output ePWM
    //允许TZ中断
     EPwm1Regs.TZEINT.bit.OST = 1;
     EPwm2Regs.TZEINT.bit.OST = 1;
     EPwm3Regs.TZEINT.bit.OST = 1;
     EPwm4Regs.TZEINT.bit.OST = 1;
     EPwm5Regs.TZEINT.bit.OST = 1;
     EPwm6Regs.TZEINT.bit.OST = 1;
     EDIS;

     EALLOW;
     SyncSocRegs.SYNCSELECT.bit.EPWM4SYNCIN=0; //与EPWM1
     SyncSocRegs.SYNCSELECT.bit.EPWM10SYNCIN=2;//与EPWM7同步
     EDIS;
}
/**********************************************************************************************
** 函数名称: I2C初始化
** 功能描述:
** 输　入:
** 输　出:
** 注  释:
***********************************************************************************************/
void InitPeripheral(void)
{
	RAM_INIT();
	EMIF_INIT();
	Timer_INIT();
	SCI_INIT();
	SPI_INIT();
	EPWM_INIT();
	I2C_INIT();
}
