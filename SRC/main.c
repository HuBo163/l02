#include "F28x_Project.h"
#include "InitCpuBase.h"
#include "mb.h"
#include "f_SFRA.h"
#include "f_data_enterchange.h"
//============================================================================
//interrupt void CpuTimer0Isr(void);

extern void (*Alpha_State_Ptr)(void);
extern void Task_Init(void);

/*-----------------------函数申明-----------------------------*/
void MemCopy(Uint16 *SourceAddr, Uint16* SourceEndAddr, Uint16* DestAddr);

void main(void)
  {
//-----------------------------------------------------
     SysInit();
     Task_Init();
   //  setupSFRA();
     data_init();
     PwmOff();
     PwmPFCOff();

//----------------------------------------------------testing---------------------------------------------------
  //   PwmOn();
  //   PwmPFCOn();

 //    PhaseTypeRSTPWMCmp(1000,1000,1000);
  //   PhaseTypeUVWPWMCmp(2000,1500,3000);
//----------------------------------------------------testing---------------------------------------------------
    for(;;)
    {
          (*Alpha_State_Ptr)();

    }
//--------------------------------------------------------------
}

//========================================
//=================================
void AD_DELAY(Uint16 times)//延时3ns时间
{
	 Uint16 i;
	 for(i=0;i<times;i++)
	 asm(" nop");
}
//==========================================================================
void MemCopy(Uint16 *SourceAddr, Uint16* SourceEndAddr, Uint16* DestAddr)
{
    while(SourceAddr < SourceEndAddr)
    {
       *DestAddr++ = *SourceAddr++;
    }
    return;
}

