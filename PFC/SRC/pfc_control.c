#include"pfc_control.h"
#include "PwmDriver.h"
#include "f_SFRA.h"

//volatile float test_in;
//volatile float test_out;
//volatile float test_ref = -0.05f;
static Uint16  gpfc_begin = 0;
static Uint16  gpfc_excursion_flag=0;

//输入电压巴特沃夫IIR数字滤波器
Butterworth_II_DEF PFC_IB = {
    .A[0] = 1, .A[1] = -1.98676, .A[2] = 0.987,
    .B[0] = 0.0065, .B[1] = 0, .B[2] = -0.0065,
};


//输入电压巴特沃夫IIR数字滤波器
Butterworth_II_DEF PFC_UAB = {
    .A[0] = 1, .A[1] = -1.98676, .A[2] = 0.987,
    .B[0] = 0.0065, .B[1] = 0, .B[2] = -0.0065,
};

//输入电压巴特沃夫IIR数字滤波器
Butterworth_II_DEF PFC_UBC = {
      .A[0] = 1, .A[1] = -1.98676, .A[2] = 0.987,
      .B[0] = 0.0065, .B[1] = 0, .B[2] = -0.0065,
};


//母线电压滤波参数设计 20k  fass-500Hz fstop 60000 butterworth
Butterworth_II_DEF PFC_UDC = {
    .A[0] = 1, .A[1] = -1.616768122, .A[2] = 0.6792111397,
    .B[0] = 0.01561076194, .B[1] = 0.03122152388, .B[2] = 0.01561076194,
    .W1[0]=0,.W1[1]=0,.W2[0]=0,.W2[1]=0,
};


//抑制三相不平衡
Butterworth_II_DEF PFC_ialpha =
{
  .A[0] = 1, .A[1] =  0.6202040911, .A[2] = 0.2404082119,
  .B[0] = 0.4651530683, .B[1] =  0.9303061366, .B[2] = 0.4651530683,
  .W1[0]=1.0f,.W1[1]=1.0f,.W2[0]=1.0f,.W2[1]=1.0f,
};

Butterworth_II_DEF PFC_ibeta =
{
   .A[0] = 1, .A[1] =  0.6202040911, .A[2] = 0.2404082119,
   .B[0] = 0.4651530683, .B[1] =  0.9303061366, .B[2] = 0.4651530683,
   .W1[0]=0,.W1[1]=0,.W2[0]=0,.W2[1]=0,
};


// INIT PID coefficient

PI_CTRL_DATA_DEF vloop_pi = {
    0.5f,
    0.0f,
    0.0f,
    VLOOP_KP,
    VLOOP_KI,
    Vloop_UP,
    Vloop_LOW,
    0, 0, 0,
    0,
};

PI_CTRL_DATA_DEF d_ctrl_pi = {
    0.0f,
    0.0f,
    0.0f,
    DLOOP_KP,
    DLOOP_KI,
    Dloop_UP,
    Dloop_LOW,
    0, 0, 0,
    0,
};

PI_CTRL_DATA_DEF q_ctrl_pi = {
    0.0f,
    0.0f,
    0.0f,
    QLOOP_KP,
    QLOOP_KI,
    Qloop_UP,
    Qloop_LOW,
    0, 0, 0,
    0,
};

PFC_DISPLAY_REF gpfc_display_linVOL = {
                                        .head = 0,
                                      };

PFC_DISPLAY_REF1 gpfc_display_control;

PFC_EXCURSION_STRUCT_DEF gpfc_ADC_EXCURSION = {  .IA=PFC_ADFULL_HALF, .IB=PFC_ADFULL_HALF, .IC=PFC_ADFULL_HALF,
                                                 .UBA=PFC_ADFULL_HALF,.UBC=PFC_ADFULL_HALF, .Count = 8000,
                                               };

//锁相环控制相关全局变量
ABC_line_phase_DEF gpfc_linev_phase  = {0, 0, 0, 0, 0, 0};                    //线电压转换为相电压相关变量
ABC_REAL_VALUE_DEF gpfc_real_iac     = {0, 0, 0};                             //电流运行实际值
INTER_DEF          gpfc_inter_spll   = {VLOOP_TS, 1, 0, 0};                   //锁相环dsrf相关参数
ABC_ab_dq_DEF      gpfc_vac          = {0, 0, 0, 0, 0, 0, 0,0.666f};          //abc至dq 电压参数
ABC_ab_dq_DEF      gpfc_iac          = {0, 0, 0, 0, 0, 0, 0,0.024184f};       //abc至dq 电流参数
ABC_ab_dq_DEF      gpfc_ctrl_ac      = {0, 0, 0, 0, 0, 0, 0};                 //abc至dq 控制变量
sinwt_coswt_DEF    gpfc_wt           = {0, 0};                                //正弦与余弦定义相关变量
DSRF_SPLL_DEF      gpfc_dsrf_spllabc = {0, 0, 0, 0};                          //锁相环控制相关变量
PFC_ADCRESULT_REF  gpfc_ad_result    = {0, 0, 0, 0, 0, 0};                    //pfc相关参数变量的ad值
PFC_ADCRESULT_REF  gpfc_adreal_result= {0, 0, 0, 0, 0, 0};                    //pfc相关参数变量的ad减去零漂值
PSM_SVGEN          gpfc_svgen        = SVGEN_DEFAULTS;                        //SVPWM发生器
SPLL_3PH_SOGI_FLL  gpfc_spll_data;

float       vdc_sen   =  1;
float       vdc_coff  =  0.299f;
float  vdc_sen_slow   =  1;

float       vdc_set   =  650;
float       tsw       = 5000-1;

float sw_a_on = 0;
float sw_b_on = 0;
float sw_c_on = 0;

float vloop_outer = 0;
float d_ctrl_feed = 0;
float q_ctrl_feed = 0;
float pfc_temp = 0.0;


void (*PFC_PWM)(uint16 RCmp, uint16 SCmp, uint16 TCmp);
void (*PFC_AD)(PFC_ADCRESULT_REF* ad_result);


void Set_PfcOn(void)
{
  gpfc_begin=1;
}

void Set_PfcOff(void)
{
  gpfc_begin=0;
}


Uint16 get_pfcexcur_flag(void)
{
    return (Uint16)gpfc_excursion_flag;
}


extern BOOL ckeck_data_normal(void);
/************************************************************
    计算PFC_AD零漂程序
************************************************************/
BOOL GetPfc_ADExcursion(PFC_ADCRESULT_REF  ad_result)
{
    static BOOL cal_over = FALSE;
    static BOOL power_on_check=FALSE;

    static uint16 times = 0;

    power_on_check = ckeck_data_normal();

    if(power_on_check && (!cal_over))
    {
        times ++ ;
        if( times >= gpfc_ADC_EXCURSION.Count )
        {
            times = 0;

            gpfc_ADC_EXCURSION.ErrUBA  = gpfc_ADC_EXCURSION.UBA/gpfc_ADC_EXCURSION.Count;
            gpfc_ADC_EXCURSION.ErrUBC  = gpfc_ADC_EXCURSION.UBC/gpfc_ADC_EXCURSION.Count;
            gpfc_ADC_EXCURSION.ErrIA  = gpfc_ADC_EXCURSION.IA/gpfc_ADC_EXCURSION.Count;
            gpfc_ADC_EXCURSION.ErrIB  = gpfc_ADC_EXCURSION.IB/gpfc_ADC_EXCURSION.Count;
            gpfc_ADC_EXCURSION.ErrIC  = gpfc_ADC_EXCURSION.IC/gpfc_ADC_EXCURSION.Count;
            gpfc_ADC_EXCURSION.ErrVdc  = gpfc_ADC_EXCURSION.Vdc/gpfc_ADC_EXCURSION.Count;
  //          gpfc_ADC_EXCURSION.ErrTemp = gpfc_ADC_EXCURSION.Temp/gpfc_ADC_EXCURSION.Count;
            if( (gpfc_ADC_EXCURSION.ErrUBA<PFC_ADEXCURSION_MIN)||(gpfc_ADC_EXCURSION.ErrUBA>PFC_ADEXCURSION_MAX)||
                (gpfc_ADC_EXCURSION.ErrUBC<PFC_ADEXCURSION_MIN)||(gpfc_ADC_EXCURSION.ErrUBC>PFC_ADEXCURSION_MAX)||
                (gpfc_ADC_EXCURSION.ErrIA<PFC_ADEXCURSION_MIN) ||(gpfc_ADC_EXCURSION.ErrIA>PFC_ADEXCURSION_MAX)||
                (gpfc_ADC_EXCURSION.ErrIB<PFC_ADEXCURSION_MIN) ||(gpfc_ADC_EXCURSION.ErrIB>PFC_ADEXCURSION_MAX)||
		(gpfc_ADC_EXCURSION.ErrIC<PFC_ADEXCURSION_MIN) ||(gpfc_ADC_EXCURSION.ErrIC>PFC_ADEXCURSION_MAX))
            {
                gpfc_ADC_EXCURSION.UBA = 0;
                gpfc_ADC_EXCURSION.UBC = 0;
                gpfc_ADC_EXCURSION.IA =  0;
                gpfc_ADC_EXCURSION.IB =  0;
                gpfc_ADC_EXCURSION.IC =  0;

                gpfc_ADC_EXCURSION.ErrUBA  = PFC_ADFULL_HALF;
		gpfc_ADC_EXCURSION.ErrUBC  = PFC_ADFULL_HALF;
		gpfc_ADC_EXCURSION.ErrIA  =  PFC_ADFULL_HALF;
		gpfc_ADC_EXCURSION.ErrIB  =  PFC_ADFULL_HALF;
		gpfc_ADC_EXCURSION.ErrIC  =  PFC_ADFULL_HALF;
		gpfc_ADC_EXCURSION.ErrVdc  = PFC_ADFULL_HALF;

                cal_over = FALSE;
            }

            else
            cal_over = TRUE;
        }
        gpfc_ADC_EXCURSION.UBA  += ad_result.UBA;
        gpfc_ADC_EXCURSION.UBC  += ad_result.UBC;
        gpfc_ADC_EXCURSION.IA   += ad_result.IA;
        gpfc_ADC_EXCURSION.IB   += ad_result.IB;
        gpfc_ADC_EXCURSION.IC   += ad_result.IC;
        gpfc_ADC_EXCURSION.Vdc  += ad_result.Vdc;
     //   gpfc_ADC_EXCURSION.Temp += ad_result.Temp;

    }

    if(FALSE==power_on_check)
      {
	gpfc_ADC_EXCURSION.UBA = 0;
	gpfc_ADC_EXCURSION.UBC = 0;
	gpfc_ADC_EXCURSION.IA =  0;
	gpfc_ADC_EXCURSION.IB =  0;
	gpfc_ADC_EXCURSION.IC =  0;

	gpfc_ADC_EXCURSION.ErrUBA  = PFC_ADFULL_HALF;
	gpfc_ADC_EXCURSION.ErrUBC  = PFC_ADFULL_HALF;
	gpfc_ADC_EXCURSION.ErrIA  =  PFC_ADFULL_HALF;
	gpfc_ADC_EXCURSION.ErrIB  =  PFC_ADFULL_HALF;
	gpfc_ADC_EXCURSION.ErrIC  =  PFC_ADFULL_HALF;
	gpfc_ADC_EXCURSION.ErrVdc  = PFC_ADFULL_HALF;

	cal_over = FALSE;
      }

    gpfc_excursion_flag = cal_over;
    return cal_over;
}


/*******************************************************************************************
Function Name:   Elliptic_V_filter
Version:         V1.0
Input:
Output:
Description:     五阶椭圆滤波器
History:
*******************************************************************************************/
float Elliptic_V_filter(float xn, Elliptic_V_DEF* PFC_idq)
{
    unsigned int i=0;
    float  yn = 0;
    yn = PFC_idq->B[0]*xn + PFC_idq->W[0] ;

    for(i=1;i<2;i++)
    {
        PFC_idq->W[i-1] = xn*PFC_idq->B[i] + PFC_idq->W[i] - PFC_idq->A[i]*yn;
    }

    PFC_idq->W[1] = PFC_idq->B[2]*xn -PFC_idq->A[2]*yn;

    return yn;
}

/*******************************************************************************************
Function Name:   pfcdata_display_refresh
Version:         V1.0
Input:
Output:
Description:     pfc测试数据刷新
History:
*******************************************************************************************/
float Butterworth2_II_filter(float xn, Butterworth_II_DEF* PFC_U)
{
	float  yn = 0;
	yn = xn*PFC_U->B[0] + PFC_U->W2[1];
	yn = yn;
	PFC_U->W2[0] = xn*PFC_U->B[1] - yn*PFC_U->A[1] + PFC_U->W1[1];
	PFC_U->W2[1] = PFC_U->W2[0];
	PFC_U->W1[0] = xn*PFC_U->B[2] - yn*PFC_U->A[2];
	PFC_U->W1[1] = PFC_U->W1[0];
	return yn;
}

/*******************************************************************************************
Function Name:   pfcdata_display_refresh
Version:         V1.0
Input:
Output:
Description:     pfc测试数据刷新
History:
*******************************************************************************************/
void pfcdata_display_refresh(PFC_DISPLAY_REF* display_linVOL, PFC_DISPLAY_REF1* display_control)
{

   uint16 i,j=display_linVOL->head+1;

   if(j>=400)
       j=0;

   for(i=0;i<400;i++)
   {
       display_linVOL->A_DISPLAY[i]=display_linVOL->A[j];
       display_linVOL->B_DISPLAY[i]=display_linVOL->B[j];
       display_linVOL->C_DISPLAY[i]=display_linVOL->C[j];
	   
       display_control->alpha_DISPLAY[i] = display_control->alpha[j];
       display_control->beta_DISPLAY[i] = display_control->beta[j];
	   
       display_control->theta_DISPLAY[i]= display_control->theta[j];
	   
       j++;
       if(j>=400)
           j=0;
   }
}


void pfcdata_test1(void)
{
    pfcdata_display_refresh(&gpfc_display_linVOL, &gpfc_display_control);
}


/*******************************************************************************************
Function Name:   getpfc_adresult
Version:         V1.0
Input:           &gpfc_linev_phasev
Output:
Description:     pfc函数初始化
History:
*******************************************************************************************/
void Init_pfc(void (*PFC_PWM_Ptr)(uint16 RCmp, uint16 SCmp, uint16 TCmp), void (*PFC_GetAD)(PFC_ADCRESULT_REF* ))
{
    SPLL_3PH_SOGI_FLL_config(&gpfc_spll_data, 50.0f, 27624, 222.2826f, -222.034f, 0.5f, 27624);
    PFC_PWM = PFC_PWM_Ptr;
    PFC_AD  = PFC_GetAD;
}

#pragma CODE_SECTION(abcline_abcphase, ".TI.ramfunc")
/*******************************************************************************************
Function Name:   abcline_abcphase
Version:         V1.0
Input:           &gpfc_linev_phasev
Output:          gpfc_linev_phasev转换后的结果保存
Description:     根据ABC相线电压计算出ABC三相相电压
History:
*******************************************************************************************/
void abcline_abcphase(ABC_line_phase_DEF *v)
{
    v->CA = -(v->AB+v->BC);  //计算每一相线电压

    v->AN = 1*(v->AB-v->CA)/3;
    v->BN = 1*(v->BC-v->AB)/3;
    v->CN = 1*(v->CA-v->BC)/3;
}


#pragma CODE_SECTION(getpfc_adresult, ".TI.ramfunc")
/*******************************************************************************************
Function Name:   getpfc_adresult
Version:         V1.0
Input:           &gpfc_linev_phasev
Output:          adc结果转换值
Description:     获取adc电压
输入电压与母线电压倍数：
电压->AD: (10/3760)*1/2(偏置1.65V)*1/3.3*4095=1.65
交流 AD->电压     1/1.65=0.606
直流 AD->电压     1/1.65/2=0.303*0.98(AD内阻分压)=0.29694
逆变电流比例倍数：
电流-AD：0.002(Rsense)*8*2.5(3k/1.2k)*1/2(偏置1.65V)*1/3.3*4095=24.818
AD->电流:    1/24.818=0.0402933

整流电流比例倍数：
电流-AD：0.002(Rsense)*8*4.25(5.1k/1.2k)*1/2(偏置1.65V)*1/3.3*4095=42.1909090909
AD->电流:    1/42.1909090909=0.0237017
*******************************************************************************************/
BOOL getpfc_adresult()
{
    static BOOL excursion_endflag = FALSE;


    PFC_AD(&gpfc_ad_result);

    pfc_temp = gpfc_ad_result.Temp;


    excursion_endflag = GetPfc_ADExcursion( gpfc_ad_result);

    gpfc_adreal_result.UBA  =gpfc_ADC_EXCURSION.ErrUBA - gpfc_ad_result.UBA ;
    gpfc_adreal_result.UBC  =gpfc_ad_result.UBC - gpfc_ADC_EXCURSION.ErrUBC;
    gpfc_adreal_result.IA   =gpfc_ad_result.IA - gpfc_ADC_EXCURSION.ErrIA;
    gpfc_adreal_result.IB   =gpfc_ad_result.IB - gpfc_ADC_EXCURSION.ErrIB;
    gpfc_adreal_result.IC   =gpfc_ad_result.IC - gpfc_ADC_EXCURSION.ErrIC;
    gpfc_adreal_result.Vdc  =gpfc_ad_result.Vdc;

   //  gpfc_linev_phase.AB = gpfc_ad_result.UBA;
   //  gpfc_linev_phase.BC = gpfc_ad_result.UBC;

     gpfc_linev_phase.AB = (float)gpfc_adreal_result.UBA*gpfc_vac.coff;//Butterworth2_II_filter((float)gpfc_ad_result.UBA, &PFC_UAB);
     gpfc_linev_phase.BC = (float)gpfc_adreal_result.UBC*gpfc_vac.coff;//Butterworth2_II_filter((float)gpfc_ad_result.UBC, &PFC_UBC);

     abcline_abcphase(&gpfc_linev_phase);

     gpfc_vac.A = gpfc_linev_phase.AN;        //计算实际输出电压，正常311V峰值
     gpfc_vac.B = gpfc_linev_phase.BN;
     gpfc_vac.C = gpfc_linev_phase.CN;

     gpfc_iac.A = (float)gpfc_adreal_result.IA*gpfc_iac.coff;   //计算实际电流，Asinwt

     gpfc_iac.B = (float)gpfc_adreal_result.IB*gpfc_iac.coff;

     gpfc_iac.C = (float)gpfc_adreal_result.IC*gpfc_iac.coff;

     vdc_sen = (float)gpfc_adreal_result.Vdc*vdc_coff;

     vdc_sen_slow = Butterworth2_II_filter(vdc_sen, &PFC_UDC);

     gpfc_real_iac.A=gpfc_iac.A ;
     gpfc_real_iac.B=gpfc_iac.B ;
     gpfc_real_iac.C=gpfc_iac.C;

    return excursion_endflag;
}

//float  xn;
//float  yn;
extern void pfc_GetCurrentabs(unsigned int sum);
/*******************************************************************************************
Function Name:   get_pfc_displaydata
Version:         V1.0
Input:           &gpfc_linev_phasev
Output:          
Description:     pfc实时数据更新
History:
*******************************************************************************************/
void get_pfc_displaydata(void)
{
    static unsigned int     n = 400;
    pfc_GetCurrentabs(8000);

	n++;
    if(n>=400)
      n = 0;

    gpfc_display_linVOL.A[n] =  gpfc_ad_result.IA;//sw_a_on;//xn*400;//sw_a_on; //vloop_outer;//gpfc_iac.A;//gpfc_vac.A;//sw_a_on;//gpfc_iac.A*MAX_IAC;
    gpfc_display_linVOL.B[n] =  gpfc_ad_result.IB;//sw_b_on;//sw_b_on;//gpfc_vac.B; //d_ctrl_feed*100;//gpfc_iac.B;//gpfc_vac.B;//sw_b_on;//gpfc_iac.B*MAX_IAC;
    gpfc_display_linVOL.C[n] =  gpfc_ad_result.IC;//sw_c_on;//q_ctrl_feed*100;//gpfc_iac.C;//gpfc_vac.C;//sw_c_on;//gpfc_iac.C*MAX_IAC;
	
    gpfc_display_control.alpha[n] =  gpfc_vac.A;//gpfc_ctrl_ac.d*400;//gpfc_vac.alpha;//gpfc_ctrl_ac.A;//gpfc_vac.alpha;
    gpfc_display_control.beta[n]  =  gpfc_vac.B;//gpfc_ctrl_ac.q*400;//gpfc_vac.beta;//gpfc_ctrl_ac.B;//gpfc_vac.beta;
    gpfc_display_control.theta[n] =  gpfc_dsrf_spllabc.theta*400;//vdc_sen;
   

    gpfc_display_linVOL.head = n;
}




#pragma CODE_SECTION(pi_func, ".TI.ramfunc")
float sat_I =0.0013;
float pi_ID(PI_CTRL_DATA_DEF *pi, float give, float sen)
{
       float temp;

    /* proportional term */
       pi->up = give - sen;

       temp = _IQmpy(pi->Ki, pi->up);

       if(temp>sat_I)
           temp = sat_I;

       /* integral term */
       pi->ui = (temp+ pi->i1);

       pi->i1 =  _IQsat(pi->ui, (pi->Umax/pi->Kp),  (pi->Umin/pi->Kp));

       /* control output */
       pi->v1 = _IQmpy(pi->Kp, (pi->up + pi->ui));
       pi->Out= _IQsat(pi->v1, pi->Umax, pi->Umin);

       return(pi->Out);
}
/*******************************************************************************************
Function Name:   pi_func
Version:         V1.0
Input:           &pi->pid参数   give->设定参数  sen->返回采样参数
Output:          gpfc_linev_phasev转换后的结果保存
Description:     增量型pid控制函数
History:
*******************************************************************************************/
float pi_func(PI_CTRL_DATA_DEF *pi, float give, float sen)
{
    /* proportional term */
    pi->up = give - sen;

    /* integral term */
    pi->ui = (_IQmpy(pi->Ki, pi->up)+ pi->i1);

    pi->i1 =  _IQsat(pi->ui, (pi->Umax/pi->Kp),  (pi->Umin/pi->Kp));

    /* control output */
    pi->v1 = _IQmpy(pi->Kp, (pi->up + pi->ui));
    pi->Out= _IQsat(pi->v1, pi->Umax, pi->Umin);

    return(pi->Out);
}

/*******************************************************************************************
Function Name:   abc_to_alpha_beta_clark_tran
Version:         V1.0
Input:           &ac_data(abc坐标系电压或电流值)
Output:          αβ坐标系(电压或电流)
Description:     abc to clark tran
History:
*******************************************************************************************/
void abc_to_alpha_beta_clark_tran(ABC_ab_dq_DEF *ac_data)
{
    ac_data->alpha = (0.6666) * (ac_data->A - 0.5 * ac_data->B - 0.5 * ac_data->C);
    ac_data->beta =  (0.6666) * (sqrt3div2 * ac_data->B - sqrt3div2 * ac_data->C);
}

/*******************************************************************************************
Function Name:   alpha_beta_to_dq_tran
Version:         V1.0
Input:           &ac_data(abc坐标系电压或电流值)  wt旋转角度
Output:          αβ坐标系(电压或电流)
Description:     αβ->DQ
History:
*******************************************************************************************/
void alpha_beta_to_dq_tran(ABC_ab_dq_DEF *ac_data, sinwt_coswt_DEF *wt)
{
    ac_data->d = ac_data->alpha * wt->coswt + ac_data->beta * wt->sinwt;
    ac_data->q = -ac_data->alpha * wt->sinwt + ac_data->beta * wt->coswt;
}

/*******************************************************************************************
Function Name:   inverse_dq_to_alpha_beta_tran
Version:         V1.0
Input:           &ac_data(abc坐标系电压或电流值)  wt旋转角度
Output:          αβ坐标系(电压或电流)
Description:     DQ->αβ Inverse DQ transformation
History:
*******************************************************************************************/
void inverse_dq_to_alpha_beta_tran(ABC_ab_dq_DEF *ac_data, sinwt_coswt_DEF *wt)
{
    ac_data->alpha = wt->coswt * ac_data->d - wt->sinwt * ac_data->q;

    ac_data->beta = wt->sinwt * ac_data->d + wt->coswt * ac_data->q;
}

/*******************************************************************************************
Function Name:   inverse_clark_alpha_beta_to_abc_tran
Version:         V1.0
Input:           &ac_data(abc坐标系电压或电流值)
Output:          αβ坐标系(电压或电流)
Description:     αβ->abcInverse clarke transformation
History:
*******************************************************************************************/
void inverse_clark_alpha_beta_to_abc_tran(ABC_ab_dq_DEF *ac_data)
{
    ac_data->A = ac_data->beta;
    ac_data->B = ac_data->beta * (-0.5) + ac_data->alpha * 0.5 * sqrt3;
    ac_data->C = ac_data->beta * (-0.5) - ac_data->alpha * 0.5 * sqrt3;
}


//! \brief Calculates the SPLL_1PH_SOGI_FLL coefficients
//! \param *spll_obj The SPLL_1PH_SOGI_FLL structure pointer
//! \return None
//!
static inline void SPLL_3PH_SOGI_FLL_coeff_calc(SPLL_3PH_SOGI_FLL *spll_obj)
{
    float32_t osgx,osgy,temp;

    osgx = (float32_t)(2.0f*spll_obj->k*spll_obj->w_dash*spll_obj->delta_t);
    osgy = (float32_t)(spll_obj->w_dash*spll_obj->delta_t*spll_obj->w_dash*spll_obj->delta_t);
    temp = (float32_t)1.0f/(osgx+osgy+4.0f);

    spll_obj->osg_coeff.osg_b0=((float32_t)osgx*temp);
    spll_obj->osg_coeff.osg_b2=((float32_t)(-1.0f)*spll_obj->osg_coeff.osg_b0);
    spll_obj->osg_coeff.osg_a1=((float32_t)(2.0f*(4.0f-osgy))*temp);
    spll_obj->osg_coeff.osg_a2=((float32_t)(osgx-osgy-4)*temp);

    spll_obj->osg_coeff.osg_qb0=((float32_t)(spll_obj->k*osgy)*temp);
    spll_obj->osg_coeff.osg_qb1=(spll_obj->osg_coeff.osg_qb0*(float32_t)(2.0));
    spll_obj->osg_coeff.osg_qb2=spll_obj->osg_coeff.osg_qb0;

  //  spll_obj->x3[0]=0;
  //  spll_obj->x3[1]=0;
}


//! \brief Resets internal data to zero,
//! \param *spll_obj  The SPLL_1PH_SOGI_FLL structure pointer
//! \return None
//!
static void SPLL_3PH_SOGI_FLL_reset(SPLL_3PH_SOGI_FLL *spll_obj)
{
    spll_obj->u[0]=(float32_t)(0.0);
    spll_obj->u[1]=(float32_t)(0.0);
    spll_obj->u[2]=(float32_t)(0.0);

    spll_obj->osg_u[0]=(float32_t)(0.0);
    spll_obj->osg_u[1]=(float32_t)(0.0);
    spll_obj->osg_u[2]=(float32_t)(0.0);

    spll_obj->osg_qu[0]=(float32_t)(0.0);
    spll_obj->osg_qu[1]=(float32_t)(0.0);
    spll_obj->osg_qu[2]=(float32_t)(0.0);


    spll_obj->x3[0]=0;
    spll_obj->x3[1]=0;

    spll_obj->ef2=0;
}


//! \brief  Configures the SPLL_1PH_SOGI_FLL coefficients
//! \param  *spll_obj The SPLL_1PH_SOGI_FLL structure pointer
//! \param  acFreq Nominal AC frequency for the SPLL Module
//! \param  isrFrequency Nominal AC frequency for the SPLL Module
//! \param  lpf_b0 B0 coefficient of LPF of SPLL
//! \param  lpf_b1 B1 coefficient of LPF of SPLL
//! \param  k k parameter for FLL
//! \param  gamma gamma parameter for FLL
//! \return None
//!
void SPLL_3PH_SOGI_FLL_config(SPLL_3PH_SOGI_FLL *spll_obj,
                         float acFreq,
                         float isrFrequency,
                         float lpf_b0,
                         float lpf_b1,
                         float k,
                         float gamma)
{
    SPLL_3PH_SOGI_FLL_reset(spll_obj);
    spll_obj->fn=acFreq;
    spll_obj->w_dash = 2*3.14159265f*acFreq;
    spll_obj->wc = 2*3.14159265f*acFreq;
    spll_obj->delta_t=((1.0f)/isrFrequency);
    spll_obj->k=k;
    spll_obj->gamma=gamma;

    SPLL_3PH_SOGI_FLL_coeff_calc(spll_obj);
    spll_obj->x3[0]=0;
    spll_obj->x3[1]=0;
}


static inline void SPLL_3PH_SOGI_FLL_run(SPLL_3PH_SOGI_FLL *spll_obj,
                                      float alpha, float beta)
{
  //  float32_t osgx,osgy,temp;

    //
    // Update the spll_obj->u[0] with the grid value
    //
    spll_obj->u[0]=alpha;

    //
    // Orthogonal Signal Generator
    //
    spll_obj->osg_u[0]=(spll_obj->osg_coeff.osg_b0*
                       (spll_obj->u[0]-spll_obj->u[2])) +
                       (spll_obj->osg_coeff.osg_a1*spll_obj->osg_u[1]) +
                       (spll_obj->osg_coeff.osg_a2*spll_obj->osg_u[2]);

    spll_obj->osg_u[2]=spll_obj->osg_u[1];
    spll_obj->osg_u[1]=spll_obj->osg_u[0];

    spll_obj->osg_qu[0]=(spll_obj->osg_coeff.osg_qb0*spll_obj->u[0]) +
                        (spll_obj->osg_coeff.osg_qb1*spll_obj->u[1]) +
                        (spll_obj->osg_coeff.osg_qb2*spll_obj->u[2]) +
                        (spll_obj->osg_coeff.osg_a1*spll_obj->osg_qu[1]) +
                        (spll_obj->osg_coeff.osg_a2*spll_obj->osg_qu[2]);

    spll_obj->osg_qu[2]=spll_obj->osg_qu[1];
    spll_obj->osg_qu[1]=spll_obj->osg_qu[0];

    spll_obj->u[2]=spll_obj->u[1];
    spll_obj->u[1]=spll_obj->u[0];


    //
    // Update the spll_obj->u1[0] with the grid value
    //
    spll_obj->u1[0]=beta;

    //
    // Orthogonal Signal Generator
    //
    spll_obj->osg_u1[0]=(spll_obj->osg_coeff.osg_b0*
                       (spll_obj->u1[0]-spll_obj->u1[2])) +
                       (spll_obj->osg_coeff.osg_a1*spll_obj->osg_u1[1]) +
                       (spll_obj->osg_coeff.osg_a2*spll_obj->osg_u1[2]);

    spll_obj->osg_u1[2]=spll_obj->osg_u1[1];
    spll_obj->osg_u1[1]=spll_obj->osg_u1[0];

    spll_obj->osg_qu1[0]=(spll_obj->osg_coeff.osg_qb0*spll_obj->u1[0]) +
                        (spll_obj->osg_coeff.osg_qb1*spll_obj->u1[1]) +
                        (spll_obj->osg_coeff.osg_qb2*spll_obj->u1[2]) +
                        (spll_obj->osg_coeff.osg_a1*spll_obj->osg_qu1[1]) +
                        (spll_obj->osg_coeff.osg_a2*spll_obj->osg_qu1[2]);

    spll_obj->osg_qu1[2]=spll_obj->osg_qu1[1];
    spll_obj->osg_qu1[1]=spll_obj->osg_qu1[0];

    spll_obj->u1[2]=spll_obj->u1[1];
    spll_obj->u1[1]=spll_obj->u1[0];


    //
    // FLL
    //
    spll_obj->ef2 = (((alpha-spll_obj->osg_u[0])*spll_obj->osg_qu[0])+((beta-spll_obj->osg_u1[0])*spll_obj->osg_qu1[0]))* spll_obj->delta_t*-0.5f;

    spll_obj->x3[0]=spll_obj->x3[1] + spll_obj->ef2;

    spll_obj->x3[0]= (spll_obj->x3[0]>100.0)?100.0:spll_obj->x3[0];
    spll_obj->x3[0]= (spll_obj->x3[0]<-100.0)?-100.0:spll_obj->x3[0];

    spll_obj->x3[1]=spll_obj->x3[0];

    spll_obj->w_dash = spll_obj->wc + spll_obj->x3[0];

    spll_obj->fn = spll_obj->w_dash / (2.0*3.1415926f);

    SPLL_3PH_SOGI_FLL_coeff_calc(spll_obj);

}


#pragma CODE_SECTION(dsrf_spll_func, ".TI.ramfunc")
/*******************************************************************************************
Function Name:   dsrf_spll_func
Version:         V1.0
Input:           va、vb、vc、&drsf
Output:          theta
Description:     dsrf锁相环
History:
*******************************************************************************************/

float dds_kp = 5.0f, dds_ki = 0.5f, dds_vqi = 0;

inline void dsrf_spll_func(DSRF_SPLL_DEF *drsf, SPLL_3PH_SOGI_FLL *spll_obj)
{
    float sintheta;
    float costheta;
    float sintheta2;
    float costheta2;
    float ualha;
    float ubeta;
    static float vd_p_ave = 0.0f, vq_p_ave = 0.0f, vd_n_ave = 0.0f, vq_n_ave = 0.0f;
    static float theta = 0.0f;

    sintheta  = __sinpuf32(theta);
    costheta  = __cospuf32(theta);
    sintheta2 = __sinpuf32(theta*2.0);
    costheta2 = __cospuf32(theta*2.0);

    ualha = spll_obj->osg_u[0];//0.6666 * (gpfc_vac.A - 0.5 * gpfc_vac.B - 0.5 * gpfc_vac.C);//spll_obj->osg_u[0];//0.6666 * (va - 0.5 * vb - 0.5 * vc);
    ubeta = spll_obj->osg_u1[0];//0.6666 * (0.866 * gpfc_vac.B - 0.866 * gpfc_vac.C);//spll_obj->osg_u1[0];//0.6666 * (0.866 * vb - 0.866 * vc);


    float vd_p = ualha*costheta + ubeta*sintheta;
    float vq_p =-ualha*sintheta + ubeta*costheta;
    float vd_n = ualha*costheta - ubeta*sintheta;
    float vq_n = ualha*sintheta + ubeta*costheta;

    float vd_p_o = (vd_p-vd_n_ave*costheta2)-(vq_n_ave*sintheta2);
    float vq_p_o = (vq_p+vd_n_ave*sintheta2)-(vq_n_ave*costheta2);
    float vd_n_o = (vd_n-vd_p_ave*costheta2)+(vq_p_ave*sintheta2);
    float vq_n_o = (vq_n-vd_p_ave*sintheta2)-(vq_p_ave*costheta2);
    //
    vd_p_ave = vd_p_ave*0.99f + vd_p_o*0.01f;
    vq_p_ave = vq_p_ave*0.99f + vq_p_o*0.01f;
    vd_n_ave = vd_n_ave*0.99f + vd_n_o*0.01f;
    vq_n_ave = vq_n_ave*0.99f + vq_n_o*0.01f;



    float dds_vqp = -vq_p_o * dds_kp; // err = 0-vq_p_o
          dds_vqi += -vq_p_o * dds_ki;

    float dds_vqo = dds_vqp + dds_vqi;
    theta -= dds_vqo * 0.0001f; // (45-65)*(1/10khz)*2pi

    drsf->times ++;
    if(theta > 1.0f){theta -= 1.0f;drsf->freq=(276240+0.5)/drsf->times;drsf->times=0;}
    if(theta < 0){theta += 1.0f; drsf->freq=(276240.0+0.5)/drsf->times;drsf->times=0;}

    //updata
    drsf->theta = theta;
    drsf->vd_p = vd_p_ave;
    drsf->vq_p = vq_p_ave;
    drsf->vd_n = vd_n_ave;
    drsf->vq_n = vq_n_ave;
}

/*******************************************************************************************
Function Name:   pfc_get_svpwmsector
Version:         V1.0
Input:           void
Output:          sector(当前函数工作扇区)
Description:     pfc控制函数，放在中断里面执行
History:
*******************************************************************************************/
extern unsigned char get_svpwmmode();
void pfc_svpwmgen(ABC_ab_dq_DEF ctrl_ac)
{

    gpfc_svgen.Ualpha = ctrl_ac.alpha;
    gpfc_svgen.Ubeta  = ctrl_ac.beta;

    if(0==get_svpwmmode())
    {
	SVGENDQ_MACRO(gpfc_svgen);
    }
    else
    {
	SVGENDPWM_MACRO(gpfc_svgen);
    }

    sw_c_on = (-gpfc_svgen.Ta+1.0f)*3620;
    sw_b_on = (-gpfc_svgen.Tb+1.0f)*3620;
    sw_a_on = (-gpfc_svgen.Tc+1.0f)*3620;


    (*PFC_PWM)(sw_a_on, sw_b_on, sw_c_on);
}

//extern Uint16 begin;
float set_q_ctrl =0.02f;
float set_q =0.0f;
float set_d_ctrl =-0.9f;
float d_ref =0.2f;
float d_ref_n =10.0f;
float q_ref_n =10.0f;
float test_3p3z = 0;



#pragma CODE_SECTION(pfc_controlfunction, ".TI.ramfunc")
/*******************************************************************************************
Function Name:   pfc_controlfunction
Version:         V1.0
Input:           void
Output:          void
Description:     pfc控制函数，放在中断里面执行
History:
*******************************************************************************************/
void pfc_controlfunction(void)
{
    static BOOL pfc_endcur_flag = FALSE;
    static unsigned int n = 0;
    static unsigned long int delay_n = 0;
    static float theta = 0.0f;
    float temp_iq=0.0f,temp_iq1=0.0f;
    float temp_id=0.0f;
    float temp_cal;
 //   GpioDataRegs.GPCTOGGLE.bit.GPIO64 = 1;  //用于计算pfc控制程序运行时间

    //启机给与4S延时，防止零漂运算出错
    if(delay_n<80000)
    {
        delay_n++;
        return;
    }

    //计算ADC运算结果，与ADC运算偏移
    pfc_endcur_flag  = getpfc_adresult();

    //获取显示数据
    get_pfc_displaydata();


    //若ADC零漂运算未完成
    if(pfc_endcur_flag  == FALSE )
    {
        PwmPFCOff();
        return;
    }

    // Run ssrf pll
    abc_to_alpha_beta_clark_tran(&gpfc_vac);
    SPLL_3PH_SOGI_FLL_run(&gpfc_spll_data, gpfc_vac.alpha,gpfc_vac.beta);
    dsrf_spll_func(&gpfc_dsrf_spllabc, &gpfc_spll_data);   //

    theta = gpfc_dsrf_spllabc.theta;

    //延时一段时间等锁相环运算完成
    if(n<40000)
    {
        n++;
        PwmPFCOff();
        vloop_pi.i1  = 0;
        d_ctrl_pi.i1 = -1.0f/d_ctrl_pi.Kp;
        q_ctrl_pi.i1 = -0.03/ d_ctrl_pi.Kp;
        return;
    }

    //开关机判断
    if(gpfc_begin==0)
    {
        vloop_pi.i1  = 0;                   //PID初始需要进行积分复位
        d_ctrl_pi.i1 = -1.0f/d_ctrl_pi.Kp;
        q_ctrl_pi.i1 = -0.03/ d_ctrl_pi.Kp;
        PwmPFCOff();
    }
    else  if(gpfc_begin==1)
    {
        PwmPFCOn();

    }

    //通过锁相环判断当前电网角度
    gpfc_wt.sinwt = __sinpuf32(theta);
    gpfc_wt.coswt = __cospuf32(theta);

    // abc_to_alpha_beta_clark_tran
  //  abc_to_alpha_beta_clark_tran(&gpfc_vac);
    abc_to_alpha_beta_clark_tran(&gpfc_iac);

    // alpha_beta_to_dq_tran
    // alpha_beta_to_dq_tran(&gpfc_vac, &wt);
    // alpha_beta_to_dq_tran(&gpfc_iac, &gpfc_wt);

    // 获取正序DQ参数
    gpfc_vac.d = gpfc_dsrf_spllabc.vd_p;
    gpfc_vac.q = gpfc_dsrf_spllabc.vq_p;

    alpha_beta_to_dq_tran(&gpfc_iac, &gpfc_wt);

    // Current inner loop D Q Control
    vloop_outer = pi_func(&vloop_pi, vdc_set, vdc_sen_slow)*vdc_sen_slow;
    //gpfc_iac.d = -2.0f;
  //  d_ctrl_feed = pi_func(&d_ctrl_pi, vloop_outer, gpfc_iac.d);
    temp_cal = gpfc_vac.d*gpfc_vac.d + gpfc_vac.q*gpfc_vac.q;
    temp_id = vloop_outer*0.6667*gpfc_vac.d/temp_cal; //0.6667->2/3;
    d_ctrl_feed = pi_func(&d_ctrl_pi, temp_id, gpfc_iac.d);

    gpfc_ctrl_ac.d = ((gpfc_vac.d)-d_ctrl_feed+0.5466*gpfc_iac.q)*1.732f/vdc_sen_slow;
//    gpfc_ctrl_ac.d = 1.0f-vloop_outer;
//    gpfc_ctrl_ac.d = Butterworth2_II_filter(gpfc_ctrl_ac.d, &PFC_ialpha);
    // Limt the control d and q output
    if(gpfc_ctrl_ac.d > 0.98f) { gpfc_ctrl_ac.d = 0.98f;}
    if(gpfc_ctrl_ac.d < 0.5) { gpfc_ctrl_ac.d = 0.5;}


    temp_iq = set_q+vloop_outer*0.6667*gpfc_vac.q/temp_cal;
    temp_iq1 = pi_func(&q_ctrl_pi, temp_iq, gpfc_iac.q);
    q_ctrl_feed = temp_iq1;

    gpfc_ctrl_ac.q =   ((gpfc_vac.q)- q_ctrl_feed-0.5466*gpfc_iac.d)*1.732f/vdc_sen_slow;

//    gpfc_ctrl_ac.q = Butterworth2_II_filter(gpfc_ctrl_ac.q, &PFC_ibeta);
    //Limt the control d and q output
    if(gpfc_ctrl_ac.q > 0.1f) { gpfc_ctrl_ac.q = 0.1f;}
    if(gpfc_ctrl_ac.q < -0.1f) { gpfc_ctrl_ac.q = -0.1f;}

    // inverse_dq_tran
    inverse_dq_to_alpha_beta_tran(&gpfc_ctrl_ac, &gpfc_wt);

    //inverse_clark_tran
    inverse_clark_alpha_beta_to_abc_tran(&gpfc_ctrl_ac);


    //svpwm
    pfc_svpwmgen(gpfc_ctrl_ac);

}


//srfa测试用，实际没什么用
/*
#pragma DATA_SECTION(set_d_ctrl,"srfaFile");
#pragma DATA_SECTION(set_q_ctrl,"srfaFile");
#pragma DATA_SECTION(gpfc_iac,"srfaFile");
#pragma DATA_SECTION(q_ctrl_feed,"srfaFile");
#pragma DATA_SECTION(test_in,"srfaFile");
#pragma DATA_SECTION(test_out,"srfaFile");
#pragma DATA_SECTION(test_ref,"srfaFile");
*/

