#include"pfc_data.h"
#include "pfc_control.h"

//三相输入电压电流均方根值显示与计算
PFC_IUVW_STRUCT_ABS        gpfc_Iabs = {.R=0,.S=0,.T=0,.NUM=4000};
PFC_UUVW_STRUCT_ABS        gpfc_Uabs = {.NUM=4000};  //求取均方根值用,10个周期取平均值

PFC_DISPLAY_STRUCT        gpfc_display = {1,1,1,1,1,1,1,1,1};


extern ABC_REAL_VALUE_DEF gpfc_real_iac;
extern ABC_line_phase_DEF gpfc_linev_phase;
extern DSRF_SPLL_DEF      gpfc_dsrf_spllabc;
extern float              vdc_sen;
extern float              pfc_temp;

static Uint32  gpfc_vdc;


//读取Vdc电压
Uint32 read_vdc(void)
{
    return gpfc_vdc;
}

//以绝对值平均值代替正弦波有效值
void pfc_GetCurrentabs(unsigned int sum)
{
    static float  Vdc_sum = 0;

    gpfc_Iabs.Sum_R += (Uint32)(gpfc_real_iac.A*gpfc_real_iac.A*10000);
    gpfc_Iabs.Sum_S += (Uint32)(gpfc_real_iac.B*gpfc_real_iac.B*10000);
    gpfc_Iabs.Sum_T += (Uint32)(gpfc_real_iac.C*gpfc_real_iac.C*10000);

    gpfc_Uabs.Sum_R += (Uint32)(gpfc_linev_phase.AB*gpfc_linev_phase.AB);
    gpfc_Uabs.Sum_S += (Uint32)(gpfc_linev_phase.BC*gpfc_linev_phase.BC);
    gpfc_Uabs.Sum_T += (Uint32)(gpfc_linev_phase.CA*gpfc_linev_phase.CA);

    Vdc_sum += vdc_sen;

    gpfc_Iabs.NUM++;

    if(gpfc_Iabs.NUM<0)
           gpfc_Iabs.NUM=0;

    if(gpfc_Iabs.NUM>sum)
    {
        gpfc_Iabs.R = __sqrt(gpfc_Iabs.Sum_R/gpfc_Iabs.NUM) ;
        gpfc_Iabs.S = __sqrt(gpfc_Iabs.Sum_S/gpfc_Iabs.NUM) ;
        gpfc_Iabs.T = __sqrt(gpfc_Iabs.Sum_T/gpfc_Iabs.NUM) ;

        gpfc_Uabs.URS = __sqrt(gpfc_Uabs.Sum_R/gpfc_Iabs.NUM);
        gpfc_Uabs.UST = __sqrt(gpfc_Uabs.Sum_S/gpfc_Iabs.NUM);
        gpfc_Uabs.UTR = __sqrt(gpfc_Uabs.Sum_T/gpfc_Iabs.NUM);

        gpfc_vdc      =  (Vdc_sum/gpfc_Iabs.NUM)*10;

        gpfc_Uabs.Sum_R = 0;
        gpfc_Uabs.Sum_S = 0;
        gpfc_Uabs.Sum_T = 0;

        gpfc_Iabs.Sum_R=0;
        gpfc_Iabs.Sum_S=0;
        gpfc_Iabs.Sum_T=0;
        gpfc_Iabs.NUM=0;
        Vdc_sum = 0;
    }
}



//pfc 显示参数更新
void pfc_update_display(void)
{
    float temp;
    if(pfc_temp<180)
      temp = 0.2398*pfc_temp-18.155;
    else if(pfc_temp<800)
      temp = 0.0784*pfc_temp+12.852;
    else if(pfc_temp<2300)
      temp = 0.0327*pfc_temp+50.304;
    else
      temp = 0.0357*pfc_temp+42.257;
    if(temp<0)
      temp=0;

    gpfc_display.Udc        = (gpfc_vdc);//PFC_Filter8(gUUVW.dc,gpfc_display.Udc);
    gpfc_display.Temp       = PFC_Filter8((uint16)temp,gpfc_display.Temp);
    gpfc_display.Ir         = PFC_Filter8(gpfc_Iabs.R,gpfc_display.Ir);
    gpfc_display.Is         = PFC_Filter8(gpfc_Iabs.S,gpfc_display.Is);
    gpfc_display.It         = PFC_Filter8(gpfc_Iabs.T,gpfc_display.It);
    gpfc_display.Urs        = PFC_Filter8(gpfc_Uabs.URS,gpfc_display.Urs);
    gpfc_display.Ust        = PFC_Filter8(gpfc_Uabs.UST,gpfc_display.Ust);
    gpfc_display.Utr        = PFC_Filter8(gpfc_Uabs.UTR,gpfc_display.Utr);
    gpfc_display.U          = (gpfc_display.Urs + gpfc_display.Ust + gpfc_display.Utr)/3;
    gpfc_display.I          = (gpfc_display.Ir + gpfc_display.Is + gpfc_display.It)/3;
    gpfc_display.In_freq    = gpfc_dsrf_spllabc.freq;

}

unsigned char get_svpwmmode()
{
  static unsigned char mode=0;
  if(gpfc_display.I<1200)
    mode=0;
  else if(gpfc_display.I>1400)
    mode=1;
  return mode;
}


