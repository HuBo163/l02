#include"pfc_control.h"
#include"pfc_logic.h"
#include"pfc_data.h"
#include"motor_protect.h"

extern DSRF_SPLL_DEF      gpfc_dsrf_spllabc;
extern ABC_ab_dq_DEF      gpfc_iac;
extern ABC_ab_dq_DEF      gpfc_vac;
extern float              vdc_sen;

static const PFC_OVRCUR_PROTECT_STRUCT pfc_curprotec_default = PFC_DEFAULT_OVERCUR_PROTECT;
static const PFC_SHOT_PROTECT_STRUCT pfc_shotprotect_default = PFC_DEFAULT_SHOT_protect;

static PFC_PROTECT_VALUE         pfc_protect_value  = PFC_PROTECT_DEFAULT_VALUE;
static PFC_OVRCUR_PROTECT_STRUCT pfc_ovrcur_protect = PFC_DEFAULT_OVERCUR_PROTECT;
static PFC_SHOT_PROTECT_STRUCT   pfc_shot_protect   = PFC_DEFAULT_SHOT_protect;
static BOOL gpfc_shot_flag = false;

/*void pfc_getdisplaydata()
{

}*/

void pfc_reset_error(void)
{
  // pfc_protect_value  = PFC_PROTECT_DEFAULT_VALUE;
   pfc_ovrcur_protect = pfc_curprotec_default;
   pfc_shot_protect   = pfc_shotprotect_default;
   gpfc_shot_flag = false;
}

//判断电压是否正常
BOOL ckeck_data_normal(void)
{
  Uint32 vdc=0;
  static Uint32 times=0;

  vdc=read_vdc();

  if( (vdc<PFC_UDC_MAX)&&(vdc>PFC_UDC_MIN))
    {
      times++;
      if(times>20000)
      {
	  return true;
      }
      else
      {
	  return false;
      }
    }
  else
    {
      times = 0;
      return false;
    }

}

//pfc预充电判断
PFCErrorCode PFC_PreCharge(DSRF_SPLL_DEF drsf,float bus_voltage)
{
    float  temp_vin;
    float  temp_vin_all;
    static float a=0.0;
    static float b=0.0;

    if(drsf.vd_n>0)
        temp_vin = drsf.vd_n;
    else
        temp_vin = -drsf.vd_n;

    temp_vin_all =(temp_vin + drsf.vd_p)*1.732;  //为输入电压对母线电压系数 1.732
    a = a*0.95+temp_vin_all*0.05;
    b = b*0.95+bus_voltage*0.05;//母线电压

    if( (a>b+50)||(a<280)||(b<400))//输入电压大于母线电压加30V，或输入电压小于280V，或母线电压<400V 报故障
      return EPRECHARGE;
    else
      return ENOERR;
}


PFCErrorCode PFC_OverFreq(DSRF_SPLL_DEF drsf)
{
    if((drsf.freq<INMIN_FREQ)||(drsf.freq>INMAX_FREQ))
        return EOVERFREQ ;
    else
        return ENOERR;
}

//鉴相器鉴别完成
PFCErrorCode PFC_PahseLock_Protect(DSRF_SPLL_DEF drsf)
{
    if( (drsf.vd_p > pfc_protect_value.phase_lock_P_min)&&(drsf.vq_p < pfc_protect_value.phase_lock_Q_max) )
    return  ENOERR;
    else
    return  ELOSSPHASE;
}

//pfc输入缺相或三相不平衡保护
PFCErrorCode PFC_LosePhase_Protect(DSRF_SPLL_DEF drsf)
{
    float m_Max=0,m_Min=50000.0f;
    float temp_vd_n = -drsf.vd_n;

    if(m_Max<gpfc_Iabs.R) m_Max=gpfc_Iabs.R;
    if(m_Max<gpfc_Iabs.S) m_Max=gpfc_Iabs.S;
    if(m_Max<gpfc_Iabs.T) m_Max=gpfc_Iabs.T;

    if(m_Min>gpfc_Iabs.R) m_Min=gpfc_Iabs.R;
    if(m_Min>gpfc_Iabs.S) m_Min=gpfc_Iabs.S;
    if(m_Min>gpfc_Iabs.T) m_Min=gpfc_Iabs.T;


    if((drsf.vd_n>pfc_protect_value.phase_unbalance)||(temp_vd_n>pfc_protect_value.phase_unbalance)||(m_Max-m_Min)>PFC_LOSS_PHASE_MIN)
    return  EFHASEUNBALANCE;
    else
    return ENOERR;
}


//pfc输入欠压保护
PFCErrorCode PFC_UnderVoltage_Protect(DSRF_SPLL_DEF drsf)
{
    static bool flag=true;  //当前是否为欠压保护状态

    if(flag==true)
    {
        if(drsf.vd_p > pfc_protect_value.voltage_min_back)
        {
            flag = false;
            return ENOERR;
        }
        else
        {
            flag = true;
            return EVOLTAGEUNDER;
        }
    }
    else
    {
        if(drsf.vd_p > pfc_protect_value.voltage_min)
        {
            flag = false;
            return ENOERR;
        }
        else
        {
            flag = true;
            return EVOLTAGEUNDER;
        }
    }
}


//pfc输入过压保护
PFCErrorCode PFC_OverVoltage_Protect(DSRF_SPLL_DEF drsf)
{
    static bool flag=true;  //当前是否为欠压保护状态

    if(flag==true)
    {
        if(drsf.vd_p < pfc_protect_value.voltage_max_back)
        {
            flag = false;
            return ENOERR;
        }
        else
        {
            flag = true;
            return EVOLTAGEOVER;
        }
    }
    else
    {
        if(drsf.vd_p < pfc_protect_value.voltage_max)
        {
            flag = false;
            return ENOERR;
        }
        else
        {
            flag = true;
            return EVOLTAGEOVER;
        }
    }
}


//pfc输入过流保护
PFCErrorCode PFC_OverCurrent_Protect()
{
    static unsigned int times=0;          //故障发生次数，用以做简单的滤波
    static unsigned int check_flag=0;     //测试周期

    check_flag++;
    if(check_flag==50)
    {
	check_flag=0;
    }
    else if(pfc_ovrcur_protect.statue|pfc_ovrcur_protect.lock)         //50*2 = 100ms做处理
    {
	return EOVERCURRENT;
    }
    if( (pfc_ovrcur_protect.lock==false) )                                 //无故障指示时，才进行相应的操作
    {
        if(pfc_ovrcur_protect.statue==false)
        {
            if( (gpfc_Iabs.R>pfc_ovrcur_protect.maxcur2)||(gpfc_Iabs.S>pfc_ovrcur_protect.maxcur2)||(gpfc_Iabs.T>pfc_ovrcur_protect.maxcur2) )        //2.5倍过载，立马保护
            {
               // get_errordata(Iac_over);
                pfc_ovrcur_protect.statue = true;                //故障发生
                pfc_ovrcur_protect.times++;                      //故障次数加1

                if( pfc_ovrcur_protect.times >=PFC_OVR_CUR_TIMES )   //故障大于指定次数，故障锁死
                {
                    pfc_ovrcur_protect.lock = true;
                }

                pfc_ovrcur_protect.reback_time=0;                  //缺相保护故障次数清除时间
                pfc_ovrcur_protect.start_time=0;                   //缺相保护重启时间

                return EOVERCURRENT;                               //返回故障
            }
            else if( (gpfc_Iabs.R>pfc_ovrcur_protect.maxcur1)||(gpfc_Iabs.S>pfc_ovrcur_protect.maxcur1)||(gpfc_Iabs.T>pfc_ovrcur_protect.maxcur1) )   //超过2倍过载，软件采取逐波限流的方式
            {
                times++;
                if(times>2)                                      //连续超过两次，说明有故障发生
                {
               //   get_errordata(Iac_over);
                  pfc_ovrcur_protect.time++;                         //故障持续时间增加
                  if( pfc_ovrcur_protect.time>PFC_MAX_OVRCUR1_TIME )
                  {
                     pfc_ovrcur_protect.statue = true;                    //故障发生
                     pfc_ovrcur_protect.times++;                          //故障次数加1

                     if( pfc_ovrcur_protect.times >=PFC_OVR_CUR_TIMES )   //故障大于指定次数，故障锁死
                         pfc_ovrcur_protect.lock = true;

                     pfc_ovrcur_protect.start_time=0;

                     return EOVERCURRENT;                                //返回故障
                  }
                   times=3;

                  pfc_ovrcur_protect.reback_time=0;
                  return ENOERR;                                        //故障时间未到1min
                }
            }
            else
            {
                pfc_ovrcur_protect.reback_time++;

                if(pfc_ovrcur_protect.reback_time>PFC_OVR_CURBACK_TIME)    //30min无故障发生，清除故障次数
                {
                    pfc_ovrcur_protect.reback_time=0;
                    pfc_ovrcur_protect.times=0;
                }
                times=0;
            }
        }
        else
        {
            pfc_ovrcur_protect.start_time++;
            if(pfc_ovrcur_protect.start_time>PFC_OVR_CURSTART_TIME)
            {
                pfc_ovrcur_protect.statue = false;
                pfc_ovrcur_protect.start_time=0;
            }
            return EOVERCURRENT;
        }
    }
    else
    {
        return EOVERCURRENT;
    }

    return ENOERR;

}


void Set_PFC_ShotFlag(void)
{
    gpfc_shot_flag=true;
}


//pfc 输入短路保护
PFCErrorCode PFC_Shot_Protect(void)
{

    if(pfc_shot_protect.lock==false)
    {
       if( (gpfc_shot_flag==true)&&(pfc_shot_protect.statue == false) )     //有电流输出且小于三相
       {
            pfc_shot_protect.times++;
            pfc_shot_protect.statue=true;
            if(pfc_shot_protect.times>=PFC_SHORT_TIMES)
            {
       //         get_errordata(shot);
               pfc_shot_protect.lock=true;
            }
            else
            {
       //         get_errordata(shot);
            }
            pfc_shot_protect.reback_time=0;
            pfc_shot_protect.start_time=0;


            return  ESHOT;;

       }
       else if(pfc_shot_protect.statue==true)
       {
           pfc_shot_protect.start_time++;
           if((pfc_shot_protect.start_time>PFC_SHOT_START_TIME))
           {
               pfc_shot_protect.statue=false;
               gpfc_shot_flag=false;                  //复位短路信号
               pfc_shot_protect.start_time=0;
               pfc_shot_protect.reback_time=0;
           }

           return  ESHOT;;
       }
       else
       {
           pfc_shot_protect.statue = false;                //清除故障标志位
           pfc_shot_protect.reback_time++;

          if(pfc_shot_protect.reback_time>PFC_SHORT_BACK_TIME) //5min无故障发生，清除故障次数
          {
              pfc_shot_protect.reback_time=0;
              pfc_shot_protect.times=0;
          }


       }
    }
    else
    {
       return  ESHOT;
    }
    return ENOERR;
}


//PFC 过温保护
PFCErrorCode PFC_OverTemp_Protect(void)
{
    static PFCErrorCode tempre_flag= ENOERR;

    if( (gpfc_display.Temp>PFC_MAX_TEMP)&&(tempre_flag==ENOERR) )
    {
        tempre_flag=EOVERTEMP;
    }
    else if(gpfc_display.Temp<PFC_MIN_TEMP)
    {
        tempre_flag = ENOERR;
    }
    return tempre_flag;
}


PFCErrorCode PFC_Logic_Protect()
{
    PFCErrorCode flag = ENOERR;
    flag  = PFC_PreCharge(gpfc_dsrf_spllabc,vdc_sen);
    flag |= PFC_OverFreq(gpfc_dsrf_spllabc);
    flag |= PFC_PahseLock_Protect(gpfc_dsrf_spllabc);
    flag |= PFC_LosePhase_Protect(gpfc_dsrf_spllabc);
    flag |= PFC_UnderVoltage_Protect(gpfc_dsrf_spllabc);
    flag |= PFC_OverVoltage_Protect(gpfc_dsrf_spllabc);
    flag |= PFC_OverCurrent_Protect();
    flag |= PFC_OverTemp_Protect();
    flag |= PFC_Shot_Protect();
    return flag;

}

Uint16 pfc_get_prechargeflag(void)
{
  return PFC_PreCharge(gpfc_dsrf_spllabc,vdc_sen);
}

extern void get_errordata(error_type error);
bool inv_pfc_protect(void)
{
    PFCErrorCode flag = ENOERR;
    flag = PFC_Logic_Protect();

    if(flag&EOVERFREQ)
    {
        get_errordata(uin_overfreq);
    }
    else if(flag&EFHASEUNBALANCE)
    {
        get_errordata(uin_lossphase);
    }
    else if(flag&EVOLTAGEUNDER)
    {
        get_errordata(uin_low);
    }
    else if(flag&EVOLTAGEOVER)
    {
        get_errordata(uin_over);
    }
    else if(flag&EOVERCURRENT)
    {
        get_errordata(uin_overcurrent);
    }
    else if(flag&EOVERTEMP)
    {
	get_errordata(over_temp);
    }
    else if(flag&EPRECHARGE)
    {
	get_errordata(pre_charge_fault);
    }



    if( ENOERR==flag  )
        return false;
    else
        return true;
}

