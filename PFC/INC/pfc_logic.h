
#ifndef     PFC_LOGIC_H_
#define     PFC_LOGIC_H_

#include "F28x_Project.h"

#define MOTOR_ADEXCURSION_MAX       (2100)
#define MOTOR_ADEXCURSION_MIN       (1700)

#define PFC_UDC_MAX                 (9000)
#define PFC_UDC_MIN                 (3500)

#define PFC_PHASE_LOCK_P_MIN        (50.0f)     //锁相d轴最小值
#define PFC_PHASE_LOCK_Q_MAX        (3.0f)      //锁相q轴最大值
#define PFC_PHASE_UNBALANCE         (30.0f)     //三相电压不平衡
#define PFC_VOLTEGE_MIN_BACK        (120.0f)    //PFC输入欠压最小值
#define PFC_VOLTEGE_MIN             (100.0f)    //PFC输入最小电压
#define PFC_VOLTEGE_MAX_BACK        (460.0f)    //PFC输入过压恢复值
#define PFC_VOLTEGE_MAX             (470.0f)    //PFC输入过压最大值

#define PFC_LOSS_PHASE_MIN          (400)    //PFC最大电流与最小电流之间大于4A认为缺相

//过流保护与缺相保护 30min解锁
#define PFC_MAX_OVRCUR1_TIME         600        //1min =100ms*600
#define PFC_OVR_CURBACK_TIME         1800       //30min=600*3=1800
#define PFC_OVR_CURSTART_TIME        20        //6s=60
#define PFC_OVR_CUR_TIMES            3          //3次故障锁死


//短路保护5min解锁
#define  PFC_SHORT_TIMES             3         //3次短路锁死
#define  PFC_SHORT_BACK_TIME         150000    //5min=500*5*60=150000
#define  PFC_SHOT_START_TIME         5000      //10S = 5000*2  短路保护10s钟后重启


#define  PFC_MAX_CUR1                4500      //1.2倍过载
#define  PFC_MAX_CUR2                5000      //1.5倍过载立马保护

#define  PFC_MAX_TEMP                125      //PFC最大温度125度
#define  PFC_MIN_TEMP                70

//PFC 保护默认值
#define PFC_DEFAULT_OVERCUR_PROTECT   {0,0,0,0,0,0,3000,3500}
#define PFC_DEFAULT_SHOT_protect      {0,0,0,0,0}
#define PFC_PROTECT_DEFAULT_VALUE     {PFC_PHASE_LOCK_P_MIN,PFC_PHASE_LOCK_Q_MAX,PFC_PHASE_UNBALANCE,PFC_VOLTEGE_MIN_BACK,PFC_VOLTEGE_MIN,PFC_VOLTEGE_MAX_BACK,PFC_VOLTEGE_MAX}

//PFC 保护值方便进行远程修改
typedef struct {
    float phase_lock_P_min;
    float phase_lock_Q_max;
    float phase_unbalance;
    float voltage_min_back;
    float voltage_min;
    float voltage_max_back;
    float voltage_max;
}PFC_PROTECT_VALUE;

typedef     Uint16 PFCErrorCode;
#define     ENOERR            0     //无故障
#define     EPRECHARGE        0x01  //PFC预充电故障
#define     ELOSSPHASE        0x02  //锁相故障故障
#define     EOVERFREQ         0x04  //输入频率超范围
#define     EFHASEUNBALANCE   0x08  //相间不平衡
#define     EVOLTAGEUNDER     0x10  //输入欠压
#define     EVOLTAGEOVER      0x20  //输入过压
#define     EOVERCURRENT      0x40  //输入过流
#define     ESHOT             0x80  //PFC短路
#define     EOVERTEMP         0x100 //PFC IBGT模块过温

#define     INMIN_FREQ        450   //最小输入频率
#define     INMAX_FREQ        650   //最大输入频率


//PFC短路保护结构体
typedef struct PFC_SHOT_PROTECT_STRUCT_DEF{
    Uint32 reback_time;       //故障恢复时间
    Uint16 start_time;        //故障发生后重启时间
    Uint16 times;             //故障次数
    Uint16 lock;              //故障锁死
    Uint16 statue;              //故障状态
}PFC_SHOT_PROTECT_STRUCT;

//PFC过流保护结构体
typedef struct PFC_OVRCUR_PROTECT_STRUCT_DEF{
   Uint16   time;             //故障时间
   Uint16   start_time;       //故障发生后重启时间
   Uint16   reback_time;      //故障恢复时间
   Uint16   times;            //故障次数
   Uint16   lock;             //故障是否锁死标志位
   Uint16   statue;           //故障状态标志位，当前是否有故障发生
   Uint16   maxcur1;          //1.5倍过载
   Uint16   maxcur2;          //2倍过载
}PFC_OVRCUR_PROTECT_STRUCT;       //输出缺相判断程序

/*
//求取电压电流均方根值
typedef struct UVW_STRUCT_DEF_ABS{
    int32    U;
    int32    V;
    int32    W;
    int32    NUM;
    int64    Sum_U;
    int64    Sum_V;
    int64    Sum_W;
    int32    coff;
}UVW_STRUCT_ABS;                  //定子三相坐标轴电压电流
*/
#endif

