/*
 * pfc_data.h
 *
 *  Created on: 2020年10月22日
 *      Author: dfs_h
 */

#ifndef PFC_INC_PFC_DATA_H_
#define PFC_INC_PFC_DATA_H_

#include "F28x_Project.h"

#define PFC_Filter8(x,total)   (( (((uint64)total)<<16) + (((uint64)x)<<13) - (((uint64)total)<<13) )>>16)

//求取电压电流均方根值
typedef struct PFC_UUVW_STRUCT_DEF_ABS{
    int32    URS;
    int32    UST;
    int32    UTR;
    int32    NUM;
    int64    Sum_R;
    int64    Sum_S;
    int64    Sum_T;
    int32    coff;
}PFC_UUVW_STRUCT_ABS;                           //定子三相坐标轴电压电流

//求取电压电流均方根值
typedef struct PFC_IUVW_STRUCT_DEF_ABS{
    int32    R;
    int32    S;
    int32    T;
    int32    NUM;
    int64    Sum_R;
    int64    Sum_S;
    int64    Sum_T;
    int32    coff;
}PFC_IUVW_STRUCT_ABS;                           //定子三相坐标轴电压电流

//求取显示所用数据
typedef struct PFC_DISPLAY_STRUCT_DEF{
    Uint16     Udc;
    Uint16     U;
    Uint16     I;
    Uint16     Ir;
    Uint16     Is;
    Uint16     It;
    Uint16     Urs;
    Uint16     Ust;
    Uint16     Utr;
    Uint16     Temp;
    Uint16     In_freq;     //用度表示的实际温度值
}PFC_DISPLAY_STRUCT;

extern PFC_IUVW_STRUCT_ABS        gpfc_Iabs;   //求取均方根值用
extern PFC_UUVW_STRUCT_ABS        gpfc_Uabs;
extern PFC_DISPLAY_STRUCT         gpfc_display;

Uint32 read_vdc(void);

#endif /* PFC_INC_PFC_DATA_H_ */
