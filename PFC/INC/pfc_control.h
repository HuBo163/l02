
#ifndef     PFC_CONTROL_H_
#define     PFC_CONTROL_H_

// Define Input data
#define MATH_TYPE 1
#include "IQmathLib.h"
#include "F28x_Project.h"


#define float32_t float


//与PSIM联合仿真用
//#define  UVW_DEBUG         1
#define  PFCPSIM_DEBUG     1
//
// PI Coefficient DEFINE
#define PFC_ADEXCURSION_MAX   2100
#define PFC_ADEXCURSION_MIN   1800

#define PFC_ADFULL_HALF        2017

//
#define ssrf_ts             1e-5
#define ssrf_kp             1
#define sser_ki             0.002
#define sser_kd             0
#define ssrf_up_limt        10
#define ssrf_low_limt      -10

#define sqrt3               1.732
#define sqrt3div2           0.866
#define value_2pi           6.2831852
#define value_pi            3.1415926
#define value_pi6           0.3183098

#define PI                  3.1415926
#define L                   1.77e-3
#define WL                  (2*PI*50*L)      //2*pi*50*L
#define TS                  1



#define VLOOP_TS            TS
#define VLOOP_KP            0.05
#define VLOOP_KI            0.0005  //增加可以减小过冲0.5-1.0，减小可以减少低频纹波
#define VLOOP_KD            0
#define Vloop_UP            40.0f
#define Vloop_LOW           -40.0f

#define DLOOP_TS            TS
#define DLOOP_KP            2.0f
#define DLOOP_KI            0.0001f
#define DLOOP_KD            0
#define Dloop_UP            800.0f
#define Dloop_LOW           -800.0f

#define QLOOP_TS            TS
#define QLOOP_KP            2.0f   //(0.02*L)/(15*TS*1.5)
#define QLOOP_KI            0.0001f//(0.0005*L)/(112.5*TS*TS*1.5)
#define QLOOP_KD            0
#define Qloop_UP            800.0f
#define Qloop_LOW           -800.0f

#define MAX_IAC             400.0f
#define MAX_UAC             400.0f
#define MAX_DC              800.0f

//
// data struct define
//
typedef struct PI_CTRL_DATA_TAG {
    float  Ref;              // Input: reference set-point
    float  Fbk;             // Input: feedback
    float  Out;             // Output: controller output
    float  Kp;              // Parameter: proportional loop gain
    float  Ki;              // Parameter: integral gain
    float  Umax;            // Parameter: upper saturation limit
    float  Umin;            // Parameter: lower saturation limit
    float  up;              // Data: proportional term
    float  ui;              // Data: integral term
    float  v1;              // Data: pre-saturated controller output
    float  i1;              // Data: integrator storage: ui(k-1)
} PI_CTRL_DATA_DEF;

typedef struct integreator_tag{
    float ts;
    float ki;
    float out_1;
    float out;
} INTER_DEF;

typedef struct ABC_ab_dq_TAG {
    float A;
    float B;
    float C;

    float alpha;
    float beta;

    float d;
    float q;

    float coff;
}ABC_ab_dq_DEF;

typedef struct ABC_REAL_VALUE {
    float A;
    float B;
    float C;
}ABC_REAL_VALUE_DEF;

typedef struct sinwt_coswt_tag{
    float sinwt;
    float coswt;
}sinwt_coswt_DEF;


//! \brief Defines the SPLL_1PH_SOGI_FLL_OSG_COEFF structure
//!
typedef volatile struct{
    float32_t osg_b0;
    float32_t osg_b2;
    float32_t osg_a1;
    float32_t osg_a2;
    float32_t osg_qb0;
    float32_t osg_qb1;
    float32_t osg_qb2;
} SPLL_3PH_SOGI_FLL_OSG_COEFF;

//! \brief Defines the SPLL_1PH_SOGI_FLL_LPF_COEFF structure
//!
typedef struct{
    float32_t b1;
    float32_t b0;
} SPLL_3PH_SOGI_FLL_LPF_COEFF;

//! \brief Defines the Orthogonal Signal Generator SPLL_1PH_SOGI_FLL structure
//!
//!
//! \details The SPLL_3PH_SOGI_FLL can be used to generate the
//!          orthogonal signal from the sensed single phase grid voltage
//!          and use that information to provide phase of the grid voltage
//!          This module also runs a frequency locked loop for adjusting the
//!          the center frequency automatically
//!
typedef struct{
    float32_t   u[3];        //!< AC input data buffer
    float32_t   osg_u[3];    //!< Orthogonal signal generator data buffer
    float32_t   osg_qu[3];   //!< Orthogonal signal generator quadrature data buffer
    float32_t   u1[3];       //!< AC input data buffer
    float32_t   osg_u1[3];   //!< Orthogonal signal generator data buffer
    float32_t   osg_qu1[3];  //!< Orthogonal signal generator quadrature data buff
    float32_t   fn;          //!< Nominal frequency (Hz)
    float32_t   wc;          //!< Center (Nominal) frequency in radians
    float32_t   delta_t;     //!< Inverse of the ISR rate at which module is called
    float32_t   ef2;         //!< FLL parameter
    float32_t   x3[2];       //!< FLL data storage
    float32_t   w_dash;      //!< Output frequency of PLL(radians)
    float32_t   gamma;       //!< Gamma parameter for FLL
    float32_t   k;           //!< K parameter for FLL
    SPLL_3PH_SOGI_FLL_OSG_COEFF osg_coeff; //!< Orthogonal signal generator coefficient
} SPLL_3PH_SOGI_FLL;


typedef struct dsrf_spll_tag{
    float theta;
    float vd_p;
    float vq_p;
    float vd_n;
    float vq_n;
    float freq;
    float times;
} DSRF_SPLL_DEF;

typedef struct ABC_line_phase {
    float AB;
    float BC;
    float CA;

    float AN;
    float BN;
    float CN;
}ABC_line_phase_DEF;

typedef struct ABC_REF {
    float AN;
    float BN;
    float CN;
}ABC_REF_DEF;


typedef struct  PFC_ADCRESULT{
    long Vdc;
    long IC;
    long IB;
    long IA;
    long UBC;
    long UBA;
    long Temp;
}PFC_ADCRESULT_REF;


// 结构体定义，用于计算零漂以及斜率
typedef struct PFC_EXCURSION_STRUCT{
    long     UBA;
    long     UBC;
    long     IA;
    long     IB;
    long     IC;
    long     Vdc;
    long     Temp;
    long    ErrUBA;
    long    ErrUBC;
    long    ErrIA;
    long    ErrIB;
    long    ErrIC;
    long    ErrVdc;
    long    ErrTemp;
    long    Count;
    long    Coff;
}PFC_EXCURSION_STRUCT_DEF;


typedef struct  PFC_DISPLAY{
    int A[400];
    int B[400];
    int C[400];

    int A_DISPLAY[400];
    int B_DISPLAY[400];
    int C_DISPLAY[400];

    int head;

}PFC_DISPLAY_REF;

typedef struct  PFC_DISPLAY1{
    int alpha[400];
    int beta[400];

    int theta[400];
    int alpha_DISPLAY[400];
    int beta_DISPLAY[400];

    int theta_DISPLAY[400];

}PFC_DISPLAY_REF1;


typedef struct  Butterworth_II{
    float  A[3];
    float  B[3];

    float  W1[2];
    float  W2[2];

} Butterworth_II_DEF;


typedef struct  Elliptic_V{
    float  A[3];
    float  B[3];

    float  W[2];

} Elliptic_V_DEF;


typedef struct  Highpass_Shift_V{
    float  A;
    float  B[2];

    float  x_1;
    float  y_1;

} Highpass_Shift_DEF;


typedef struct  { float  Ualpha;               // Input: reference alpha-axis phase voltage
                  float  Ubeta;               // Input: reference beta-axis phase voltage
                  float  Ta;                  // Output: reference phase-a switching function
                  float  Tb;                  // Output: reference phase-b switching function
                  float  Tc;                  // Output: reference phase-c switching function
                  float  tmp1;                // Variable: temp variable
                  float  tmp2;                // Variable: temp variable
                  float  tmp3;                // Variable: temp variable
                  unsigned int VecSector;     // Space vector sector
                } PSM_SVGEN;

/*-----------------------------------------------------------------------------
Default initalizer for the SVGEN object.
-----------------------------------------------------------------------------*/
#define SVGEN_DEFAULTS { 0,0,0,0,0 }

/*------------------------------------------------------------------------------
Space Vector  Generator (SVGEN) Macro Definition
------------------------------------------------------------------------------*/


#define SVGENDQ_MACRO(v)                                                    \
v.tmp1= v.Ubeta;                                                            \
v.tmp2= v.Ubeta/2.0f + (0.866f*v.Ualpha);                                   \
v.tmp3= v.tmp2 - v.tmp1;                                                    \
                                                                            \
v.VecSector=3;                                                              \
v.VecSector=(v.tmp2> 0)?( v.VecSector-1):v.VecSector;                       \
v.VecSector=(v.tmp3> 0)?( v.VecSector-1):v.VecSector;                       \
v.VecSector=(v.tmp1< 0)?(7-v.VecSector) :v.VecSector;                       \
                                                                            \
if     (v.VecSector==1 || v.VecSector==4)                                   \
  {     v.Ta= v.tmp2;                                                       \
        v.Tb= v.tmp1-v.tmp3;                                                \
        v.Tc=-v.tmp2;                                                       \
  }                                                                         \
                                                                            \
else if(v.VecSector==2 || v.VecSector==5)                                   \
  {     v.Ta= v.tmp3+v.tmp2;                                                \
        v.Tb= v.tmp1;                                                       \
        v.Tc=-v.tmp1;                                                       \
  }                                                                         \
                                                                            \
else                                                                        \
  {     v.Ta= v.tmp3;                                                       \
        v.Tb=-v.tmp3;                                                       \
        v.Tc=-(v.tmp1+v.tmp2);                                              \
  }                                                                         \


//使用交错零矢量
#define SVGENDPWM_MACRO(v)		                                    \
								            \
	v.tmp1= v.Ubeta;						    \
	v.tmp2= v.Ubeta/2.0f + 0.866f*v.Ualpha;				    \
        v.tmp3= v.tmp2 - v.tmp1;					    \
									    \
	v.VecSector=3;							    \
	v.VecSector=(v.tmp2> 0)?( v.VecSector-1):v.VecSector;		    \
	v.VecSector=(v.tmp3> 0)?( v.VecSector-1):v.VecSector;		    \
	v.VecSector=(v.tmp1< 0)?(7-v.VecSector) :v.VecSector;		    \
									    \
    if     (v.VecSector==1 || v.VecSector==6)				    \
		{							    \
			v.Ta= 0; 					    \
			v.Tb= v.tmp3; 					    \
			v.Tc= v.tmp2 ;					    \
		}							    \
	else if(v.VecSector==2 || v.VecSector==3)			    \
		{							    \
			v.Ta= -v.tmp3; 					    \
			v.Tb= 0; 					    \
			v.Tc= v.tmp1;					    \
		}							    \
	else 								    \
		{							    \
			v.Ta= -v.tmp2; 					    \
			v.Tb= -v.tmp1; 					    \
			v.Tc= 0;					    \
		}							    \
									    \
								            \
	v.Ta= -(v.Ta)*2.0f+(1.0f);				            \
	v.Tb= -(v.Tb)*2.0f+(1.0f);				            \
	v.Tc= -(v.Tc)*2.0f+(1.0f);				            \



// __SVGEN__

#define CNTL_2P2Z_F_COEFFS_DEFAULTS_D { -0.0040174, -0.0140741, 0.0181055, -0.7522776,1.7522776,0.5,-1,-1 }
#define CNTL_2P2Z_F_VARS_DEFAULTS_D { 0,0,0,0,0,0,0,0 }

#define CNTL_2P2Z_F_COEFFS_DEFAULTS_Q { 0, 0.6082391,  -0.2186002, 0, -0.9844144,0.2,-0.2,-0.2 }
#define CNTL_2P2Z_F_VARS_DEFAULTS_Q { 0,0,0,0,0,0,0,0 }

typedef struct {
    // Coefficients
    float Coeff_B2;
    float Coeff_B1;
    float Coeff_B0;
    float Coeff_A2;
    float Coeff_A1;

    // Output saturation limits
    float Max;
    float IMin;
    float Min;
} CNTL_2P2Z_F_COEFFS;

typedef struct {
    float Out1;
    float Out2;
    // Internal values
    float Errn;
    float Errn1;
    float Errn2;
    // Inputs
    float Ref;
    float Fdbk;
    // Output values
    float Out;
} CNTL_2P2Z_F_VARS;


//#define CNTL_3P3Z_F_COEFFS_DEFAULTS_D { -0.0040174, -0.0140741, 0.0181055, -0.7522776,1.7522776,0.5,-1,-1 }
//#define CNTL_3P3Z_F_VARS_DEFAULTS_D { 0,0,0,0,0,0,0,0 }
#define CNTL_3P3Z_F_COEFFS_DEFAULTS_Q { 0, 4.2673, -8.8115, 4.5442, 0, -0.9691, 1.9691,0.05,-0.1,-0.1 }
#define CNTL_3P3Z_F_VARS_DEFAULTS_Q { 0,0,0,0,0,0,0,0,0,0,0 }

typedef struct {
    // Coefficients
    float Coeff_B3;
    float Coeff_B2;
    float Coeff_B1;
    float Coeff_B0;
    float Coeff_A3;
    float Coeff_A2;
    float Coeff_A1;

    // Output saturation limits
    float Max;
    float IMin;
    float Min;
} CNTL_3P3Z_F_COEFFS;

typedef struct {
    float OutPresat;
    float Out1;
    float Out2;
    float Out3;
    // Internal values
    float Errn;
    float Errn1;
    float Errn2;
    float Errn3;
    // Inputs
    float Ref;
    float Fdbk;
    // Output values
    float Out;
} CNTL_3P3Z_F_VARS;

//*********** Function Declarations *******//
//void CNTL_2P2Z_F_COEFFS_init(CNTL_2P2Z_F_COEFFS *v);
//void CNTL_2P2Z_F_VARS_init(CNTL_2P2Z_F_VARS *k);
//void CNTL_2P2Z_F_FUNC(CNTL_2P2Z_F_COEFFS *v, CNTL_2P2Z_F_VARS *k);
//void CNTL_2P2Z_F_ASM(CNTL_2P2Z_F_COEFFS *v, CNTL_2P2Z_F_VARS *k);

//*********** Macro Definition ***********//
#define CNTL_3P3Z_F_MACRO(v, k)                                                                                     \
/* Calculate error */                                                                                               \
    k.Errn = k.Ref - k.Fdbk;                                                                                        \
    /* Calculate the pre-saturated output */                                                                        \
    k.OutPresat = (v.Coeff_A1 * k.Out1) + (v.Coeff_A2 * k.Out2) + (v.Coeff_A3 * k.Out3) + (v.Coeff_B0 * k.Errn)     \
                    + (v.Coeff_B1 * k.Errn1) + (v.Coeff_B2 * k.Errn2) + (v.Coeff_B3 * k.Errn3);                     \
                                                                                                                    \
    /* Update error values */                                                                                       \
    k.Errn3 = k.Errn2;                                                                                              \
    k.Errn2 = k.Errn1;                                                                                              \
    k.Errn1 = k.Errn;                                                                                               \
                                                                                                                    \
    /* Determine new output */                                                                                      \
    k.Out = k.OutPresat;                                                                                            \
    k.Out = (k.Out < v.Max) ? k.Out : v.Max;                                                                        \
    k.Out = (k.Out > v.IMin) ? k.Out : v.IMin;                                                                      \
                                                                                                                    \
    /* Store outputs */                                                                                             \
    k.Out3 = k.Out2;                                                                                                \
    k.Out2 = k.Out1;                                                                                                \
    k.Out1 = k.Out;                                                                                                 \
    /* Saturate output */                                                                                           \
    k.Out = ((k.Out > v.Min) ? k.Out : v.Min);

/* CNTL_2P2Z_F_H_ */


//*********** Macro Definition ***********//
#define CNTL_2P2Z_F_MACRO(v, k)                                                             \
    /* Calculate error */                                                                   \
    k.Errn = k.Ref - k.Fdbk;                                                                \
    k.Out = (v.Coeff_A2*k.Out2) + (v.Coeff_A1 *k.Out1) + (v.Coeff_B2 *k.Errn2)              \
                    + (v.Coeff_B1 * k.Errn1) + (v.Coeff_B0 * k.Errn);                       \
    /* Update error values */                                                               \
    k.Errn2 = k.Errn1;                                                                      \
    k.Errn1 = k.Errn;                                                                       \
    /* Determine new output */                                                              \
    k.Out = (k.Out < v.Max) ? k.Out : v.Max;                                                \
    k.Out = (k.Out > v.IMin) ? k.Out : v.IMin;                                              \
    /* Store outputs */                                                                     \
    k.Out2 = k.Out1;                                                                        \
    k.Out1 = k.Out;                                                                         \
    /* Saturated output */                                                                  \
    k.Out = ((k.Out > v.Min) ? k.Out : v.Min);

/* CNTL_2P2Z_F_H_ */

extern void Set_PfcOff(void);
extern void Set_PfcOn(void);
extern void Init_pfc(void (*PFC_PWM_Ptr)(uint16 RCmp, uint16 SCmp, uint16 TCmp), void (*PFC_GetAD)(PFC_ADCRESULT_REF* ));
extern void pfc_controlfunction(void);
extern void SPLL_3PH_SOGI_FLL_config(SPLL_3PH_SOGI_FLL *spll_obj,
                         float acFreq,
                         float isrFrequency,
                         float lpf_b0,
                         float lpf_b1,
                         float k,
                         float gamma);
#endif


