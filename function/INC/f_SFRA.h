#ifndef _F_SFRA_H_
#define _F_SFRA_H_

#include "InitCpuBase.h"
#include "SFRA_F_Include.h"

extern volatile SFRA_F sfra1;
void   setupSCIconnectionForSFRA();
void   SerialHostComms();
void   SFRA_RUNBACKGROUND(void);
void   setupSFRA(void);

#endif

