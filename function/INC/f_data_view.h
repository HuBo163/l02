/*
 * f_data_view.h
 *
 *  Created on: 2021��6��8��
 *      Author: dfs_h
 */

#ifndef FUNCTION_INC_F_DATA_VIEW_H_
#define FUNCTION_INC_F_DATA_VIEW_H_

#include "F28x_Project.h"
#include "f_SCIDriver.h"

void communicate_init(Uint32 band_rate);
void txt_data();

#endif /* FUNCTION_INC_F_DATA_VIEW_H_ */
