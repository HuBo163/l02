/*******************************************************************************************

                  Copyright (c) 2016 Shenzhen Greatland Electrics Co.,Ltd

File Name:      ADCIf.h

Version:         V1.0

Date:

Author:

Input:

Output:

Description:

History:

*******************************************************************************************/


#ifndef     _ADCIF_H_
#define     _ADCIF_H_

#include "F28x_Project.h"
#include "FunctionCommon.h"


typedef struct
{

    int32 IuAD;
    int32 IvAD;
    int32 IwAD;

    int32 VdcAD;
    int32 IdcAD;


    int32 UuAD;
    int32 UvAD;
    int32 UwAD;

    int32 TempAD;
    /* �¶� ADֵ */


}StructADCResult;



extern StructADCResult gADCResult;

void    pfc_ADCSample(PFC_ADCRESULT_REF *pADCResult);
void    CmdVoltADCSample(StructADCResult *pADCResult);
void    PhaseTypeUVWCurrentADCSample(StructADCResult *pADCResult);
void    PhaseTypeWVUCurrentADCSample(StructADCResult * pADCResult);
void    TempADCSample(StructADCResult *pADCResult);
void    CtrlVolADCSample(StructADCResult *pADCResult);



#endif
