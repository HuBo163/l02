
#ifndef _F_RUN_H
#define _F_RUN_H


#include "F28x_Project.h"
#include "FunctionCommon.h"

#define MOTOR_STOP        0
#define MOTOR_RUN         1

#define READ_RUN        GpioDataRegs.GPBDAT.bit.GPIO44

//=====================================================================
// runFlag, 变频器运行过程中的状态字
//
struct RUN_FLAG_BITS
{                               // bits  description
    Uint16 run:1;               // 0    (总的)运行标志

    Uint16 common:1;            // 1    普通运行(非点动、非调谐)
    Uint16 jog:1;               // 2    点动运行
    Uint16 tune:1;              // 3    调谐运行

    Uint16 jogWhenRun:1;        // 4    运行中点动

    Uint16 accDecStatus:2;      // 6:5  0 恒速； 1 加速； 2 减速


// 之下的bit位在shutdown时不要清除
    Uint16 plc:1;               // 7     PLC运行
    Uint16 pid:1;               // 8     PID运行
    Uint16 torque:1;            // 9     转矩控制

    Uint16 dir:1;               // 10    设定频率方向(功能码F0-12运行方向之前), 0-fwd, 1-rev
    Uint16 curDir:1;            // 11    当前运行频率方向, 0-fwd, 1-rev
    Uint16 dirReversing:1;      // 12    正在反向标志, 0-当前没有反向, 1-正在反向
    Uint16 dirFinal:1;          // 13    设定频率方向(功能码F0-12运行方向之后), 0-fwd, 1-rev

    Uint16 servo:1;             // 14
    Uint16 rsvd:1;              // 15
};


union RUN_FLAG
{
    Uint16               all;
    struct RUN_FLAG_BITS bit;
};

extern union RUN_FLAG runFlag;

extern void Change_Para();
extern void Start_Control();
extern Uint16 Get_VfSpeed();
extern void  stop_power(void);

#endif
