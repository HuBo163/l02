#ifndef _FUNCTIONCODE_H
#define _FUNCTIONCODE_H

#include "F28x_Project.h"
#include "FunctionCommon.h"


#define GROUP   9

#define F0NUM    10
#define F1NUM    16
//#define F2NUM    10
#define F3NUM    15
//#define F4NUM    11
//#define F5NUM    10
#define F6NUM    11
#define F7NUM    14
#define F8NUM    9
#define F9NUM    44
//#define FANUM    10
//#define FBNUM    10
//#define FCNUM    10
//#define FDNUM    10
//#define FENUM    10
//#define FFNUM    10

#define REM_NUM 2

#define U0NUM    32


#define GetCodeIndex(code)      ((Uint16)((&(code)) - (&(funcCode.all[0]))))
#define GetGradeIndex(group, grade)  (funcCodeGradeSum[group] + (grade))

#define MAX_FRQ_INDEX           (GetCodeIndex(funcCode.code.maxFrq))           // F0-10   最大频率
#define UPPER_FRQ_INDEX         (GetCodeIndex(funcCode.code.upperFrq))         // F0-12   上限频率
#define LOWER_FRQ_INDEX         (GetCodeIndex(funcCode.code.lowerFrq))         // F0-14   下限频率
#define ACC_TIME1_INDEX         (GetCodeIndex(funcCode.code.accTime1))         // F0-17   加速时间1
#define DEC_TIME1_INDEX         (GetCodeIndex(funcCode.code.decTime1))         // F0-18   减速时间1
#define CARRIER_FRQ_INDEX       (GetCodeIndex(funcCode.code.carrierFrq))       // F0-15   载波频率


#define RATING_POWER_INDEX      (GetCodeIndex(funcCode.code.motorParaM1.elem.ratingPower))    // 电机额定功率
#define RATING_VOL_INDEX        (GetCodeIndex(funcCode.code.motorParaM1.elem.ratingVoltage))  // 电机额定电压
#define RATING_CUR_INDEX        (GetCodeIndex(funcCode.code.motorParaM1.elem.ratingCurrent))  // 电机额定电流


#define RATING_FRQ_INDEX        (GetCodeIndex(funcCode.code.motorParaM1.elem.ratingFrq))      // 电机额定频率
#define VF_FRQ1_INDEX           (GetCodeIndex(funcCode.code.vfFrq1))                          // F3-03   多点VF频率点1
#define VF_FRQ2_INDEX           (GetCodeIndex(funcCode.code.vfFrq2))                          // F3-05   多点VF频率点2
#define VF_FRQ3_INDEX           (GetCodeIndex(funcCode.code.vfFrq3))                          // F3-07   多点VF频率点3


#define FNUM_PARA      (F0NUM + F1NUM + F3NUM + F6NUM + F7NUM +  F7NUM + F9NUM )

//=====================================================================
//
// 功能码组，F0组，F1组, ...
//
// eepromCheckWord放在最前面
// 厂家参数、用户密码、用户定制菜单放在前面，防止增删功能码时被误修改。
// fc与eeprom有对应关系表，所以以上要求不需要了
//
//=====================================================================
struct FUNCCODE_GROUP
{
//======================================
    Uint16 f0[F0NUM];               // F0 基本功能组
    Uint16 f1[F1NUM];               // F1 电机参数
//    Uint16 f2[F2NUM];               // F2 矢量控制参数
    Uint16 f3[F3NUM];               // F3 V/F控制参数

//======================================
    Uint16 f6[F6NUM];               // F6 启停控制
    Uint16 f7[F7NUM];               // F7 键盘与显示
    Uint16 f8[F8NUM];
//======================================
    Uint16 f9[F9NUM];               // F9 故障与保护

//======================================
    Uint16 remember[REM_NUM];       // 掉电记忆

// 之前的功能码有上下限，属性；之后没有，节省空间
//======================================
//======================================
// 之后的数据不需要放在EEPROM中，仅RAM
/*    Uint16 u0[U0NUM];             // U0 显示
    Uint16 u1[U1NUM];               // U1
    Uint16 u2[U2NUM];               // U2
    Uint16 u3[U3NUM];               // U3

//======================================
    Uint16 u4[U4NUM];               // U4
    Uint16 u5[U5NUM];               // U5
    Uint16 u6[U6NUM];               // U6
    Uint16 u7[U7NUM];               // U7

//======================================
    Uint16 u8[U8NUM];               // U8
    Uint16 u9[U9NUM];               // U9
    Uint16 ua[UANUM];               // UA
    Uint16 ub[UBNUM];               // UB

//======================================
    Uint16 uc[UCNUM];               // UC
    Uint16 ud[UDNUM];               // UD
    Uint16 ue[UENUM];               // UE
    Uint16 uf[UFNUM];               // UF, 显示，性能调试使用
//====================================== */
    Uint16 u0[U0NUM];               // U0 显示
};


//=====================================================================
//
// 功能码，F0-00, F0-01, ..., F1-00, F1-01, ...
//
//=====================================================================
struct FUNCCODE_CODE
{
//======================================
// F0 基本功能组

    Uint16 presetFrq;               // F0-08  预置频率
    Uint16 maxFrq;                  // F0-10  最大频率
    Uint16 upperFrq;                // F0-12  上限频率数值设定
    Uint16 upperFrqOffset;          // F0-13  上限频率偏置
    Uint16 lowerFrq;                // F0-14  下限频率数值设定
    Uint16 carrierFrq;              // F0-15  载波频率
    Uint16 varFcByTem;              // F0-16  载波频率随温度调整
    Uint16 accTime1;                // F0-17  加速时间1
    Uint16 decTime1;                // F0-18  减速时间1
    Uint16 frqPoint;                // F0-22  频率指令小数点

//======================================
// F1 电机参数
    union MOTOR_PARA motorParaM1;   // F1-00  F1-26 第1电机参数


//======================================
// F3 V/F控制参数
    Uint16 vfCurve;                 // F3-00  VF曲线设定
    Uint16 torqueBoost;             // F3-01  转矩提升
    Uint16 boostCloseFrq;           // F3-02  转矩提升截止频率
    Uint16 vfFrq1;                  // F3-03  多点VF频率点1
    Uint16 vfVol1;                  // F3-04  多点VF电压点1
    Uint16 vfFrq2;                  // F3-05  多点VF频率点2
    Uint16 vfVol2;                  // F3-06  多点VF电压点2
    Uint16 vfFrq3;                  // F3-07  多点VF频率点3
    Uint16 vfVol3;                  // F3-08  多点VF电压点3
    Uint16 slipCompCoef;            // F3-09  转差补偿系数
    Uint16 vfOverMagGain;           // F3-10  VF过励磁增益
    Uint16 antiVibrateGain;         // F3-11  振荡抑制增益

    Uint16 vfVoltageSrc;            // F3-13  VF分离的电压源
    Uint16 vfVoltageDigtalSet;      // F3-14  VF分离的电压源数字设定
    Uint16 vfVoltageAccTime;        // F3-15  VF分离的电压上升时间


// F6 启停控制
    Uint16 startMode;               // F6-00  启动方式
    Uint16 speedTrackMode;          // F6-01  转速跟踪方式
    Uint16 speedTrackVelocity;      // F6-02  转速跟踪快慢
    Uint16 startFrq;                // F6-03  启动频率
    Uint16 startFrqTime;            // F6-04  启动频率保持时间
    Uint16 startBrakeCurrent;       // F6-05  启动直流制动电流
    Uint16 startBrakeTime;          // F6-06  启动直流制动时间
    Uint16 accDecSpdCurve;          // F6-07  加减速方式
    Uint16 sCurveStartPhaseTime;    // F6-08  S曲线开始段时间比例
    Uint16 sCurveEndPhaseTime;      // F6-09  S曲线结束段时间比例
    Uint16 brakeDutyRatio;          // F6-15  制动使用率

//======================================
// F7 键盘与显示
    Uint16 mfkKeyFunc;              // F7-01  MF.K键功能选择
    Uint16 stopKeyFunc;             // F7-02  STOP键功能
    Uint16 ledDispParaRun1;         // F7-03  LED运行显示参数1
    Uint16 ledDispParaRun2;         // F7-04  LED运行显示参数2

    Uint16 ledDispParaStop;         // F7-05  LED停机显示参数
    Uint16 speedDispCoeff;          // F7-06  负载速度显示系数
    Uint16 radiatorTemp;            // F7-07  逆变器模块散热器温度
    Uint16 temp2;                   // F7-08  整流桥散热器温度
    Uint16 runTimeAddup;            // F7-09  累计运行时间, 单位: h
    Uint16 productVersion;          // F7-10  产品号
    Uint16 softVersion;             // F7-11  软件版本号
    Uint16 speedDispPointPos;       // F7-12  负载速度显示小数点位置
    Uint16 powerUpTimeAddup;        // F7-13  累计上电时间
    Uint16 powerAddup;              // F7-14  累计耗电量



    Uint16 oCurrentChkValue;        // F8-34  零电流检测值
    Uint16 oCurrentChkTime;         // F8-35  零电流检测延迟时间
    Uint16 softOCValue;             // F8-36  软件过流点
    Uint16 softOCDelay;             // F8-37  软件过流检测延迟时间

    Uint16 currentArriveValue1;     // F8-38  电流到达检测值1
    Uint16 currentArriveRange1;     // F8-39  电流到达检测1幅度
    Uint16 currentArriveValue2;     // F8-40  电流到达检测值1
    Uint16 currentArriveRange2;     // F8-41  电流到达检测1幅度


    Uint16 temperatureArrive;       // F8-47  模块温度到达


//======================================
// F9 故障与保护
    Uint16 overload;                    // F9-00  电机过载保护选择
    Uint16 overloadGain;                // F9-01  电机过载保护增益
    Uint16 foreOverloadCoef;            // F9-02  电机过载预警系数
    Uint16 ovGain;                      // F9-03  过压失速增益
    Uint16 ovPoint;                     // F9-04  过压失速保护电压

    Uint16 ocGain;                      // F9-05  过流失速增益
    Uint16 ocPoint;                     // F9-06  过流失速保护电流
    Uint16 shortCheckMode;              // F9-07  上电对地短路保护功能
    Uint16 rsvdF91;                     // F9-08  保留
    Uint16 errAutoRstNumMax;            // F9-09  故障自动复位次数

    Uint16 errAutoRstRelayAct;          // F9-10  故障自动复位期间故障继电器动作选择
    Uint16 errAutoRstSpaceTime;         // F9-11  故障自动复位间隔时间, 0.1s
    Uint16 inPhaseLossProtect;          // F9-12  输入缺相保护选择
    Uint16 outPhaseLossProtect;         // F9-13  输出缺相保护选择
    Uint16 errorLatest1;                // F9-14  第一次故障类型

    Uint16 errorLatest2;                // F9-15  第二次故障类型
    Uint16 errorLatest3;                // F9-16  第三次(最近一次)故障类型

    union ERROR_SCENE errorScene3;      // F9-17  第三次(最近一次)故障时频率
                                        // F9-18  第三次(最近一次)故障时母线电流
                                        // F9-19  第三次(最近一次)故障时母线电压
                                        // F9-20  第三次(最近一次)故障时计算输出电流
                                        // F9-21  第三次(最近一次)故障时计算输出电压

                                        // F9-22  第三次(最近一次)故障时A相电流
                                        // F9-23  第三次(最近一次)故障时B相电流
                                        // F9-24  第三次(最近一次)故障时C相电流


    union ERROR_SCENE errorScene2;      // F9-27  第二次故障现场


    union ERROR_SCENE errorScene1;      // F9-37  第一次故障现场


    Uint16 motorOtMode;                 // F9-56  电机温度传感器类型
    Uint16 motorOtProtect;              // F9-57  电机过热保护阈值
    Uint16 motorOtCoef;                 // F9-58  电机过热预报警阈值



    Uint16 dispParaRunBit;              // 运行时LED显示参数的bit位值
    Uint16 dispParaStopBit;             // 停机时LED显示参数的bit位置
//======================================
    Uint16 u0[U0NUM];                   // U0 显示
    Uint16 updata;            // 更新程序标志
};


#define FNUM_ALL   100
//=====================================================================
//
// 功能码的定义。
// 联合体，成员分别为数组，结构体，结构体
// 于是，一个功能码的访问，有三种方式:
// funcCode.all[index]     index = GetCodeIndex(funcCode.code.presetFrq);
// funcCode.group.f0[8]    index = GetCodeIndex(funcCode.group.f0[8]);
// funcCode.code.presetFrq
//
//=====================================================================
typedef union FUNCCODE_ALL_UNION
{
    Uint16 all[FNUM_PARA];

    struct FUNCCODE_GROUP group;

    struct FUNCCODE_CODE code;
} FUNCCODE_ALL;





extern FUNCCODE_ALL funcCode;
extern const FUNCCODE_ATTRIBUTE funcCodeAttribute[FNUM_PARA];
extern const Uint16 funcCodeGradeSum[FUNCCODE_GROUP_NUM];
extern const Uint16 *grade_adrress[9];

#endif

