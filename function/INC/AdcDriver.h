#ifndef     ADCDRIVER_H_
#define     ADCDRIVER_H_

#include "F28x_Project.h"
#include "FunctionCommon.h"

void InitAdc(void);
uint16  GetADCAResult(uint16 Chenal);
uint16  GetADCBResult(uint16 Chenal);
//uint16  GetADCCResult(uint16 Chenal);
uint16  GetADCDResult(uint16 Chenal);



#endif
