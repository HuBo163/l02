#ifndef F_DISPLAY_H
#define F_DISPLAY_H

#include "F28x_Project.h"
#include "FunctionCommon.h"


#define DISP_FRQ_RUN            0   //  0, 运行频率
#define DISP_BUS_VOL            1   //  1, 母线电压
#define DISP_BUS_CUL            2   //  2, 母线电流
#define DISP_OUT_VOL            3   //  3, 输出电压实时值
#define DISP_OUT_CUL            4   //  4, 输出电流实时值
#define DISP_OUT_IA             5   //  5, 输出电流IA
#define DISP_OUT_IB             6   //  6, 输出电流IB
#define DISP_OUT_IC             7   //  7, 输出电流IC
#define DISP_OUT_UA             8   //  8, 输出电压UA
#define DISP_OUT_UB             9   //  9, 输出电压UB
#define DISP_OUT_UC            10   // 10, 输出电压UC
#define DISP_OUT_TEMP          11   // 11, 工作温度

//#define DISP_FRQ_AIM          17  //  17, 设定频率
//#define DISP_OUT_CURRENT        4   //  4, 输出电流
//#define DISP_OUT_POWER          5   //  5, 输出功率
//#define DISP_FRQ_RUN_FDB        19  //  19,反馈速度
//#define DISP_FRQ_COMM           28  //  28,通讯设定值
//#define DISP_P2P_COMM_SEND      63  //  63,点对点通讯数据发送
////#define DISP_P2P_COMM_REV       64  //  64,点对点通讯数据接收
//#define DISP_FRQ_FDB            29  //  29,反馈速度
//#define DISP_FRQ_X              30  //  30, 主频率X显示
//#define DISP_FRQ_Y              31  //  31, 辅助频率Y显示
//#define DISP_LOAD_SPEED         14  //  运行时负载速度显示的bit位置


#define MENU_LEVEL_NUM  6   // 一共有_级菜单


#define ON_UP_KEY       1
#define ON_DOWN_KEY     2

enum MENU_LEVEL
{
    MENU_LEVEL_0,           // 0级菜单
    MENU_LEVEL_1,           // 1级菜单
    MENU_LEVEL_2,           // 2级菜单
    MENU_LEVEL_3,           // 3级菜单
    MENU_LEVEL_PWD_HINT,    // 提示输入密码
    MENU_LEVEL_PWD_INPUT    // 输入密码
};

// 将菜单操作封装在一起
typedef struct tagSysMenu
{
    void (*onPrgFunc)();         // 在当前菜单按下 PRG   键的处理函数指针
    void (*onUpFunc)();          // 在当前菜单按下 UP    键的处理函数指针
    void (*onEnterFunc)();       // 在当前菜单按下 ENTER 键的处理函数指针
    void (*onMfkFunc)();         // 在当前菜单按下 MF.K  键的处理函数指针
    void (*onDownFunc)();        // 在当前菜单按下 DOWN  键的处理函数指针
    void (*onShiftFunc)();       // 在当前菜单按下 SHIFT 键的处理函数指针
    void (*onRunFunc)();         // 在当前菜单按下 RUN   键的处理函数指针
    void (*onStopFunc)();        // 在当前菜单按下 STOP  键的处理函数指针
    void (*onQuickFunc)();       // 在当前菜单按下 QUICK 键的处理函数指针

    void (*UpdateDisplayBuffer)(); // 当前菜单下更新显示数据缓冲的函数指针
}sysMenu, *sysMenuHandle;


extern const sysMenu menu[MENU_LEVEL_NUM];

#endif
