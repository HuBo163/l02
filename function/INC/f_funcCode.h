
#ifndef __F_FUNCCODE_H__
#define __F_FUNCCODE_H__

#include "F28x_Project.h"
#include "FunctionCommon.h"

//=====================================================================
// 功能码属性位定义
//
// 单位
#define ATTRIBUTE_UNIT_HZ_BIT           0
#define ATTRIBUTE_UNIT_A_BIT            1
#define ATTRIBUTE_UNIT_V_BIT            2

// 读写属性
#define ATTRIBUTE_READ_AND_WRITE        0   // (任何时候)可写
#define ATTRIBUTE_READ_ONLY_WHEN_RUN    1   // 运行时只读
#define ATTRIBUTE_READ_ONLY_ANYTIME     2   // 只读

// 多功能码组合属性
#define ATTRIBUTE_MULTI_LIMIT_SINGLE    0   // 单独的功能码
#define ATTRIBUTE_MULTI_LIMIT_DEC       1   // 多个功能码，十进制
#define ATTRIBUTE_MULTI_LIMIT_HEX       2   // 多个功能码，十六进制


#define FUNCCODE_GROUP_NUM  10  // 包括预留的组

#define FUNCCODE_GROUP_F0   0       // F0组
#define FUNCCODE_GROUP_F1   1       // F1组
#define FUNCCODE_GROUP_U0   9     // U0组，显示

struct  FUNC_ATTRIBUTE_BITS
{                           // bits   description
    Uint16 point:3;         // 2:0    radix point,小数点。
                            //        0-无小数点，1-1位小数，...，4-4位小数
                            //        (0.0000-100,00.000-011,000.00-010,0000.0-001,00000-000)
    Uint16 unit:3;          // 5:3    unit,单位
                            //        1-hz, 2-A, 3-RPM, 4-V, 6-%; 001-Hz, 010-A, 100-V
    Uint16 displayBits:3;   // 8:6    5个数码管要显示的位数。0-显示0位，1-显示1位，...，5-显示5位
    Uint16 upperLimit:1;    // 9      1-参数由上限相关功能码限制，0-直接由上限限制
    Uint16 lowerLimit:1;    // 10     1-参数由下限相关功能码限制，0-直接由下限限制
    Uint16 writable:2;      // 12:11  参数读写特性，00-可以读写, 01-运行中只读，10-参数只读
    Uint16 signal:1;        // 13     符号，unsignal-0; signal-1
    Uint16 multiLimit:2;    // 15:14  该功能码为多个功能码的组合.
                            //        00-单独功能码(非组合);
                            //        01-十进制,  多个功能码的组合;
                            //        10-十六进制,多个功能码的组合;
};

union FUNC_ATTRIBUTE
{
   Uint16                      all;
   struct FUNC_ATTRIBUTE_BITS  bit;
};
//=====================================================================



//=====================================================================
// 功能码属性表：上限、下限、属性
// 功能码的出厂值，包括EEPROM_CHECK、掉电记忆，但不包括显示组
typedef struct FUNCCODE_ATTRIBUTE_STRUCT
{
    Uint16                  lower;          // 下限
    Uint16                  upper;          // 上限
    Uint16                  init;           // 出厂值
    union FUNC_ATTRIBUTE    attribute;      // 属性

    Uint16                  eepromIndex;    // 对应EEPROM存储的index
} FUNCCODE_ATTRIBUTE;


struct MOTOR_PARA_STRUCT
{
    // 电机基本参数
    Uint16 motorType;               // F1-00  电机类型选择
    Uint16 ratingPower;             // F1-01  电机额定功率
    Uint16 ratingVoltage;           // F1-02  电机额定电压
    Uint16 ratingCurrent;           // F1-03  电机额定电流
    Uint16 ratingFrq;               // F1-04  电机额定频率
    Uint16 ratingSpeed;             // F1-05  电机额定转速

    // 异步机调谐参数
    Uint16 statorResistance;        // F1-06  异步机定子电阻
    Uint16 rotorResistance;         // F1-07  异步机转子电阻
    Uint16 leakInductance;          // F1-08  异步机漏感抗
    Uint16 mutualInductance;        // F1-09  异步机互感抗
    Uint16 zeroLoadCurrent;         // F1-10  异步机空载电流
    Uint16 rsvdF11[5];


};

union MOTOR_PARA
{
    Uint16 all[sizeof(struct MOTOR_PARA_STRUCT)];

    struct MOTOR_PARA_STRUCT elem;
};


//=================================
struct ERROR_SCENE_STRUCT
{
    Uint16 errorFrq;                    // 第三次(最近一次)故障时频率
    Uint16 errorBusCurrent;             // 第三次(最近一次)故障时母线电流
    Uint16 errorBusVoltage;             // 第三次(最近一次)故障时母线电压
    Uint16 errorOutcurrent;             // 第三次(最近一次)故障时计算输出电压
    Uint16 errorOutvoltage;             // 第三次(最近一次)故障时计算输出电流

    Uint16 errorCurrentA;               // 第三次(最近一次)故障时A相电流
    Uint16 errorCurrentB;               // 第三次(最近一次)故障时B相电流
    Uint16 errorCurrentC;               // 第三次(最近一次)故障时C相电流
};


union ERROR_SCENE
{
    Uint16 all[sizeof(struct ERROR_SCENE_STRUCT)];

    struct ERROR_SCENE_STRUCT elem;
};


extern const Uint16 menu_group[FUNCCODE_GROUP_NUM];
extern const Uint16 menu_group[FUNCCODE_GROUP_NUM];


#endif
