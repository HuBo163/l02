/*
 * f_data_enterchange.h
 *
 *  Created on: 2020年4月14日
 *      Author: dfs_h
 */

#ifndef FUNCTION_SRC_F_DATA_ENTERCHANGE_H_
#define FUNCTION_SRC_F_DATA_ENTERCHANGE_H_

#include "F28x_Project.h"
#include "FunctionCommon.h"


/*****************************************
设备型号
Bit15~11: 产品类型(A、B、…、Z) Bit10~4:  产品编号  Bit3~0: 程序编号
((uint16)((((X1)&0x1F)<<11)|(((X2)&0x3F)<<5)|((X3)&0x07)))
X1:(Bit15~11)产品类型(A、B、…、Z)(1、2、...、26)
A	B	C	D	E	F	G	H	I	J
1	2	3	4	5	6	7	8	9	10
K 	L 	M       N 	O	P	Q	R	S	T
11	12	13	14	15	16	17	18	19	20
U 	V	W	X	Y	Z
21	22	23	24	25	26
X2:(Bit10~4)产品编号1~127
X3:(Bit3~0)子编号(A、B、…、O)0~15
A	B	C	D	E	F	G	H	I	J
1	2	3	4	5	6	7	8	9	10
K 	L 	M       N 	O
11	12	13	14	15
*****************************************/
#define SOFTWARE_REV1 	((((12)&0x1F)<<11)|(((2)&0x7F)<<4)|((1)&0x0F))
															//H：01000   2：000110 V01.01：001.01
/*****************************************
软件版本号
//Bit15~12: 版本号第一位 V(0~15)
  Bit11~8 : 版本号第二位 .(0~15)
  Bit7~0  : 版本号第三位 .(0~255)
*****************************************/
#define SOFTWARE_NUMBER (0x0105)                //软件版本号L02AV0.1.5



void data_init();
void modbus_data_enterchange(uint16 chage_mode);

extern Uint16 get_motorexcur_flag(void);
extern Uint16 get_pfcexcur_flag(void);


#endif /* FUNCTION_SRC_F_DATA_ENTERCHANGE_H_ */
