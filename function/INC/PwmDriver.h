
#ifndef     PWMDRIVER_H_
#define     PWMDRIVER_H_

#include "F28x_Project.h"


#define     LED_ON               (GpioDataRegs.GPCCLEAR.bit.GPIO70 = 1)
#define     LED_OFF              (GpioDataRegs.GPCSET.bit.GPIO70 = 1)
#define     LED_ON_OFF           (GpioDataRegs.GPCTOGGLE.bit.GPIO70 = 1)
#define     PWM_EN               (GpioDataRegs.GPCCLEAR.bit.GPIO93 = 1)
#define     PWM_DIS              (GpioDataRegs.GPCSET.bit.GPIO93 = 1)     //电机驱动使能信号

#define     PFCPWM_EN            (GpioDataRegs.GPCCLEAR.bit.GPIO94 = 1)
#define     PFCPWM_DIS           (GpioDataRegs.GPCSET.bit.GPIO94 = 1)    //PFC驱动使能信号


//#define     THETA_ON_OFF         (GpioDataRegs.GPCTOGGLE.bit.GPIO64 = 1)

#define     INV_FAULT_H          (GpioDataRegs.GPCDAT.bit.GPIO74)
#define     INV_FAULT_L          (GpioDataRegs.GPCDAT.bit.GPIO75)
#define     PFC_FAULT_H          (GpioDataRegs.GPADAT.bit.GPIO22)
#define     PFC_FAULT_L          (GpioDataRegs.GPADAT.bit.GPIO22)


//以下为PFC控制相关
void InitPFCEPWM(void);                                                  //PFC控制参数初始化程序代码
void PhaseTypeRSTPWMCmp(uint16 RCmp, uint16 SCmp, uint16 TCmp);          //PFC RST三相控制比较值
void PwmPFCOn(void);                                                     //pfc开通函数
void PwmPFCOff(void);
void InitPFC_Ptr(void (*function_Ptr)(void));

//以下为电机控制相关
void   InitEPWM(void);                                                    //变频器控制初始化程序代码
void   PhaseTypeWVUPWMCmp(uint16 UCmp, uint16 VCmp, uint16 WCmp);         //变频器控制参数比较值
void   PhaseTypeUVWPWMCmp(uint16 UCmp, uint16 VCmp, uint16 WCmp);
void   PwmOn(void);
void   PwmOff(void);
void   WriteOutputPort(uint16 OutputNum,uint16 OutVal);
uint16 ReadInputPort(void);
void   IGBTSelfTest(void);
int16  GetPwmUpDownFlag(int32 Prd);
void   Initmotor_Ptr(void (*function_Ptr)(void));


#endif
