#ifndef     _FUNCTIONCOMMON_H_
#define     _FUNCTIONCOMMON_H_

#define DSP_CLOCK 200

#include"pfc_control.h"
#include <f_keyboard.h>
#include"math.h"
#include"InitCpuBase.h"
#include"PwmDriver.h"
#include"AdcDriver.h"
#include"GetADCresult.h"
#include"motorctr.h"
#include"motor_protect.h"


#include"f_SpiDriver.h"
#include"f_display.h"
#include"f_keyboard.h"
#include"f_funcCode.h"
#include "calvalue.h"
#include"f_run.h"
#include"f_functionCode.h"
#include"f_SCIDriver.h"
#include"f_CpuTimers.h"
#include"f_dma.h"


#endif
