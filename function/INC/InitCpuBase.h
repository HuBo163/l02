#ifndef __INITCPUDRIVER_H__
#define __INITCPUDRIVER_H__

#include "F28x_Project.h"
#include "FunctionCommon.h"
#include "mb.h"  //FreeModbus
#include "mbport.h"
#include "inc/port.h"
#include "f_cladriver.h"


void InitCpuBase(void);
void EneGlobalIRQ(void);
void SysInit(void);
void DisGlobalIRQ(void);
void SCIA_Init(long SCI_VBUS_CLOCKRATE, long SCI_BAUDRATE);

#endif
