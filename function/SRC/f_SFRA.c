#include"f_SFRA.h"

//SFRA Options
//1 FRA for the Current Loop
//2 FRA for the Voltage Loop
//3 FRA for the Voltage Balance Loop
#define SFRA_CURRENT 1
#define SFRA_VOLTAGE 2
#define SFRA_BALANCECNTL 3
#if INCR_BUILD ==4
#define SFRA_TYPE           SFRA_BALANCECNTL
#elif INCR_BUILD==3
#define SFRA_TYPE           SFRA_VOLTAGE
#else
#define SFRA_TYPE           SFRA_CURRENT
#endif
#define SFRA_ISR_FREQ       20000

//==================================================================================
// System Settings
//----------------------------------------------------------------------------------
//Add any system specific setting below
#if SFRA_TYPE==SFRA_VOLTAGE || SFRA_TYPE==SFRA_BALANCECNTL // voltage loop/ balance loop
#define SFRA_FREQ_START 2
#define SFRA_FREQ_LENGTH 30
//SFRA step Multiply = 10^(1/No of steps per decade(40))
#define FREQ_STEP_MULTIPLY (float)1.08
#define SFRA_AMPLITUDE (float)0.003
#elif SFRA_TYPE==SFRA_CURRENT  // current loop
#define SFRA_FREQ_START 50
#define SFRA_FREQ_LENGTH 60
//SFRA step Multiply = 10^(1/No of steps per decade(83))  200-3k decade =11
//100(length)/11(steps)*10(per decade) = 83.3   10^(1/83.3)=1.02565
#define FREQ_STEP_MULTIPLY (float)1.0797
#define SFRA_AMPLITUDE (float)0.02
#endif


//--------------------------------- SFRA Related Variables ----------------------------
float plantMagVect[SFRA_FREQ_LENGTH];
float plantPhaseVect[SFRA_FREQ_LENGTH];
float olMagVect[SFRA_FREQ_LENGTH];
float olPhaseVect[SFRA_FREQ_LENGTH];
float freqVect[SFRA_FREQ_LENGTH];

volatile SFRA_F sfra1;

//extern to access tables in ROM
extern long FPUsinTable[];
//--------------------------------- SFRA GUI Related Variables ----------------------
volatile int16_t SerialCommsTimer=0;
volatile int16_t CommsOKflg=0;
//Flag for reinitializing SFRA variables
volatile int16_t initializationFlag;

//GUI support variables
// sets a limit on the amount of external GUI controls - increase as necessary
volatile int16_t  *varSetTxtList[16]; //16 textbox controlled variables
volatile int16_t  *varSetBtnList[16]; //16 button controlled variables
volatile int16_t  *varSetSldrList[16];//16 slider controlled variables
volatile int16_t  *varGetList[16];    //16 variables sendable to GUI
volatile int32_t  *arrayGetList[16];  //16 arrays sendable to GUI
volatile uint32_t *dataSetList[16];   //16 32-bit textbox or label controlled variables

//放在0.5ms的任务中执行
void SFRA_RUNBACKGROUND(void)
{
    static unsigned long long tims0_5ms=0;          //64位
    tims0_5ms++;
    if(tims0_5ms%2==0) //每隔1ms执行一次
    {
        SerialHostComms();
        SerialCommsTimer++;
    }
    if(tims0_5ms%2==0)
        SFRA_F_BACKGROUND(&sfra1);   //每隔5ms执行一次

    if(initializationFlag == 1)
    {
        SFRA_F_INIT(&sfra1);
        initializationFlag = 0;
        sfra1.start = 1;
    }
}


//TODO setupSFRA
void setupSFRA(void)
{
    unsigned int i=0;

    setupSCIconnectionForSFRA();

    //Specify the injection amplitude
    sfra1.amplitude=SFRA_AMPLITUDE;

    SFRA_F_INIT(&sfra1);

    //SFRA Related
    //SFRA Object Initialization

    //Specify the length of SFRA
    sfra1.Vec_Length=SFRA_FREQ_LENGTH;
    //Specify the SFRA ISR Frequency
    sfra1.ISR_Freq=SFRA_ISR_FREQ;
    //Specify the Start Frequency of the SFRA analysis
    sfra1.Freq_Start=SFRA_FREQ_START;
    //Specify the Frequency Step
    sfra1.Freq_Step=FREQ_STEP_MULTIPLY;
    //Assign array location to Pointers in the SFRA object
    sfra1.FreqVect=freqVect;
    sfra1.GH_MagVect=olMagVect;
    sfra1.GH_PhaseVect=olPhaseVect;
    sfra1.H_MagVect=plantMagVect;
    sfra1.H_PhaseVect=plantPhaseVect;

    // Re-initialize the frequency array to make SFRA sweep go fast
    #if SFRA_TYPE==SFRA_CURRENT// current loop
    sfra1.FreqVect[i++]=SFRA_FREQ_START;
    for(;i<sfra1.Vec_Length;i++)
    {
        sfra1.FreqVect[i]=sfra1.FreqVect[i-1]*sfra1.Freq_Step;
    }
    #else
    sfra1.FreqVect[0]=2;
    sfra1.FreqVect[1]=4;
    sfra1.FreqVect[2]=6;
    sfra1.FreqVect[3]=8;
    sfra1.FreqVect[4]=10;
    sfra1.FreqVect[5]=12;
    sfra1.FreqVect[6]=14;
    sfra1.FreqVect[7]=16;
    sfra1.FreqVect[8]=18;
    sfra1.FreqVect[9]=20;
    sfra1.FreqVect[10]=22;
    sfra1.FreqVect[11]=24;
    sfra1.FreqVect[12]=26;
    sfra1.FreqVect[13]=28;
    sfra1.FreqVect[14]=30;
    sfra1.FreqVect[15]=35;
    sfra1.FreqVect[16]=40;
    sfra1.FreqVect[17]=45;
    sfra1.FreqVect[18]=55;
    sfra1.FreqVect[19]=65;
    sfra1.FreqVect[20]=70;
    sfra1.FreqVect[21]=80;
    sfra1.FreqVect[22]=90;
    sfra1.FreqVect[23]=100;
    sfra1.FreqVect[24]=120;
    sfra1.FreqVect[25]=140;
    sfra1.FreqVect[26]=160;
    sfra1.FreqVect[27]=170;
    sfra1.FreqVect[28]=210;
    sfra1.FreqVect[29]=250;
    #endif



    //"Set" variables
    // assign GUI Buttons to desired flag addresses
    varSetBtnList[0] = (int16*)&initializationFlag;

    //"Get" variables
    //---------------------------------------
    // assign a GUI "getable" parameter address
    varGetList[0] = (int16*)&(sfra1.Vec_Length);
    varGetList[1] = (int16*)&(sfra1.status);
    varGetList[2] = (int16*)&(sfra1.FreqIndex);

    //"Setable" variables
    //----------------------------------------
    // assign GUI "setable" by Text parameter address
    dataSetList[0] = (Uint32*)&(sfra1.Freq_Start);
    dataSetList[1] = (Uint32*)&(sfra1.amplitude);
    dataSetList[2] = (Uint32*)&(sfra1.Freq_Step);

    // assign a GUI "getable" parameter array address
    arrayGetList[0] = (int32*)freqVect;
    arrayGetList[1] = (int32*)olMagVect;
    arrayGetList[2] = (int32*)olPhaseVect;
    arrayGetList[3] = (int32*)plantMagVect;
    arrayGetList[4] = (int32*)plantPhaseVect;
    arrayGetList[5] = (int32*)&(sfra1.Freq_Start);
    arrayGetList[6] = (int32*)&(sfra1.amplitude);
    arrayGetList[7] = (int32*)&(sfra1.Freq_Step);

    CommsOKflg = 0;
    SerialCommsTimer = 0;
}


//TODO setupSCIconnectionForSFRA()
void setupSCIconnectionForSFRA()
{

    // Use Gpio 84 and 85 for the SCI comms through JTAG
    EALLOW;
    GPIO_SetupPinMux(85, GPIO_MUX_CPU1, 5);
    GPIO_SetupPinOptions(85, GPIO_INPUT, GPIO_SYNC);
    GPIO_SetupPinMux(84, GPIO_MUX_CPU1, 5);
    GPIO_SetupPinOptions(84, GPIO_OUTPUT, GPIO_SYNC);
    EDIS;

    // 50000000 is the LSPCLK or the Clock used for the SCI Module
    // 57600 is the Baudrate desired of the SCI module
    SCIA_Init(50000000, 57600);
}
