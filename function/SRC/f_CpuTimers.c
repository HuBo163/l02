/*
 * f_CpuTimers.c
 *
 *  Created on: 2020年4月9日
 *      Author: dfs_h
 */

#include"f_CpuTimers.h"

#define     C_TIME_05MS         (DSP_CLOCK*500L)            //0.5ms对应的定时器1计数值
#define     C_TIME_20MS         (DSP_CLOCK*2000L)

//系统时钟初始化
void Systerm_TickInit(void)
{
    CpuTimer1Regs.PRD.all =  C_TIME_05MS;

    CpuTimer1Regs.TIM.all      = 0;   //计数器清零
    CpuTimer1Regs.TCR.bit.TIF  = 0x01;//清除中断标志位
    CpuTimer1Regs.TCR.bit.TIE  = 0x00;//不使能定时器中断

    ReloadCpuTimer1();
    StartCpuTimer1();  //InitCpuTimers()已经在modbus初始化中初始化了
}
