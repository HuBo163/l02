#include"f_run.h"
#include"SvpwmStd.h"
#include"motor_protect.h"

extern PFC_ADCRESULT_REF  gpfc_ad_result;

union RUN_FLAG runFlag;                    // 运行标识字
extern SVGENMF  g_mf;

static Uint16 gfault_flag = 0;
static Uint16 stop_bit = 1;

extern bool logic_protect(void);
//预充电控制逻辑
extern Uint16 pfc_get_prechargeflag(void);
void clear_errordata(void);


void  stop_power(void)
{
  runFlag.bit.run = MOTOR_STOP;
  Set_PfcOff();
}


Uint16 relay_control(void)
{
    Uint16 relay_flag=0;

    relay_flag = pfc_get_prechargeflag();

    if(relay_flag==0)
    {
       GpioDataRegs.GPASET.bit.GPIO24 = 1;
       GpioDataRegs.GPASET.bit.GPIO25 = 1;
    }
    else if(relay_flag==1)
    {
       GpioDataRegs.GPACLEAR.bit.GPIO24 = 1;
       GpioDataRegs.GPACLEAR.bit.GPIO25 = 1;
    }
    return 0;
}


//启机控制逻辑
unsigned char Start_Program(union RUN_FLAG flag)
{
    static unsigned int time=0;

    if(time<20)
      time++;
    if((MOTOR_RUN==flag.bit.run)&&(time<15))
    {
      GpioDataRegs.GPCCLEAR.bit.GPIO76 = 1;  //整流器故障清除信号
      GpioDataRegs.GPCCLEAR.bit.GPIO69 = 1;  //逆变器故障清除信号
      GpioDataRegs.GPCCLEAR.bit.GPIO71 = 1;  //驱动辅助供电信号目前一直给
    }
    else
    {
      GpioDataRegs.GPCSET.bit.GPIO76 = 1;    //整流器故障清除信号复位
      GpioDataRegs.GPCSET.bit.GPIO69 = 1;    //逆变器故障清除信号复位
      GpioDataRegs.GPCCLEAR.bit.GPIO71 = 1;  // 驱动辅助供电信号驱动辅助供电信号目前一直给
    }

    if( MOTOR_STOP==flag.bit.run)              //停机的时候接收管压降检测
    {
	GpioDataRegs.GPCSET.bit.GPIO76 = 1;    //整流器故障清除信号复位
	GpioDataRegs.GPCSET.bit.GPIO69 = 1;    //逆变器故障清除信号复位
	GpioDataRegs.GPCCLEAR.bit.GPIO71 = 1;  // 驱动辅助供电信号驱动辅助供电信号目前一直给
        time = 0;
    }

    if(time>15)
     return 1;
    else
      return 0;

}

extern uint16 FAN_control;

//风扇控制
void Fan_control(unsigned int flag)
{
  if(flag==1)
  {
      GpioDataRegs.GPASET.bit.GPIO20 = 1;
  }
  else
  {
      GpioDataRegs.GPACLEAR.bit.GPIO20 = 1;
  }
}


extern bool inv_pfc_protect(void);
//启动方式判断
void Start_Control()
{
    gfault_flag = logic_protect();
    gfault_flag |= inv_pfc_protect();
    gfault_flag |= relay_control();

    reset_errornow(gfault_flag);
    Fan_control(FAN_control);

    //变频器启动控制信号
    if( 1 == funcCode.code.startMode )    //面板启动
    {
        if(KEY_STOP == keyFunc )
        {
            stop_bit = 1;
        }
        if( KEY_RUN == keyFunc )
        {
            stop_bit = 0;
        }
        if(gfault_flag)
        {
           runFlag.bit.run = MOTOR_STOP;
           PwmOff();
        }
        else if( stop_bit )
       {
           if(g_mf.StepAngle<100)
           {
               runFlag.bit.run = MOTOR_STOP;
               g_mf.StepAngle = 0;
           }
        }
       else
       {
           runFlag.bit.run = MOTOR_RUN;
       }

    }
    else                                   //硬线启动
    {
        if(gfault_flag)                    //有故障直接关闭PWM输出
        {
           PwmOff();
           runFlag.bit.run = MOTOR_STOP;   //逆变器停止输出
           if( 0==READ_RUN )               //启动信号为0 清除现有故障
           {
               clear_errordata();
           }
        }
        else if(0==READ_RUN )             //无故障 ，收到停机信号
         {
            stop_bit = 1;                 //减速停机
            if(g_mf.StepAngle<100)        //输出频率小于1Hz 关闭PWM输出
            {
        	runFlag.bit.run = MOTOR_STOP;
        	 g_mf.StepAngle = 0;
            }
         }
        else                              //无故障启机
        {
            stop_bit = 0;
            runFlag.bit.run = MOTOR_RUN;
        }
     }

    if(Start_Program(runFlag))
    {
        runFlag.bit.run = MOTOR_RUN;
    }
    else
    {
        runFlag.bit.run = MOTOR_STOP;
    }
    //整流器控制信号， 未启动电机时 不启动pfc
    if(runFlag.bit.run == MOTOR_RUN)
    {
	Set_PfcOn();  //整个程序只在此处能开启PFC
    }
    else
    {
	Set_PfcOff();
    }
}


Uint16 read_stopbit(void)
{
    return stop_bit;
}


//参数传递
void Changerun_Para()
{
    if(gfault_flag )                             //有故障时，无输出
    {
       g_mf.StepAngle = 0;
       g_mf.Freq = 0;
       funcCode.group.u0[0] = 0;
    }
    else if( 0==stop_bit )        //无故障并且运行时
    {
       g_mf.Freq = funcCode.code.presetFrq;
       funcCode.group.u0[0]  = g_mf.StepAngle;
    }
    else                                         //无故障缓慢停机
    {
        g_mf.Freq = 0;
        funcCode.group.u0[0]  = g_mf.StepAngle;

    }
}


//获取正在运行中的VF频率
Uint16 Get_VfSpeed()
{
    return g_mf.StepAngle;
}

