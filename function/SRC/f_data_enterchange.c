/*
 * f_data_enterchange.c
 *
 *  Created on: 2020年4月14日
 *      Author: dfs_h
 */

#include"f_data_enterchange.h"
#include"inc/port.h"
#include"motorVf.h"
#include"motor_protect.h"
#include"pfc_data.h"

extern USHORT usRegHoldingBuf[REG_HOLDING_NREGS];  //保持寄存器,modbus协议既能修改该寄存器,又能读取该寄存器

extern DISPLAY_STRUCT  gdisplay;
extern VF_INFO_STRUCT  gVFPar;
extern error_type read_error_flag(void);
uint16 power_select = 0;                       //H05-H06选择标志位 0-H05  1-H06
uint16 FAN_control = 1;                        //0不开风扇， 1开风扇,默认开风扇

extern UVW_EXCURSION_STRUCT  gIExcursionInfo;//={ .Count=2000,.Coff=4280,.Coffdc=1400};
extern void  Set_protectType(Uint16 flag);
extern PFC_DISPLAY_STRUCT   gpfc_display;

void Change_vf_data()
{
    gVFPar.VFLineFreq1 = funcCode.code.vfFrq1;
    gVFPar.VFLineVolt1 = funcCode.code.vfVol1;
    gVFPar.VFLineFreq2 = funcCode.code.vfFrq2;
    gVFPar.VFLineVolt2 = funcCode.code.vfVol2;
    gVFPar.VFLineFreq3 = funcCode.code.vfFrq3;
    gVFPar.VFLineVolt3 = funcCode.code.vfVol3;

    gVFPar.Up_freq   = funcCode.code.accTime1;
    gVFPar.Down_freq = funcCode.code.decTime1;

}


void F0_data_display()
{
    uint16 i=0;
    uint16* pointer = &gdisplay.Udc;
    gdisplay.Udc = gpfc_display.Udc;

    for(i=0;i<sizeof(DISPLAY_STRUCT)-1;i++)
        funcCode.code.u0[i+1] = *(pointer+i);
 // funcCode.code.u0[0] = g_mf.StepAngle;                                        //输出频率    在f_run.c中已更改
 /* funcCode.code.u0[1] = gRealOut.U;                                            //输出电压
    funcCode.code.u0[2] = gRealOut.I;                                            //输出电流
    funcCode.code.u0[3] = gUUVW.dc;                                              //母线电压
    funcCode.code.u0[4] = gIUVW.dc;                                              //母线电流
    funcCode.code.u0[5] = gIUVWabs.U;                                            //输出电流A相
    funcCode.code.u0[6] = gIUVWabs.V;                                            //输出电流B相
    funcCode.code.u0[7] = gIUVWabs.W;                                            //输出电流C相
    funcCode.code.u0[8] = gUUVWabs.U;                                            //输出电压A相
    funcCode.code.u0[9] = gUUVWabs.V;                                            //输出电压B相
    funcCode.code.u0[10] = gUUVWabs.W;  */                                       //输出电压C相
    funcCode.code.u0[11] = (Uint32)gRealOut.U*(Uint32)gRealOut.I/10000;          //输出功率
    funcCode.code.u0[12] = (Uint32)gUUVW.dc*(Uint32)gIUVW.dc/10000;              //输入功率
    funcCode.code.u0[13] = funcCode.code.u0[10]*10000/funcCode.code.u0[11];      //效率
    funcCode.code.u0[14] = gTemperature.Temp;                                    //逆变温度
    funcCode.code.u0[15] = gpfc_display.Ir;                                      //R相电流
    funcCode.code.u0[16] = gpfc_display.Is;                                      //S相电流
    funcCode.code.u0[17] = gpfc_display.It;                                      //T相电流
    funcCode.code.u0[18] = gpfc_display.Urs;                                     //RS线电压
    funcCode.code.u0[19] = gpfc_display.Ust;                                     //ST线电压
    funcCode.code.u0[20] = gpfc_display.Utr;                                     //Tr线电压
    funcCode.code.u0[21] = gpfc_display.U;                                       //输入三相线电压平均
    funcCode.code.u0[22] = gpfc_display.I;                                       //输入三相电流平均
    funcCode.code.u0[23] = gpfc_display.In_freq;                                 //输入频率
    funcCode.code.u0[24] = gpfc_display.Temp;                                    //pfc温度

}

//modbus 与应用程序数据交互
void modbus_data_enterchange(uint16 chage_mode)
{
    F0_data_display();
    if( 0==chage_mode )    //频率源选择
    {  // 功能码 0x03/0x06                    寄存器地址31~63预留                                                显示码                    含义
        funcCode.code.presetFrq          = usRegHoldingBuf[0];             // F0-08     预置频率                           =X*100
        funcCode.code.maxFrq             = usRegHoldingBuf[1];             // F0-10     最大频率                            =X*100
        funcCode.code.carrierFrq         = usRegHoldingBuf[2];             // F0-15     载波频率                            =X/1000
        funcCode.code.varFcByTem         = usRegHoldingBuf[3];             // F0-16     载波频率随温度调整          预留
        funcCode.code.accTime1           = usRegHoldingBuf[4];             // F0-17     加速频率                             =X*10
        funcCode.code.decTime1           = usRegHoldingBuf[5];             // F0-18     减速频率                             =X*10
        funcCode.code.vfCurve            = usRegHoldingBuf[6];             // F3-00     VF曲线设定                         0:单点VF曲线，1:3点VF曲线
        funcCode.code.torqueBoost        = usRegHoldingBuf[7];             // F3-01     转矩提升                                预留
        funcCode.code.vfFrq1             = usRegHoldingBuf[8];             // F3-03     多点VF频率点1        =X*100
        funcCode.code.vfVol1             = usRegHoldingBuf[9];             // F3-04     多点VF电压点1        =X*100
        funcCode.code.vfFrq2             = usRegHoldingBuf[10];            // F3-05     多点VF频率点2        =X*100
        funcCode.code.vfVol2             = usRegHoldingBuf[11];            // F3-06     多点VF电压点2        =X*100
        funcCode.code.vfFrq3             = usRegHoldingBuf[12];            // F3-07     多点VF频率点3        =X*100
        funcCode.code.vfVol3             = usRegHoldingBuf[13];            // F3-08     多点VF电压点3        =X*100
        funcCode.code.slipCompCoef       = usRegHoldingBuf[14];            // F3-09     转差补偿系数                          预留
        funcCode.code.vfOverMagGain      = usRegHoldingBuf[15];            // F3-10     VF过励磁增益                        预留
        funcCode.code.antiVibrateGain    = usRegHoldingBuf[16];            // F3-11     振荡抑制增益                          预留
        funcCode.code.vfVoltageSrc       = usRegHoldingBuf[17];            // F3-13     VF分离的电压源                     预留
        funcCode.code.vfVoltageDigtalSet = usRegHoldingBuf[18];            // F3-14     VF分离的电压源数字设定
        funcCode.code.vfVoltageAccTime   = usRegHoldingBuf[19];            // F3-15     VF分离的电压上升
        funcCode.code.softOCValue        = usRegHoldingBuf[20];            // F8-36     软件过流点                            =X*100
        funcCode.code.softOCDelay        = usRegHoldingBuf[21];            // F8-37     软件过流检测延迟时间       单位:ms
        funcCode.code.temperatureArrive  = usRegHoldingBuf[22];            // F8-47     模块过温点设定
        funcCode.code.overload           = usRegHoldingBuf[23];            // F9-00     电机过载保护                        =X*1000 (额定50kW/s, 500表示25kW)
        funcCode.code.foreOverloadCoef   = usRegHoldingBuf[24];            // F9-01     电机过载保护增益                =X*1000 (额定50kW,     500表示25kW)
        funcCode.code.ovGain             = usRegHoldingBuf[25];            // F9-03     过压失速增益                        =X*1000 (额定300V/s, 500表示150V)
        funcCode.code.ovPoint            = usRegHoldingBuf[26];            // F9-04     过压失速保护电压                =X*1000 (额定300V,     500表示150V)
        funcCode.code.ocGain             = usRegHoldingBuf[27];            // F9-05     过流失速增益                        =X*1000 (额定50A/s,    500表示25A/s)
        funcCode.code.ocPoint            = usRegHoldingBuf[28];            // F9-05     过流保护点                            =X*1000 (额定50A,    500表示25A/s)
        funcCode.code.shortCheckMode     = usRegHoldingBuf[29];            // F9-07     上电对地短路保护功能         预留
        funcCode.code.errAutoRstNumMax   = usRegHoldingBuf[30];            // F9-09     故障自动复位次数                   0（故障自动复位不锁死） N(故障N次后锁死
        power_select                     = usRegHoldingBuf[31];            // H05/H06   选择0-H05  1-H06
        FAN_control                      = usRegHoldingBuf[32];            // H05/H06   选择0-H05  1-H06
        funcCode.code.updata             = usRegHoldingBuf[255];           // 更新程序

    }
    else
    {
        // 寄存器地址             功能码                                   显示码            含义
        usRegHoldingBuf[0]  = funcCode.code.presetFrq;              // F0-08     预置频率                          =X*100
        usRegHoldingBuf[1]  = funcCode.code.maxFrq;                 // F0-10     最大频率                          =X*100
        usRegHoldingBuf[2]  = funcCode.code.carrierFrq;             // F0-15     载波频率                          =X/1000
        usRegHoldingBuf[3]  = funcCode.code.varFcByTem;             // F0-16     载波频率随温度调整         预留
        usRegHoldingBuf[4]  = funcCode.code.accTime1;               // F0-17     加速频率                          =X*10
        usRegHoldingBuf[5]  = funcCode.code.decTime1;               // F0-18     减速频率                          =X*10
        usRegHoldingBuf[6]  = funcCode.code.vfCurve;                // F3-00     VF曲线设定                      0:单点VF曲线，1:3点VF曲线
        usRegHoldingBuf[7]  = funcCode.code.torqueBoost;            // F3-01     转矩提升                          预留
        usRegHoldingBuf[8]  = funcCode.code.vfFrq1;                 // F3-03     多点VF频率点1      =X*100
        usRegHoldingBuf[9]  = funcCode.code.vfVol1;                 // F3-04     多点VF电压点1      =X*100
        usRegHoldingBuf[10] = funcCode.code.vfFrq2;                 // F3-05     多点VF频率点2      =X*100
        usRegHoldingBuf[11] = funcCode.code.vfVol2;                 // F3-06     多点VF电压点2      =X*100
        usRegHoldingBuf[12] = funcCode.code.vfFrq3;                 // F3-07     多点VF频率点3      =X*100
        usRegHoldingBuf[13] = funcCode.code.vfVol3;                 // F3-08     多点VF电压点3      =X*100
        usRegHoldingBuf[14] = funcCode.code.slipCompCoef;           // F3-09     转差补偿系数                      预留
        usRegHoldingBuf[15] = funcCode.code.vfOverMagGain;          // F3-10     VF过励磁增益                     预留
        usRegHoldingBuf[16] = funcCode.code.antiVibrateGain;        // F3-11     振荡抑制增益                       预留
        usRegHoldingBuf[17] = funcCode.code.vfVoltageSrc;           // F3-13     VF分离的电压源                  预留
        usRegHoldingBuf[18] = funcCode.code.vfVoltageDigtalSet;     // F3-14     VF分离的电压源数字设定    预留
        usRegHoldingBuf[19] = funcCode.code.vfVoltageAccTime;       // F3-15     VF分离的电压上升时间
        usRegHoldingBuf[20] = funcCode.code.softOCValue;            // F8-36     软件过流点                         =X*100
        usRegHoldingBuf[21] = funcCode.code.softOCDelay;            // F8-37     软件过流检测延迟时间     单位:ms
        usRegHoldingBuf[22] = funcCode.code.temperatureArrive;      // F8-47     模块过温点设定
        usRegHoldingBuf[23] = funcCode.code.overload;               // F9-00     电机过载保护                        =X*1000 (额定50kW/s, 500表示25kW)
        usRegHoldingBuf[24] = funcCode.code.foreOverloadCoef;       // F9-01     电机过载保护增益                =X*1000 (额定50kW,     500表示25kW)
        usRegHoldingBuf[25] = funcCode.code.ovGain;                 // F9-03     过压失速增益                        =X*1000 (额定300V/s, 500表示150V)
        usRegHoldingBuf[26] = funcCode.code.ovPoint;                // F9-04     过压失速保护电压                 =X*1000 (额定300V,     500表示150V)
        usRegHoldingBuf[27] = funcCode.code.ocGain;                 // F9-05     过流失速增益                        =X*1000 (额定50A/s,    500表示25A/s)
        usRegHoldingBuf[28] = funcCode.code.ocPoint;                // F9-05     过流保护点                            =X*1000 (额定50A,    500表示25A/s)
        usRegHoldingBuf[29] = funcCode.code.shortCheckMode;         // F9-07     上电对地短路保护功能          预留
        usRegHoldingBuf[30] = funcCode.code.errAutoRstNumMax;       // F9-09     故障自动复位次数                  0（故障自动复位不锁死） N(故障N次后锁死)
        usRegHoldingBuf[31] = power_select;                         // H05/H06   选择0-H05  1-H06
        usRegHoldingBuf[32] = FAN_control;                          // 0不开风扇，1开风扇
        usRegHoldingBuf[255] = funcCode.code.updata;                // 更新程序

    }

        //以下寄存器只支持0x03          0x06无效
        usRegHoldingBuf[64] = funcCode.code.runTimeAddup;           // F7-09     累计运行时间       高位 单位: h
        usRegHoldingBuf[65] = funcCode.code.runTimeAddup;           // F7-09     累计运行时间 低位 单位: h
        usRegHoldingBuf[66] = funcCode.code.productVersion;         // F7-10     产品号                                0x0805:H05  0x0806:H06
        usRegHoldingBuf[67] = funcCode.code.softVersion;            // F7-11     软件版本号                         0x0001:版本V01
        usRegHoldingBuf[68] = funcCode.code.powerUpTimeAddup;       // F7-13     累计上电时间 单位: 0.1h
        usRegHoldingBuf[69] = funcCode.code.powerAddup;             // F7-14     累计耗电量   kW   高位
        usRegHoldingBuf[70] = funcCode.code.powerAddup;             // F7-14     累计耗电量   kW   低位

        usRegHoldingBuf[71] = funcCode.code.errorLatest1;           // F9-14     第一次故障类型
        usRegHoldingBuf[72] = funcCode.code.errorLatest2;           // F9-15     第二次故障类型
        usRegHoldingBuf[73] = funcCode.code.errorLatest3;           // F9-16     第三次(最近一次)故障类型
        usRegHoldingBuf[74] = funcCode.code.errorScene3.all[0];     // F9-17     第三次(最近一次)故障时频率                            =X*100
        usRegHoldingBuf[75] = funcCode.code.errorScene3.all[1];     // F9-18     第三次(最近一次)故障时母线电流                     =X*100
        usRegHoldingBuf[76] = funcCode.code.errorScene3.all[2];     // F9-19     第三次(最近一次)故障时母线电压                     =X*100
        usRegHoldingBuf[77] = funcCode.code.errorScene3.all[3];     // F9-20     第三次(最近一次)故障时计算输出电流              =X*100
        usRegHoldingBuf[78] = funcCode.code.errorScene3.all[4];     // F9-21     第三次(最近一次)故障时计算输出电压              =X*100
        usRegHoldingBuf[79] = funcCode.code.errorScene3.all[5];     // F9-22     第三次(最近一次)故障时A相电流                      =X*100
        usRegHoldingBuf[80] = funcCode.code.errorScene3.all[6];     // F9-23     第三次(最近一次)故障时B相电流                      =X*100
        usRegHoldingBuf[81] = funcCode.code.errorScene3.all[7];     // F9-24     第三次(最近一次)故障时C相电流                      =X*100
        usRegHoldingBuf[82] = 0;                                    // 故障预留
        usRegHoldingBuf[83] = 0;
        usRegHoldingBuf[84] = 0;
        usRegHoldingBuf[85] = 0;

        usRegHoldingBuf[87] = funcCode.code.errorScene2.all[0];      //同上        第二次
        usRegHoldingBuf[88] = funcCode.code.errorScene2.all[1];
        usRegHoldingBuf[89] = funcCode.code.errorScene2.all[2];
        usRegHoldingBuf[90] = funcCode.code.errorScene2.all[3];
        usRegHoldingBuf[100]= funcCode.code.errorScene2.all[4];
        usRegHoldingBuf[101]= funcCode.code.errorScene2.all[5];
        usRegHoldingBuf[102]= funcCode.code.errorScene2.all[6];
        usRegHoldingBuf[103]= funcCode.code.errorScene2.all[7];
        usRegHoldingBuf[104]= 0;
        usRegHoldingBuf[105]= 0;
        usRegHoldingBuf[106]= 0;
        usRegHoldingBuf[107]= 0;
        usRegHoldingBuf[108]= funcCode.code.errorScene2.all[0];
        usRegHoldingBuf[109]= funcCode.code.errorScene2.all[0];
        usRegHoldingBuf[110]= funcCode.code.errorScene2.all[0];
        usRegHoldingBuf[111]= funcCode.code.errorScene2.all[0];
        usRegHoldingBuf[112]= funcCode.code.errorScene2.all[0];
        usRegHoldingBuf[113]= funcCode.code.errorScene2.all[0];
        usRegHoldingBuf[114]= funcCode.code.errorScene2.all[0];
        usRegHoldingBuf[115]= funcCode.code.errorScene2.all[0];
        usRegHoldingBuf[104]= 0;
        usRegHoldingBuf[105]= 0;
        usRegHoldingBuf[106]= 0;
        usRegHoldingBuf[107]= 0;
                                                                      //以下参数  =X*100
        usRegHoldingBuf[108]= funcCode.code.u0[0];                    //输出频率
        usRegHoldingBuf[109]= funcCode.code.u0[1];                    //母线电压
        usRegHoldingBuf[110]= funcCode.code.u0[2];                    //母线电流
        usRegHoldingBuf[111]= funcCode.code.u0[3];                    //输出电压
        usRegHoldingBuf[112]= funcCode.code.u0[4];                    //输出电流
        usRegHoldingBuf[113]= funcCode.code.u0[5];                    //输出电流A相
        usRegHoldingBuf[114]= funcCode.code.u0[6];                    //输出电流B相
        usRegHoldingBuf[115]= funcCode.code.u0[7];                    //输出电流C相
        usRegHoldingBuf[116]= funcCode.code.u0[8];                    //输出电压A相
        usRegHoldingBuf[117]= funcCode.code.u0[9];                    //输出电压B相
        usRegHoldingBuf[118]= funcCode.code.u0[10];                   //输出电压C相
        usRegHoldingBuf[119]= funcCode.code.u0[11];                   //输出功率
        usRegHoldingBuf[120]= funcCode.code.u0[12];                   //输入功率
        usRegHoldingBuf[121]= funcCode.code.u0[13];                   //效率
        usRegHoldingBuf[122]= funcCode.code.u0[14];                   //逆变温度
        usRegHoldingBuf[123]= read_error_flag();                      //故障信号
        usRegHoldingBuf[124]= funcCode.code.u0[15];                   //R相电流
        usRegHoldingBuf[125]= funcCode.code.u0[16];                   //S相电流
        usRegHoldingBuf[126]= funcCode.code.u0[17];                   //T相电流
        usRegHoldingBuf[127]= funcCode.code.u0[18];                   //RS线电压
        usRegHoldingBuf[128]= funcCode.code.u0[19];                   //ST线电压
        usRegHoldingBuf[129]= funcCode.code.u0[20];                   //Tr线电压
        usRegHoldingBuf[130]= funcCode.code.u0[21];                   //输入三相线电压平均
        usRegHoldingBuf[131]= funcCode.code.u0[22];                   //输入三相电流平均
        usRegHoldingBuf[132]= funcCode.code.u0[23];                   //输入频率
        usRegHoldingBuf[133]= funcCode.code.u0[24];                   //pfc温度


        usRegHoldingBuf[252] = SOFTWARE_REV1;
        usRegHoldingBuf[253] = SOFTWARE_NUMBER;
        usRegHoldingBuf[254] = (((Uint16)get_motorexcur_flag())<<1 ) + (Uint16)get_pfcexcur_flag();

        Change_vf_data();
        Set_protectType(power_select);

}



// 出厂数据初始化
void data_init()
{
    uint16 i=0;
    for( i=0; i<FNUM_PARA; i++ )
        funcCode.all[i] = funcCodeAttribute[i].init;

    funcCode.code.vfFrq1 = gVFPar.VFLineFreq1 ;
    funcCode.code.vfVol1 = gVFPar.VFLineVolt1;
    funcCode.code.vfFrq2 = gVFPar.VFLineFreq2;
    funcCode.code.vfVol2 = gVFPar.VFLineVolt2;
    funcCode.code.vfFrq3 = gVFPar.VFLineFreq3;
    funcCode.code.vfVol3 = gVFPar.VFLineVolt3;

    funcCode.code.accTime1 = gVFPar.Up_freq;
    funcCode.code.decTime1 = gVFPar.Down_freq;

    funcCode.code.ledDispParaRun1  = 0x07FF;
    funcCode.code.ledDispParaStop  = 0x0003;

    modbus_data_enterchange(1);
}
