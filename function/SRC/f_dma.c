/*
 * f_dma.c
 *
 *  Created on: 2021年7月8日
 *      Author: dfs_h
 */
#include"f_dma.h"
//
// Defines
//
#define BURST       9       // write 7 to the register for a burst size of 8
#define TRANSFER    100      // [(MEM_BUFFER_SIZE/(BURST + 1)) - 1]

extern volatile Uint16 g_adIa[20];   //PFCA相电流
extern volatile Uint16 g_adIb[20];   //PFCB相电流
extern volatile Uint16 g_adIc[20];   //PFCC相电流

// Globals
//
// dma_init - DMA setup for both TX and RX channels.
//
void Dma_Init(void)
{
    //Initialize DMA
    DMAInitialize();

    // configure DMA CH1
    DMACH1AddrConfig(g_adIc,&AdcaResultRegs.ADCRESULT3);

    //burst传输：burst传输是由每一个ADC中断标志触发，ADC每次转化完成，该传输模式启动。
    // void DMACH1BurstConfig(Uint16 bsize, int16 srcbstep, int16 desbstep)
    // bsize    ：一帧传输的字数
    // srcbstep ：源地址步长。每次传输完一个字后增加一个步长。
    // desbstep ：目的地址步长。每次传输完一个字后增加一个步长。
    DMACH1BurstConfig(BURST,0,1);
    //在上一次burst传输完成后，源和目的地址的基础上进行偏移。
    //tsize    ：每tsize+1帧传输后中断一次
    // srctstep ：每次中断后，源地址偏移，可以为负数，负增长。
    //deststep ：每次中断后，目的地偏移，可以为负数，负增长。
    DMACH1TransferConfig(TRANSFER,0,0);
    DMACH1WrapConfig(0,0,0,0);          //wrap传输

    DMACH1ModeConfig(DMA_ADCAINT1,PERINT_ENABLE,ONESHOT_DISABLE,CONT_ENABLE,
                     SYNC_DISABLE,SYNC_SRC,OVRFLOW_DISABLE,SIXTEEN_BIT,
                     CHINT_END,CHINT_ENABLE);



    DMACH2AddrConfig(g_adIb,&AdcbResultRegs.ADCRESULT3);
    DMACH2BurstConfig(BURST,0,1);
    DMACH2TransferConfig(TRANSFER,0,0);
    DMACH2WrapConfig(0,0,0,0);          //wrap传输

    DMACH2ModeConfig(DMA_ADCAINT1,PERINT_ENABLE,ONESHOT_DISABLE,CONT_ENABLE,
                     SYNC_DISABLE,SYNC_SRC,OVRFLOW_DISABLE,SIXTEEN_BIT,
                     CHINT_END,CHINT_ENABLE);


    DMACH3AddrConfig(g_adIa,&AdcdResultRegs.ADCRESULT3);
    DMACH3BurstConfig(BURST,0,1);
    DMACH3TransferConfig(TRANSFER,0,0);
    DMACH3WrapConfig(0,0,0,0);          //wrap传输

    DMACH3ModeConfig(DMA_ADCAINT1,PERINT_ENABLE,ONESHOT_DISABLE,CONT_ENABLE,
                     SYNC_DISABLE,SYNC_SRC,OVRFLOW_DISABLE,SIXTEEN_BIT,
                     CHINT_END,CHINT_ENABLE);
    StartDMACH1();
    StartDMACH2();
    StartDMACH3();

}



