/*******************************************************************************************

                  Copyright (c) 2016 Shenzhen Greatland Electrics Co.,Ltd

File Name:      ADCIf.c

Version:         V1.0

Date:

Author:

Input:

Output:

Description:    获取AD原值

History:

*******************************************************************************************/
#include"GetADCresult.h"

volatile Uint16 g_adIa[20];   //PFCA相电流
volatile Uint16 g_adIb[20];   //PFCB相电流
volatile Uint16 g_adIc[20];   //PFCC相电流


/*******************************************************************************************
Function Name:  CmdVoltADCSample
Version:         V1.0
Input:
Output:
Return:
Description:   电机电压电流命令AD值
History:
*******************************************************************************************/
void  CmdVoltADCSample(StructADCResult *pADCResult)
{

    //  UVW voltage
    //pADCResult->UuAD= 0;////GetADCAResult(1);
    //pADCResult->UvAD= 0;//GetADCBResult(1);
   // pADCResult->UwAD= 0;//GetADCDResult(1);

    //UVW current
    pADCResult->IuAD = GetADCAResult(2);
    pADCResult->IvAD = GetADCBResult(2);
    pADCResult->IwAD = GetADCDResult(2);

    //DC voltage and current
   // pADCResult->VdcAD = 0;//GetADCDResult(0);
   // pADCResult->IdcAD = 0;//GetADCAResult(0);

    //temp
    pADCResult->TempAD = GetADCAResult(0);
}


void pfc_ADCSample(PFC_ADCRESULT_REF *pADCResult)
{
  //  int TEMP_IA,TEMP_IB,TEMP_IC;
  //  unsigned int i=0;

    pADCResult ->Temp = (long)GetADCAResult(4);
    pADCResult ->UBA  = (long)GetADCAResult(1);
    pADCResult ->UBC  = (long)GetADCBResult(1);
   // GetADCDResult(3);filter by dma

    //TEMP_IA = (int)g_adIa[0]+ (int)g_adIa[1]+ (int)g_adIa[2]+ (int)g_adIa[3]+ (int)g_adIa[4];
    //TEMP_IA += (int)g_adIa[5]+ (int)g_adIa[6]+ (int)g_adIa[7]+ (int)g_adIa[8]+ (int)g_adIa[9];
   // pADCResult ->IA = GetADCDResult(3);//TEMP_IA/10;

    pADCResult ->IA   =((long)g_adIa[0]+ (long)g_adIa[1]+ (long)g_adIa[2]+ (long)g_adIa[3]+ (long)g_adIa[4]
	              + (long)g_adIa[5]+ (long)g_adIa[6]+ (long)g_adIa[7]+ (long)g_adIa[8]+ (long)g_adIa[9])/10;
   //GetADCBResult(3);filter by dma
    pADCResult ->IB   =((long)g_adIb[0]+ (long)g_adIb[1]+ (long)g_adIb[2]+ (long)g_adIb[3]+ (long)g_adIb[4]
		      + (long)g_adIb[5]+ (long)g_adIb[6]+ (long)g_adIb[7]+ (long)g_adIb[8]+ (long)g_adIb[9])/10;
   // GetADCAResult(3); filter by dma
    pADCResult ->IC   = ((long)g_adIc[0]+ (long)g_adIc[1]+ (long)g_adIc[2]+ (long)g_adIc[3]+ (long)g_adIc[4]
		      + (long)g_adIc[5]+ (long)g_adIc[6]+ (long)g_adIc[7]+ (long)g_adIc[8]+ (long)g_adIc[9])/10;
    pADCResult ->Vdc  = (long)GetADCDResult(0);
}

