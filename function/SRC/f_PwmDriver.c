#include"PwmDriver.h"


//void InitEPwm1Driver(void);
//void InitEPwm2Driver(void);
//void InitEPwm3Driver(void);
//void InitEPWM(void);
//
//
//void PwmOn(void);
//void PwmOff(void);
//void PhaseTypeWVUPWMCmp(uint16 UCmp, uint16 VCmp, uint16 WCmp);
//void PhaseTypeUVWPWMCmp(uint16 UCmp, uint16 VCmp, uint16 WCmp);
extern void Set_SpeedZero(void);

void (*PFC_Control_Ptr)(void);                   //PFC_CONTROL_BRANCH
void (*Motor_Control_Ptr)(void);                 //MOTOR_CONTROL_BRANCH

//#define PWM_ACTIVE_LEVEL  DB_ACTV_LOC         /* old */
#define PWM_ACTIVE_LEVEL    DB_ACTV_HIC         /* new */
#define PWM_PRD             8333                //6k载波
#define PWM_PRD_PFC         7240                //13.812k载波

void  Pwm_Empty_Project(void)
{
    ;
}

/*
void  Set_SpeedZero(void)
{
    ;
}
*/

__interrupt void  MtrCtrlISR(void)
{
    volatile uint16 TempPIEIER1 = PieCtrlRegs.PIEIER1.all;
    volatile uint16 TempPIEIER12 = PieCtrlRegs.PIEIER12.all;

    IER = M_INT1|M_INT12;
    PieCtrlRegs.PIEIER1.all &= 0x0018;                  // Set "group"  priority
    PieCtrlRegs.PIEIER12.all &= 0x0007;                  // Set "group"  priority
    PieCtrlRegs.PIEACK.all = 0xFFFF;                    // Enable PIE interrupts
    EINT;

   (*Motor_Control_Ptr)();

   EPwm4Regs.ETCLR.bit.INT = 1;
   PieCtrlRegs.PIEACK.all = PIEACK_GROUP3;

   DINT;
   PieCtrlRegs.PIEIER1.all = TempPIEIER1;
   PieCtrlRegs.PIEIER12.all = TempPIEIER12;
}


__interrupt void  PfcCtrlISR(void)
{
    volatile uint16 TempPIEIER1 = PieCtrlRegs.PIEIER1.all;
    volatile uint16 TempPIEIER12 = PieCtrlRegs.PIEIER12.all;

    IER = M_INT1|M_INT12;
    PieCtrlRegs.PIEIER1.all &= 0x0018;                  // Set "group"  priority
    PieCtrlRegs.PIEIER12.all &= 0x0007;                 // Set "group"  priority
    PieCtrlRegs.PIEACK.all = 0xFFFF;                    // Enable PIE interrupts
    EINT;

    (*PFC_Control_Ptr)();
    EPwm1Regs.ETCLR.bit.INT = 1;
    PieCtrlRegs.PIEACK.all = PIEACK_GROUP3;

    DINT;
    PieCtrlRegs.PIEIER1.all = TempPIEIER1;
    PieCtrlRegs.PIEIER12.all = TempPIEIER12;
}

void InitEPwm1Driver(void)
{


  // Enable TZ1 as CBC trip sources
  /*   EALLOW;
     EPwmXbarRegs.TRIP4MUX0TO15CFG.bit.MUX0=0;   //ADCAEVENT
     EPwmXbarRegs.TRIP4MUX0TO15CFG.bit.MUX1=0;   //ADCBEVENT
     EPwmXbarRegs.TRIP4MUX0TO15CFG.bit.MUX9=3;   //ADCDEVENT

     EPwmXbarRegs.TRIP4MUXENABLE.bit.MUX0=1;     //ENABLE
     EPwmXbarRegs.TRIP4MUXENABLE.bit.MUX1=1;
     EPwmXbarRegs.TRIP4MUXENABLE.bit.MUX9=1;

     EPwmXbarRegs.TRIPOUTINV.bit.TRIP4=0;        //high output

     //TRIPIN1-TRIPIN3    TZ1-TZ3
     EPwm1Regs.DCTRIPSEL.bit.DCAHCOMPSEL=3;       //TRIP4 - DCAH
     EPwm1Regs.DCACTL.bit.EVT2FRCSYNCSEL=0;       //1:synchronized with EPWMCLK 0 Source is passed through asynchronously
     EPwm1Regs.DCACTL.bit.EVT2SRCSEL=0;           //0: Source Is DCAEVT2 Signa  1: Source Is DCEVTFILT Signa

     // 000 disable  001 DCAH=LOW   010 DCAH=HIGH   011 DCAL=LOW 100 DCAL=HIGH  101   AL=HIGH AH=LOW
     EPwm1Regs.TZDCSEL.bit.DCAEVT2 =2;
     //DCAEVT2   CBC   DCAEVT1 one-shot    SET CBC
     EPwm1Regs.TZSEL.bit.DCAEVT2 = 1;

     // Set TZA
     EPwm1Regs.TZCTL.bit.TZA = TZ_FORCE_LO;
     EPwm1Regs.TZCTL.bit.TZB = TZ_FORCE_LO;
     // Enable TZ interrupt
   //  EPwm1Regs.TZEINT.bit.OST = 1;
     EDIS;*/

    EPwm1Regs.TBPRD = PWM_PRD_PFC;                  // Set timer period
    EPwm1Regs.TBPHS.bit.TBPHS = 0x0000;             // Phase is 0
    EPwm1Regs.TBCTR = 0x0000;                       // Clear counter

    // Setup TBCLK
    EPwm1Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN; // Count up
    EPwm1Regs.TBCTL.bit.PHSEN = TB_DISABLE;        // Disable phase loading
    EPwm1Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1;       // Clock ratio to SYSCLKOUT
    EPwm1Regs.TBCTL.bit.CLKDIV = TB_DIV1;

    EPwm1Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;    // Load registers every ZERO
    EPwm1Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW;
    EPwm1Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO_PRD;
    EPwm1Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO_PRD;

    // Setup compare
    EPwm1Regs.CMPA.bit.CMPA = 0;
    EPwm1Regs.CMPB.bit.CMPB = PWM_PRD_PFC/2;
    EPwm1Regs.CMPD = PWM_PRD_PFC/2;

    // Set actions
    EPwm1Regs.AQCTLA.bit.CAU = AQ_SET;             // Set PWM1A on CAU
    EPwm1Regs.AQCTLA.bit.CAD = AQ_CLEAR;           // Clear PWM1A on CAD
    EPwm1Regs.AQCTLB.bit.CAU = AQ_CLEAR;
    EPwm1Regs.AQCTLB.bit.CAD = AQ_SET;

    // Active Low PWMs - Setup Deadband
    EPwm1Regs.DBCTL.bit.OUT_MODE = DB_FULL_ENABLE;
    EPwm1Regs.DBCTL.bit.POLSEL = PWM_ACTIVE_LEVEL;
    EPwm1Regs.DBCTL.bit.IN_MODE = DBA_ALL;
    EPwm1Regs.DBRED.bit.DBRED = 200;
    EPwm1Regs.DBFED.bit.DBFED = 200;

    //Set the Event-trigger (ET) Module
    EPwm1Regs.ETCLR.bit.INT = 1;
    EPwm1Regs.ETSEL.all = 0x0e0b;
    EPwm1Regs.ETPS.all = 0x0101;

}



void InitEPwm2Driver(void)
{

    EPwm2Regs.TBPRD = PWM_PRD_PFC;                  // Set timer period
    EPwm2Regs.TBPHS.bit.TBPHS = 0x0000;             // Phase is 0
    EPwm2Regs.TBCTR = 0x0000;                       // Clear counter

    // Setup TBCLK
    EPwm2Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN; // Count up
    EPwm2Regs.TBCTL.bit.PHSEN = TB_DISABLE;        // Disable phase loading
    EPwm2Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1;       // Clock ratio to SYSCLKOUT
    EPwm2Regs.TBCTL.bit.CLKDIV = TB_DIV1;

    EPwm2Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;    // Load registers every ZERO
    EPwm2Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW;
    EPwm2Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO_PRD;
    EPwm2Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO_PRD;

    // Setup compare
    EPwm2Regs.CMPA.bit.CMPA = 0;
    EPwm2Regs.CMPB.bit.CMPB = PWM_PRD_PFC/2;

    // Set actions
    EPwm2Regs.AQCTLA.bit.CAU = AQ_SET;             // Set PWM1A on CAU
    EPwm2Regs.AQCTLA.bit.CAD = AQ_CLEAR;           // Clear PWM1A on CAD
    EPwm2Regs.AQCTLB.bit.CAU = AQ_CLEAR;
    EPwm2Regs.AQCTLB.bit.CAD = AQ_SET;

    // Active Low PWMs - Setup Deadband
    EPwm2Regs.DBCTL.bit.OUT_MODE = DB_FULL_ENABLE;
    EPwm2Regs.DBCTL.bit.POLSEL = PWM_ACTIVE_LEVEL;
    EPwm2Regs.DBCTL.bit.IN_MODE = DBA_ALL;
    EPwm2Regs.DBRED.bit.DBRED = 200;
    EPwm2Regs.DBFED.bit.DBFED = 200;
}




void InitEPwm3Driver(void)
{
    // Enable TZ1 as one shot trip sources
    EPwm3Regs.TBPRD = PWM_PRD_PFC;                      // Set timer period
    EPwm3Regs.TBPHS.bit.TBPHS = 0x0000;             // Phase is 0
    EPwm3Regs.TBCTR = 0x0000;                       // Clear counter

    // Setup TBCLK
    EPwm3Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN; // Count up
    EPwm3Regs.TBCTL.bit.PHSEN = TB_DISABLE;        // Disable phase loading
    EPwm3Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1;       // Clock ratio to SYSCLKOUT
    EPwm3Regs.TBCTL.bit.CLKDIV = TB_DIV1;

    EPwm3Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;    // Load registers every ZERO
    EPwm3Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW;
    EPwm3Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO_PRD;
    EPwm3Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO_PRD;

    // Setup compare
    EPwm3Regs.CMPA.bit.CMPA = 0;
    EPwm3Regs.CMPB.bit.CMPB = PWM_PRD_PFC/2;
    // Set actions
    EPwm3Regs.AQCTLA.bit.CAU = AQ_SET;             // Set PWM1A on CAU
    EPwm3Regs.AQCTLA.bit.CAD = AQ_CLEAR;           // Clear PWM1A on CAD
    EPwm3Regs.AQCTLB.bit.CAU = AQ_CLEAR;
    EPwm3Regs.AQCTLB.bit.CAD = AQ_SET;

    // Active Low PWMs - Setup Deadband
    EPwm3Regs.DBCTL.bit.OUT_MODE = DB_FULL_ENABLE;
    EPwm3Regs.DBCTL.bit.POLSEL = PWM_ACTIVE_LEVEL;
    EPwm3Regs.DBCTL.bit.IN_MODE = DBA_ALL;
    EPwm3Regs.DBRED.bit.DBRED = 200;
    EPwm3Regs.DBFED.bit.DBFED = 200;  //TBCLK=200MHz 死区时间1μS
}




void InitEPwm4Driver(void)
{
    // Enable TZ1 as one shot trip sources
    EPwm4Regs.TBPRD = PWM_PRD;                      // Set timer period
    EPwm4Regs.TBPHS.bit.TBPHS = 0x0000;             // Phase is 0
    EPwm4Regs.TBCTR = 0x0000;                       // Clear counter

    // Setup TBCLK
    EPwm4Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN; // Count up
    EPwm4Regs.TBCTL.bit.PHSEN = TB_DISABLE;        // Disable phase loading
    EPwm4Regs.TBCTL.bit.HSPCLKDIV = TB_DIV2;       // Clock ratio to SYSCLKOUT
    EPwm4Regs.TBCTL.bit.CLKDIV = TB_DIV1;

    EPwm4Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;    // Load registers every ZERO
    EPwm4Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW;
    EPwm4Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO_PRD;
    EPwm4Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO_PRD;

    // Setup compare
    EPwm4Regs.CMPA.bit.CMPA = 0;

    // Set actions
    EPwm4Regs.AQCTLA.bit.CAU = AQ_SET;             // Set PWM1A on CAU
    EPwm4Regs.AQCTLA.bit.CAD = AQ_CLEAR;           // Clear PWM1A on CAD
    EPwm4Regs.AQCTLB.bit.CAU = AQ_CLEAR;
    EPwm4Regs.AQCTLB.bit.CAD = AQ_SET;

    // Active Low PWMs - Setup Deadband
    EPwm4Regs.DBCTL.bit.OUT_MODE = DB_FULL_ENABLE;
    EPwm4Regs.DBCTL.bit.POLSEL = PWM_ACTIVE_LEVEL;
    EPwm4Regs.DBCTL.bit.IN_MODE = DBA_ALL;
    EPwm4Regs.DBRED.bit.DBRED = 108;
    EPwm4Regs.DBFED.bit.DBFED = 108;

    //Set the Event-trigger (ET) Module
    EPwm4Regs.ETCLR.bit.INT = 1;
    EPwm4Regs.ETSEL.all = 0x0e0b;
    EPwm4Regs.ETPS.all = 0x0101;
}




void InitEPwm5Driver(void)
{
    EPwm5Regs.TBPRD = PWM_PRD;                      // Set timer period
    EPwm5Regs.TBPHS.bit.TBPHS = 0x0000;             // Phase is 0
    EPwm5Regs.TBCTR = 0x0000;                       // Clear counter

    // Setup TBCLK
    EPwm5Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN; // Count up
    EPwm5Regs.TBCTL.bit.PHSEN = TB_DISABLE;        // Disable phase loading
    EPwm5Regs.TBCTL.bit.HSPCLKDIV = TB_DIV2;       // Clock ratio to SYSCLKOUT
    EPwm5Regs.TBCTL.bit.CLKDIV = TB_DIV1;

    EPwm5Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;    // Load registers every ZERO
    EPwm5Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW;
    EPwm5Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO_PRD;
    EPwm5Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO_PRD;

    // Setup compare
    EPwm5Regs.CMPA.bit.CMPA = 0;

    // Set actions
    EPwm5Regs.AQCTLA.bit.CAU = AQ_SET;             // Set PWM1A on CAU
    EPwm5Regs.AQCTLA.bit.CAD = AQ_CLEAR;           // Clear PWM1A on CAD
    EPwm5Regs.AQCTLB.bit.CAU = AQ_CLEAR;
    EPwm5Regs.AQCTLB.bit.CAD = AQ_SET;

    // Active Low PWMs - Setup Deadband
    EPwm5Regs.DBCTL.bit.OUT_MODE = DB_FULL_ENABLE;
    EPwm5Regs.DBCTL.bit.POLSEL = PWM_ACTIVE_LEVEL;
    EPwm5Regs.DBCTL.bit.IN_MODE = DBA_ALL;
    EPwm5Regs.DBRED.bit.DBRED = 108;
    EPwm5Regs.DBFED.bit.DBFED = 108;
}



void InitEPwm6Driver(void)
{
    EPwm6Regs.TBPRD = PWM_PRD;                      // Set timer period
    EPwm6Regs.TBPHS.bit.TBPHS = 0x0000;             // Phase is 0
    EPwm6Regs.TBCTR = 0x0000;                       // Clear counter

    // Setup TBCLK
    EPwm6Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN; // Count up
    EPwm6Regs.TBCTL.bit.PHSEN = TB_DISABLE;        // Disable phase loading
    EPwm6Regs.TBCTL.bit.HSPCLKDIV = TB_DIV2;       // Clock ratio to SYSCLKOUT
    EPwm6Regs.TBCTL.bit.CLKDIV = TB_DIV1;

    EPwm6Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;    // Load registers every ZERO
    EPwm6Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW;
    EPwm6Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO_PRD;
    EPwm6Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO_PRD;

    // Setup compare
    EPwm6Regs.CMPA.bit.CMPA = 0;

    // Set actions
    EPwm6Regs.AQCTLA.bit.CAU = AQ_SET;             // Set PWM1A on CAU
    EPwm6Regs.AQCTLA.bit.CAD = AQ_CLEAR;           // Clear PWM1A on CAD
    EPwm6Regs.AQCTLB.bit.CAU = AQ_CLEAR;
    EPwm6Regs.AQCTLB.bit.CAD = AQ_SET;

    // Active Low PWMs - Setup Deadband
    EPwm6Regs.DBCTL.bit.OUT_MODE = DB_FULL_ENABLE;
    EPwm6Regs.DBCTL.bit.POLSEL = PWM_ACTIVE_LEVEL;
    EPwm6Regs.DBCTL.bit.IN_MODE = DBA_ALL;
    EPwm6Regs.DBRED.bit.DBRED = 108;
    EPwm6Regs.DBFED.bit.DBFED = 108;

    //Set the Event-trigger (ET) Module
    EPwm6Regs.ETCLR.bit.INT = 1;
    EPwm6Regs.ETSEL.all = 0x0e0b;
    EPwm6Regs.ETPS.all = 0x0101;
}


/*******************************************************************************************
Function Name:   InitPFC_Ptr
Version:         V1.0
Description:     初始化function函数
History:
*******************************************************************************************/
void Initmotor_Ptr(void (*function_Ptr)(void))
{
    Motor_Control_Ptr = function_Ptr;
}


/*******************************************************************************************
Function Name:   InitEPWM
Version:         V1.0
Description:     初始化电机驱动PWM函数
History:
*******************************************************************************************/
void InitADCEPWM(void)
{
   CpuSysRegs.PCLKCR2.bit.EPWM9=1;
   SyncSocRegs.SYNCSELECT.bit.EPWM7SYNCIN=0;

   EPwm9Regs.TBPRD = PWM_PRD_PFC/10;               // Set timer period
   EPwm9Regs.TBPHS.bit.TBPHS = 0x0000;             // Phase is 0
   EPwm9Regs.TBCTR = 0x0000;                       // Clear counter

   // Setup TBCLK
   EPwm9Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN; // Count up
   EPwm9Regs.TBCTL.bit.PHSEN = TB_ENABLE;        // Disable phase loading
   EPwm9Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1;       // Clock ratio to SYSCLKOUT
   EPwm9Regs.TBCTL.bit.CLKDIV = TB_DIV1;

   EPwm9Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;    // Load registers every ZERO
   EPwm9Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW;
   EPwm9Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO_PRD;
   EPwm9Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO_PRD;

   // Setup compare
   EPwm9Regs.CMPA.bit.CMPA = PWM_PRD_PFC/20;

   // Set actions
   EPwm9Regs.AQCTLA.bit.CAU = AQ_SET;             // Set PWM1A on CAU
   EPwm9Regs.AQCTLA.bit.CAD = AQ_CLEAR;           // Clear PWM1A on CAD
   EPwm9Regs.AQCTLB.bit.CAU = AQ_CLEAR;
   EPwm9Regs.AQCTLB.bit.CAD = AQ_SET;

   // Active Low PWMs - Setup Deadband
   EPwm9Regs.DBCTL.bit.OUT_MODE = DB_FULL_ENABLE;
   EPwm9Regs.DBCTL.bit.POLSEL = PWM_ACTIVE_LEVEL;
   EPwm9Regs.DBCTL.bit.IN_MODE = DBA_ALL;
   EPwm9Regs.DBRED.bit.DBRED = 10;
   EPwm9Regs.DBFED.bit.DBFED = 1;

   //Set the Event-trigger (ET) Module
   EPwm9Regs.ETCLR.bit.INT = 1;
   EPwm9Regs.ETSEL.all = 0x0B0B;    //111000001101   ENABLE ADC PULSE TRIGLE ZERO OR PERIOD
   EPwm9Regs.ETPS.all = 0x0101;     //FIRST EVENT

}


/*******************************************************************************************
Function Name:   InitPFCEPWM
Version:         V1.0
Description:    输入RST PFC底层驱动程序
History:
*******************************************************************************************/
void InitPFCEPWM(void)
{
    PFC_Control_Ptr = Pwm_Empty_Project;

    PwmPFCOff();
    // PWMCLK equal to PLLSYSCLK
    EALLOW;
    ClkCfgRegs.PERCLKDIVSEL.bit.EPWMCLKDIV = 0;
    EDIS;

    // enable PWM1, PWM2 and PWM3
    CpuSysRegs.PCLKCR2.bit.EPWM1=1;
    CpuSysRegs.PCLKCR2.bit.EPWM2=1;
    CpuSysRegs.PCLKCR2.bit.EPWM3=1;

    // For this case just init GPIO pins for ePWM1, ePWM2, ePWM3
    // These functions are in the F2837xS_EPwm.c file
    InitEPwm1Gpio();
    InitEPwm2Gpio();
    InitEPwm3Gpio();
 //   InitEPwm9Gpio();   //测试用

    EALLOW; // This is needed to write to EALLOW protected registers
    PieVectTable.EPWM1_INT = &PfcCtrlISR;
    EDIS;   // This is needed to disable write to EALLOW protected registers

    EALLOW;
    CpuSysRegs.PCLKCR0.bit.TBCLKSYNC =0;
    EDIS;

    InitEPwm1Driver();
    InitEPwm2Driver();
    InitEPwm3Driver();

    InitADCEPWM();
   //  InitCMPSS();

    EALLOW;
    CpuSysRegs.PCLKCR0.bit.TBCLKSYNC =1;
    EDIS;

    // Enable CPU INT3 which is connected to EPWM1-3 INT:
    IER |= M_INT3;

    // Enable EPWM INTn in the PIE: Group 3 interrupt 1-3
    PieCtrlRegs.PIEIER3.bit.INTx1 = 1;

    PwmPFCOff();
}


/*******************************************************************************************
Function Name:   InitPFC_Ptr
Version:         V1.0
Description:     初始化function函数
History:
*******************************************************************************************/
void InitPFC_Ptr(void (*function_Ptr)(void))
{
    PFC_Control_Ptr = function_Ptr;
}


/*******************************************************************************************
Function Name:   InitEPWM
Version:         V1.0
Description:     初始化电机驱动PWM函数
History:
*******************************************************************************************/
void InitEPWM(void)
{

    Motor_Control_Ptr = Pwm_Empty_Project;

    PwmOff();
    // PWMCLK equal to PLLSYSCLK
    EALLOW;
    ClkCfgRegs.PERCLKDIVSEL.bit.EPWMCLKDIV = 0;
    EDIS;

    // enable PWM1, PWM2 and PWM3
    CpuSysRegs.PCLKCR2.bit.EPWM4=1;
    CpuSysRegs.PCLKCR2.bit.EPWM5=1;
    CpuSysRegs.PCLKCR2.bit.EPWM6=1;

    // For this case just init GPIO pins for ePWM1, ePWM2, ePWM3
    // These functions are in the F2837xS_EPwm.c file
    InitEPwm4Gpio();
    InitEPwm5Gpio();
    InitEPwm6Gpio();

    EALLOW; // This is needed to write to EALLOW protected registers
    PieVectTable.EPWM4_INT = &MtrCtrlISR;
    EDIS;   // This is needed to disable write to EALLOW protected registers

    EALLOW;
    CpuSysRegs.PCLKCR0.bit.TBCLKSYNC =0;
    EDIS;

    InitEPwm4Driver();
    InitEPwm5Driver();
    InitEPwm6Driver();

    EALLOW;
    CpuSysRegs.PCLKCR0.bit.TBCLKSYNC =1;
    EDIS;

    // Enable CPU INT3 which is connected to EPWM1-3 INT:
    IER |= M_INT3;

    // Enable EPWM INTn in the PIE: Group 3 interrupt 1-3
    PieCtrlRegs.PIEIER3.bit.INTx4 = 1;

    PwmOff();
}



/*******************************************************************************************
Function Name:   PhaseTypeRSTPWMCmp
Version:         V1.0
Input:           RST三相输入比较直
Description:    输入三相比较
*******************************************************************************************/
void PhaseTypeRSTPWMCmp(uint16 RCmp, uint16 SCmp, uint16 TCmp)
{
    /* 从底层设置比较值   */
    EPwm1Regs.CMPA.bit.CMPA = RCmp;
    EPwm2Regs.CMPA.bit.CMPA = SCmp;
    EPwm3Regs.CMPA.bit.CMPA = TCmp;

}


/*******************************************************************************************
Function Name:   PhaseTypeWVUPWMCmp
Version:         V1.0
Description:    设置比较值
*******************************************************************************************/
void PhaseTypeWVUPWMCmp(uint16 UCmp, uint16 VCmp, uint16 WCmp)
{
    /* 从底层改相序   */
    EPwm4Regs.CMPA.bit.CMPA = WCmp;
    EPwm5Regs.CMPA.bit.CMPA = VCmp;
    EPwm6Regs.CMPA.bit.CMPA = UCmp;

}

void PhaseTypeUVWPWMCmp(uint16 UCmp, uint16 VCmp, uint16 WCmp)
{
    /* 从底层改相序   */
    EPwm4Regs.CMPA.bit.CMPA = UCmp;
    EPwm5Regs.CMPA.bit.CMPA = VCmp;
    EPwm6Regs.CMPA.bit.CMPA = WCmp;

}

/*******************************************************************************************
Function Name:   PwmPFCOn
Version:         V1.0
Description:     使能PFCPWM PFCPWM_EN为设置74ACT541使能信号
*******************************************************************************************/
void PwmPFCOn(void)
{
    if(EPwm1Regs.TZFLG.bit.OST)
    {
        DINT;
        EALLOW;

        EPwm1Regs.TZCLR.bit.OST=1;
        EPwm1Regs.TZCLR.bit.INT=1;
        EPwm2Regs.TZCLR.bit.OST=1;
        EPwm2Regs.TZCLR.bit.INT=1;
        EPwm3Regs.TZCLR.bit.OST=1;
        EPwm3Regs.TZCLR.bit.INT=1;
        PFCPWM_EN;

        EDIS;
        EINT;
    }
}


/*******************************************************************************************
Function Name:   PwmPFCOff
Version:         V1.0
Description:     禁能PFCPWM PFCPWM_DIS为设置74ACT541输出高阻
*******************************************************************************************/
void PwmPFCOff(void)
{
    if(EPwm1Regs.TZFLG.bit.OST==0)
    {
        DINT;
        EALLOW;
        PFCPWM_DIS;
        EPwm1Regs.TZFRC.bit.OST=1;
        EPwm2Regs.TZFRC.bit.OST=1;
        EPwm3Regs.TZFRC.bit.OST=1;
        Set_SpeedZero();
        EDIS;
        EINT;
    }
}


void PwmOn(void)
{
    if(EPwm4Regs.TZFLG.bit.OST)
    {
        DINT;
        EALLOW;
        EPwm4Regs.TZCLR.bit.OST=1;
        EPwm4Regs.TZCLR.bit.INT=1;
        EPwm5Regs.TZCLR.bit.OST=1;
        EPwm5Regs.TZCLR.bit.INT=1;
        EPwm6Regs.TZCLR.bit.OST=1;
        EPwm6Regs.TZCLR.bit.INT=1;
        PWM_EN;
        EDIS;
        EINT;
    }
}



void PwmOff(void)
{
    if(EPwm4Regs.TZFLG.bit.OST==0)
    {
        DINT;
        EALLOW;
        PWM_DIS;
        EPwm4Regs.TZFRC.bit.OST=1;
        EPwm5Regs.TZFRC.bit.OST=1;
        EPwm6Regs.TZFRC.bit.OST=1;
        EDIS;
        EINT;
    }
}



/*******************************************************************************************
Function Name:   InitEPWM
Version:         V1.0
Description:     初始化风散函数
History:
*******************************************************************************************/
void InitFanPWM(void)
{

    // PWMCLK equal to PLLSYSCLK
    EALLOW;
    ClkCfgRegs.PERCLKDIVSEL.bit.EPWMCLKDIV = 0;
    EDIS;

    // enable PWM11
    CpuSysRegs.PCLKCR2.bit.EPWM11=1;


    InitEPwm11Gpio();


  //  InitEPwm11Driver();

    EALLOW;
    CpuSysRegs.PCLKCR0.bit.TBCLKSYNC =1;
    EDIS;


}
