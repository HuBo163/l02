/*
 * Task.c
 *
 *  Created on: 2020年4月9日
 *      Author: dfs_h
 */

// State Machine function prototypes
//------------------------------------
#include"Task.h"
#include"f_data_enterchange.h"
#include"f_SFRA.h"
#include"Flash.h"
#define  GetTime()  (CpuTimer1.RegsAddr->TIM.all)

// A branch states
void A0(void);
void A1(void);  //state A1
void A2(void);  //state A2
void A3(void);  //state A3
void A4(void);  //state A4


// Variable declarations
void (*Alpha_State_Ptr)(void);  // Base States pointer
void (*A_Task_Ptr)(void);       // State pointer A branch

static unsigned  long m_BaseTime;//m_DetaTime;

//=================================================================================
//  STATE-MACHINE SEQUENCING AND SYNCRONIZATION FOR SLOW BACKGROUND TASKS
//=================================================================================

void Task_Init(void)
{
    Alpha_State_Ptr = &A0;
    A_Task_Ptr = &A1;
    m_BaseTime = GetTime();
}


extern void change_displaydata(void);
//--------------------------------- FRAMEWORK -------------------------------------
void A0(void)
{
    eMBErrorCode Code;
  //  m_DetaTime = m_BaseTime - GetTime();


    // loop rate synchronizer for A-tasks
    if(CpuTimer1Regs.TCR.bit.TIF == 1)
    {
        CpuTimer1Regs.TCR.bit.TIF =1;   //计数器清零

        Code = eMBPoll();
     //   SFRA_RUNBACKGROUND();
        //-----------------------------------------------------------
        (*A_Task_Ptr)();        // jump to an A Task (A1,A2,A3,...)
        //-----------------------------------------------------------

    }

}


//=================================================================================
//  A - TASKS (executed in every 1 msec)
//=================================================================================
//--------------------------------------------------------
void A1(void) // SPARE (not used)
//--------------------------------------------------------
{
    static Uint16 i=0;

    DisplayScanPrepare();
    i++;
    if(i>200)
   {
       i=0;
       modbus_data_enterchange(funcCode.code.startMode);
       GpioDataRegs. GPCTOGGLE.bit.GPIO66=1;
       if(funcCode.code.updata == 0xB255)
       {
           Jump_Bootloader();
       }

    }
    //-------------------
    //the next time CpuTimer0 'counter' reaches Period value go to A2
    A_Task_Ptr = &A2;
    //-------------------
}

//-----------------------------------------------------------------
void A2(void) // SPARE (not used)
//-----------------------------------------------------------------
{    
    DisplayScan();
    KeyScan();
    KeyProcess();

    //-------------------
    //the next time CpuTimer0 'counter' reaches Period value go to A3
    A_Task_Ptr = &A3;
    //-------------------
}

extern void pfcdata_test1(void);
//-----------------------------------------
void A3(void) // SPARE (not used)
//-----------------------------------------
{
     static unsigned char i=0;
    WinkDeal();
    UpdateDisplayBuffer();
    i++;
    if(i==10)
    {
        pfcdata_test1();
        i = 0;
    }
    //-----------------
    //the next time CpuTimer0 'counter' reaches Period value go to A4
    A_Task_Ptr = &A4;
    //-----------------
}

extern void pfc_update_display(void);
extern void Changerun_Para();
//-----------------------------------------
void A4(void) // SPARE (not used)
//-----------------------------------------
{
    static unsigned int i=0;
    Start_Control();
    Changerun_Para();
    pfc_update_display();
    i++;
    if(i>=10)
    {
        change_displaydata();
        i=0;
    }
    //-----------------
    //the next time CpuTimer0 'counter' reaches Period value go to A0
    A_Task_Ptr = &A1;
    //-----------------
}
