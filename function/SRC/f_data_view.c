/*
 * f_data_view.c
 *
 *  Created on: 2021年6月8日
 *      Author: dfs_h
 */

#include "f_data_view.h"

void sci_delay(Uint16 j)
{
  Uint16 i=j;
  for(;i>0;i--)
    j=i;
}


//调试用串口通信初始化
void communicate_init(Uint32 band_rate)
{
  Uint32  BaudRate_Reg_temp;
  BaudRate_Reg_temp = (USHORT)((200000000/(4*8*band_rate))-1);
  InitSciaGpio();                           //使用串口B作为freemodbus的串口

  SciaRegs.SCICCR.bit.ADDRIDLE_MODE = 0;    //选择通信协议，选择空闲模式，此模式兼容RS232协议
  SciaRegs.SCICCR.bit.LOOPBKENA = 0;        //自测试模式使能位:为1使能，为0禁用
  SciaRegs.SCICCR.bit.PARITY = 0;           //奇偶校验位选择位:0为奇校验，1为偶校验
  SciaRegs.SCICCR.bit.PARITYENA = 0;        //奇偶校验位使能:为1使能，为0禁用
  SciaRegs.SCICCR.bit.SCICHAR = 7;          //该配置选择SCI的一个字符的长度可配置为1到8位(单位bit)
  SciaRegs.SCICCR.bit.STOPBITS = 0;         //停止位的配置: 0 1位停止位  1 2位停止位

  //SCIC控制寄存器的配置
  SciaRegs.SCICTL1.bit.RXENA = 1;            //SCI接收器使能位
  SciaRegs.SCICTL1.bit.RXERRINTENA = 0;      //SCI接受中断错误使能
  SciaRegs.SCICTL1.bit.SLEEP = 0;            //睡眠模式使能位
  SciaRegs.SCICTL1.bit.SWRESET = 0;          //SCI软件复位
  SciaRegs.SCICTL1.bit.TXENA = 1;            //SCI发送器使能位
  SciaRegs.SCICTL1.bit.TXWAKE = 0;           //SCI发送器唤醒模式选择位，具体的不是太明白
  SciaRegs.SCICTL2.bit.RXBKINTENA = 0;       //接收中断使能
  SciaRegs.SCICTL2.bit.TXINTENA = 0;

  //波特率计算
  //9600
  //SciaRegs.SCIHBAUD = 0x01;
  //SciaRegs.SCILBAUD = 0xE7;
  SciaRegs.SCIHBAUD.bit.BAUD = (BaudRate_Reg_temp>>8)&0xFF;
  SciaRegs.SCILBAUD.bit.BAUD = BaudRate_Reg_temp&0xFF;
  SciaRegs.SCICTL1.bit.SWRESET = 1;

  //以下代码是配置SCI的接收数据的FIFO和发送数据的FIFO
  //-------------------接收数据的FIFO配置
  SciaRegs.SCIFFTX.bit.SCIRST = 0;          //复位SCIC模块
  SciaRegs.SCIFFTX.bit.SCIRST = 1;          //复位SCIC模块
  SciaRegs.SCIFFTX.bit.SCIFFENA = 1;        //使能FIFO功能
  SciaRegs.SCIFFRX.bit.RXFFIENA = 0;        //关闭接收中断
  SciaRegs.SCIFFRX.bit.RXFIFORESET = 0;     //复位接收器的FIFO
  SciaRegs.SCIFFRX.bit.RXFIFORESET = 1;
  SciaRegs.SCIFFRX.bit.RXFFIL = 0x01;       //接受1个字节之后产生一个中断

  //---------------------发送数据的FIFO配置
  SciaRegs.SCIFFTX.bit.SCIRST = 0;
  SciaRegs.SCIFFTX.bit.SCIRST = 1;
  SciaRegs.SCIFFTX.bit.SCIFFENA = 1;
  SciaRegs.SCIFFTX.bit.TXFFIENA = 0;
  SciaRegs.SCIFFTX.bit.TXFIFORESET = 0;
  SciaRegs.SCIFFTX.bit.TXFIFORESET=1;
  SciaRegs.SCIFFTX.bit.TXFFIL = 0x00;        //发送完一个字节产生中断
}


void Scia_transmit(unsigned char *pData, Uint16 size)
{
  unsigned char i=0;
  unsigned char j=0;

  for(j=0;j<size;j++)
  {

    while(SciaRegs.SCIFFTX.bit.TXFFST == 1)
    {
	sci_delay(10);
	i++;
	if(i>10)
	  return;
    }

    i=0;
    scia_xmit(*(pData+j)&0x00FF);
    while(SciaRegs.SCIFFTX.bit.TXFFST == 1)
    {
    sci_delay(10);
    i++;
    if(i>10)
      return;
    }

        i=0;
    scia_xmit((*(pData+j)&0xFF00)>>8);
  }

}

#define CH_COUNT 4
struct Frame {
    float fdata[CH_COUNT];
    unsigned char tail[4];
};

struct Frame fram={.tail={0x0000, 0x7F80}};

void txt_data()
{

  static float a=0.0f,b=0.0f;

  a+=0.01;
  b=sinf(a);
  fram.fdata[0]=b*(2.0f+(float)(rand()%100)/10000.0f);
  b=sinf(a+1.0);
  fram.fdata[1]=a;
  b=sinf(a+1.7);
  fram.fdata[2]=b*(2.0f+(float)(rand()%100)/10000.0f);
  b=sinf(a+2.4);
  fram.fdata[3]=b*(2.0f+(float)(rand()%100)/10000.0f);

  if(a>3.1415926*4+0.1)
	a-=3.1415926*4+0.1;

  Scia_transmit((unsigned char*)fram.fdata,(uint16_t)(CH_COUNT*2+2));
}
