/*
 * f_cmpss.c
 *
 *  Created on: 2021��7��7��
 *      Author: dfs_h
 */
#include"f_cmpss.h"

//
// Defines
//
//definitions for selecting DACH reference
#define REFERENCE_VDDA     0
#define REFERENCE_VDAC     1
//definitions for COMPH input selection
#define NEGIN_DAC          0
#define NEGIN_PIN          1
//definitions for CTRIPH/CTRIPOUTH output selection
#define CTRIP_ASYNCH       0
#define CTRIP_SYNCH        1
#define CTRIP_FILTER       2
#define CTRIP_LATCH        3
//definitions for selecting output pin
#define GPIO_CTRIP_PIN_NUM      14 //OUTPUTXBAR3 is mux'd with GPIO14
#define GPIO_CTRIP_PER_NUM       6 //OUTPUTXBAR3 is peripheral option 6 for
                                   //GPIO14
#define GPIO_CTRIPOUT_PIN_NUM   15 //EPWM8B is mux'd with GPIO15
#define GPIO_CTRIPOUT_PER_NUM    1 //EPWM8B is peripheral option 1 for GPIO15


// InitCMPSS - Initialize CMPSS1 and configure settings
//
void InitCMPSS(void)
{
    EALLOW;

    //
    //Enable CMPSS
    //
    Cmpss1Regs.COMPCTL.bit.COMPDACE = 1;

    //
    //NEG signal comes from DAC
    //
    Cmpss1Regs.COMPCTL.bit.COMPHSOURCE = NEGIN_DAC;
    Cmpss1Regs.COMPCTL.bit.COMPLSOURCE = NEGIN_DAC;

    //
    //Use VDDA as the reference for DAC
    //
    Cmpss1Regs.COMPDACCTL.bit.SELREF = REFERENCE_VDDA;

    //
    //Set DAC to midpoint for arbitrary reference
    //
    Cmpss1Regs.DACHVALS.bit.DACVAL = 3848;
    Cmpss1Regs.DACLVALS.bit.DACVAL = 200;

    Cmpss1Regs.COMPCTL.bit.COMPLINV =1;
    //
    // Configure CTRIPOUT path
    // Asynch output feeds CTRIPH and CTRIPOUTH
    //
    Cmpss1Regs.COMPCTL.bit.CTRIPHSEL = CTRIP_ASYNCH;  //
    Cmpss1Regs.COMPCTL.bit.CTRIPLSEL = CTRIP_ASYNCH;


    //Cmpss1Regs.COMPCTL.bit.CTRIPOUTHSEL = CTRIP_ASYNCH;
    //Cmpss1Regs.COMPCTL.bit.CTRIPOUTLSEL = CTRIP_ASYNCH;

    //
    // Configure CTRIPOUTH output pin
    // Configure OUTPUTXBAR3 to be CTRIPOUT1H
    //
/*    OutputXbarRegs.OUTPUT3MUX0TO15CFG.bit.MUX0 = 0;

    //
    //Enable OUTPUTXBAR3 Mux for Output
    //
    OutputXbarRegs.OUTPUT3MUXENABLE.bit.MUX0 = 1;*/
    EDIS;
}



