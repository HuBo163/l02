/*
 * f_SCIDriver.c
 *
 *  Created on: 2020年4月7日
 *      Author: dfs_h
 */

#include"f_SCIDriver.h"


//485 通信用到串口B
void InitScibGpio(void)
{
    EALLOW;

    //485_EN引脚使能初始化
    GPIO_SetupPinOptions(60, GPIO_OUTPUT, GPIO_PUSHPULL);
    GPIO_SetupPinMux(60, GPIO_MUX_CPU1, 0);

    //SCI引脚初始化
    GPIO_SetupPinMux(87, GPIO_MUX_CPU1, 5);
    GPIO_SetupPinOptions(87, GPIO_INPUT, GPIO_PUSHPULL);
    GPIO_SetupPinMux(86, GPIO_MUX_CPU1, 5);
    GPIO_SetupPinOptions(86, GPIO_OUTPUT, GPIO_ASYNC);

    EDIS;
}


