#include"AdcDriver.h"


void ConfigureADC(void);
void SetupADCSoftware(void);
void EnableADCINT(void);
void ConfigurePPBLimits(void);
interrupt void adca1_isr(void);
interrupt void adca_ppb_isr(void);
interrupt void adcb_ppb_isr(void);
interrupt void adcd_ppb_isr(void);


void InitAdc(void)
{
    EnableADCINT();
    ConfigureADC();
    SetupADCSoftware();
    ConfigurePPBLimits();
    Dma_Init();
}


uint16  GetADCAResult(uint16 Chenal)
{
    uint16 Result;

    Result = ((uint16*)&AdcaResultRegs)[Chenal]&0x0FFF;

    return Result;
}

uint16  GetADCBResult(uint16 Chenal)
{
    uint16 Result;

    Result = ((uint16*)&AdcbResultRegs)[Chenal]&0x0FFF;

    return Result;
}

/*
uint16  GetADCCResult(uint16 Chenal)
{
    uint16 Result;

    Result = ((uint16*)&AdcResultRegs)[Chenal]&0x0FFF;

    return Result;
}
*/

uint16  GetADCDResult(uint16 Chenal)
{
    uint16 Result;

    Result = ((uint16*)&AdcdResultRegs)[Chenal]&0x0FFF;

    return Result;
}




void EnableADCINT(void)
{
    EALLOW;
    PieVectTable.ADCA1_INT = &adca1_isr; //function for ADCA interrupt 1
    EDIS;

    //enable PIE interrupt
    PieCtrlRegs.PIEIER1.bit.INTx1 = 0;

   // IER |= M_INT1;
}



//Write ADC configurations and power up the ADC for both ADC A and ADC B
void ConfigureADC(void)
{
    EALLOW;

    //write configurations , ADCCLK = 5M - 50M
    AdcaRegs.ADCCTL2.bit.PRESCALE = 2;//2; //set ADCCLK divider to /2
    AdcbRegs.ADCCTL2.bit.PRESCALE = 2;//2; //set ADCCLK divider to /2
   // AdccRegs.ADCCTL2.bit.PRESCALE = 6;//2; //set ADCCLK divider to /2
    AdcdRegs.ADCCTL2.bit.PRESCALE = 2;//2; //set ADCCLK divider to /2
    AdcSetMode(ADC_ADCA, ADC_RESOLUTION_12BIT, ADC_SIGNALMODE_SINGLE);
    AdcSetMode(ADC_ADCB, ADC_RESOLUTION_12BIT, ADC_SIGNALMODE_SINGLE);
    AdcSetMode(ADC_ADCC, ADC_RESOLUTION_12BIT, ADC_SIGNALMODE_SINGLE);
    AdcSetMode(ADC_ADCD, ADC_RESOLUTION_12BIT, ADC_SIGNALMODE_SINGLE);

    //Set pulse positions to late
    AdcaRegs.ADCCTL1.bit.INTPULSEPOS = 1;
    AdcbRegs.ADCCTL1.bit.INTPULSEPOS = 1;
  //  AdccRegs.ADCCTL1.bit.INTPULSEPOS = 1;
    AdcdRegs.ADCCTL1.bit.INTPULSEPOS = 1;

    //power up the ADCs
    AdcaRegs.ADCCTL1.bit.ADCPWDNZ = 1;
    AdcbRegs.ADCCTL1.bit.ADCPWDNZ = 1;
 //   AdccRegs.ADCCTL1.bit.ADCPWDNZ = 1;
    AdcdRegs.ADCCTL1.bit.ADCPWDNZ = 1;

    //delay for 1ms to allow ADC time to power up
    DELAY_US(1000);

    EDIS;
}



void SetupADCSoftware(void)
{
    uint16 acqps;

    //determine minimum acquisition window (in SYSCLKS) based on resolution
    if(ADC_RESOLUTION_12BIT == AdcaRegs.ADCCTL2.bit.RESOLUTION){
        acqps = 199; //75ns(14 min)  1/200M=5ns  (199+1)*5=1uS  采样率设置在1M 提升抗干扰能力
    }
    else { //resolution is 16-bit
        acqps = 63; //320ns
    }

    //Select the channels to convert and end of conversion flag
    //ADCA
    EALLOW;
    AdcaRegs.ADCSOC0CTL.bit.CHSEL = 0;  //SOC0 will convert pin A0
    AdcaRegs.ADCSOC0CTL.bit.ACQPS = acqps; //sample window is acqps + 1 SYSCLK cycles
    AdcaRegs.ADCSOC0CTL.bit.TRIGSEL = 5; //trigger on ePWM1 SOCA/C

    AdcaRegs.ADCSOC1CTL.bit.CHSEL = 1;  //SOC0 will convert pin A1
    AdcaRegs.ADCSOC1CTL.bit.ACQPS = acqps; //sample window is acqps + 1 SYSCLK cycles
    AdcaRegs.ADCSOC1CTL.bit.TRIGSEL = 5; //trigger on ePWM1 SOCA/C

    AdcaRegs.ADCSOC2CTL.bit.CHSEL = 2;  //SOC0 will convert pin A2
    AdcaRegs.ADCSOC2CTL.bit.ACQPS = acqps; //sample window is acqps + 1 SYSCLK cycles
    AdcaRegs.ADCSOC2CTL.bit.TRIGSEL = 5; //trigger on ePWM1 SOCA/C

    AdcaRegs.ADCSOC3CTL.bit.CHSEL = 3;  //SOC0 will convert pin A3
    AdcaRegs.ADCSOC3CTL.bit.ACQPS = acqps; //sample window is acqps + 1 SYSCLK cycles
    AdcaRegs.ADCSOC3CTL.bit.TRIGSEL = 0x15; //trigger on ePWM9 SOCA/C

    AdcaRegs.ADCSOC4CTL.bit.CHSEL = 4;  //SOC0 will convert pin A4
    AdcaRegs.ADCSOC4CTL.bit.ACQPS = acqps; //sample window is acqps + 1 SYSCLK cycles
    AdcaRegs.ADCSOC4CTL.bit.TRIGSEL = 5; //trigger on ePWM1 SOCA/C

//    AdcaRegs.ADCSOC5CTL.bit.CHSEL = 5;  //SOC0 will convert pin A5
//    AdcaRegs.ADCSOC5CTL.bit.ACQPS = acqps; //sample window is acqps + 1 SYSCLK cycles
//    AdcaRegs.ADCSOC5CTL.bit.TRIGSEL = 5; //trigger on ePWM1 SOCA/C
//    AdcaRegs.ADCINTSEL1N2.bit.INT1SEL = 5; //end of SOC5 will set INT1 flag
//    AdcaRegs.ADCINTSEL1N2.bit.INT1E = 1;   //enable INT1 flag
//    AdcaRegs.ADCINTFLGCLR.bit.ADCINT1 = 1; //make sure INT1 flag is cleared

    //ADCB 0 1 2 3
    AdcbRegs.ADCSOC0CTL.bit.CHSEL = 0;  //SOC0 will convert pin B0
    AdcbRegs.ADCSOC0CTL.bit.ACQPS = acqps; //sample window is acqps + 1 SYSCLK cycles
    AdcbRegs.ADCSOC0CTL.bit.TRIGSEL = 5; //trigger on ePWM1 SOCA/C

    AdcbRegs.ADCSOC1CTL.bit.CHSEL = 1;  //SOC0 will convert pin B1
    AdcbRegs.ADCSOC1CTL.bit.ACQPS = acqps; //sample window is acqps + 1 SYSCLK cycles
    AdcbRegs.ADCSOC1CTL.bit.TRIGSEL = 5; //trigger on ePWM1 SOCA/C

    AdcbRegs.ADCSOC2CTL.bit.CHSEL = 2;  //SOC0 will convert pin B2
    AdcbRegs.ADCSOC2CTL.bit.ACQPS = acqps; //sample window is acqps + 1 SYSCLK cycles
    AdcbRegs.ADCSOC2CTL.bit.TRIGSEL = 5; //trigger on ePWM1 SOCA/C

    AdcbRegs.ADCSOC3CTL.bit.CHSEL = 3;  //SOC0 will convert pin B3
    AdcbRegs.ADCSOC3CTL.bit.ACQPS = acqps; //sample window is acqps + 1 SYSCLK cycles
    AdcbRegs.ADCSOC3CTL.bit.TRIGSEL = 0x15; //trigger on ePWM9 SOCA/C

    //ADCC 2 3 4
//    AdccRegs.ADCSOC2CTL.bit.CHSEL = 2;  //SOC0 will convert pin C2
//    AdccRegs.ADCSOC2CTL.bit.ACQPS = acqps; //sample window is acqps + 1 SYSCLK cycles
//    AdccRegs.ADCSOC2CTL.bit.TRIGSEL = 5; //trigger on ePWM1 SOCA/C
//
//    AdccRegs.ADCSOC3CTL.bit.CHSEL = 3;  //SOC0 will convert pin C3
//    AdccRegs.ADCSOC3CTL.bit.ACQPS = acqps; //sample window is acqps + 1 SYSCLK cycles
//    AdccRegs.ADCSOC3CTL.bit.TRIGSEL = 5; //trigger on ePWM1 SOCA/C
//
//    AdccRegs.ADCSOC4CTL.bit.CHSEL = 4;  //SOC0 will convert pin C4
//    AdccRegs.ADCSOC4CTL.bit.ACQPS = acqps; //sample window is acqps + 1 SYSCLK cycles
//    AdccRegs.ADCSOC4CTL.bit.TRIGSEL = 5; //trigger on ePWM1 SOCA/C

    //ADCD 0 1 2 3 4
    AdcdRegs.ADCSOC0CTL.bit.CHSEL = 0;  //SOC0 will convert pin D0
    AdcdRegs.ADCSOC0CTL.bit.ACQPS = acqps; //sample window is acqps + 1 SYSCLK cycles
    AdcdRegs.ADCSOC0CTL.bit.TRIGSEL = 5; //trigger on ePWM1 SOCA/C

    AdcdRegs.ADCSOC1CTL.bit.CHSEL = 1;  //SOC0 will convert pin D1
    AdcdRegs.ADCSOC1CTL.bit.ACQPS = acqps; //sample window is acqps + 1 SYSCLK cycles
    AdcdRegs.ADCSOC1CTL.bit.TRIGSEL = 5; //trigger on ePWM1 SOCA/C

    AdcdRegs.ADCSOC2CTL.bit.CHSEL = 2;  //SOC0 will convert pin D2
    AdcdRegs.ADCSOC2CTL.bit.ACQPS = acqps; //sample window is acqps + 1 SYSCLK cycles
    AdcdRegs.ADCSOC2CTL.bit.TRIGSEL = 5; //trigger on ePWM1 SOCA/C

    AdcdRegs.ADCSOC3CTL.bit.CHSEL = 3;  //SOC0 will convert pin D3
    AdcdRegs.ADCSOC3CTL.bit.ACQPS = acqps; //sample window is acqps + 1 SYSCLK cycles
    AdcdRegs.ADCSOC3CTL.bit.TRIGSEL = 0x15; //trigger on ePWM9 SOCA/C

//    AdcdRegs.ADCSOC4CTL.bit.CHSEL = 4;  //SOC0 will convert pin D4
//    AdcdRegs.ADCSOC4CTL.bit.ACQPS = acqps; //sample window is acqps + 1 SYSCLK cycles
//    AdcdRegs.ADCSOC4CTL.bit.TRIGSEL = 5; //trigger on ePWM1 SOCA/C
    AdcaRegs.ADCINTSEL1N2.bit.INT1E = 1;    // Enable INT1 flag
    AdcaRegs.ADCINTSEL1N2.bit.INT1CONT = 1;
    AdcaRegs.ADCINTSEL1N2.bit.INT1SEL = 3;

    AdcbRegs.ADCINTSEL1N2.bit.INT1E = 1;    // Enable INT1 flag
    AdcbRegs.ADCINTSEL1N2.bit.INT1CONT = 1;
    AdcbRegs.ADCINTSEL1N2.bit.INT1SEL = 3;

    AdcdRegs.ADCINTSEL1N2.bit.INT1E = 1;    // Enable INT1 flag
    AdcdRegs.ADCINTSEL1N2.bit.INT1CONT = 1;
    AdcdRegs.ADCINTSEL1N2.bit.INT1SEL = 3;

    EDIS;
}




void ConfigurePPBLimits(void)
{
    EALLOW;
    PieVectTable.ADCA_EVT_INT = &adca_ppb_isr; //function for ADCA PPB
    PieVectTable.ADCB_EVT_INT = &adcb_ppb_isr; //function for ADCB PPB
    PieVectTable.ADCD_EVT_INT = &adcd_ppb_isr; //function for ADCC PPB
    EDIS;

    PieCtrlRegs.PIEIER10.bit.INTx1 = 0;        //function for ADCA PPB
    PieCtrlRegs.PIEIER10.bit.INTx5 = 0;        //function for ADCB PPB
    PieCtrlRegs.PIEIER10.bit.INTx9 = 0;        //function for ADCC PPB

    IER |= M_INT10; //Enable group 10 interrupts

    EALLOW;

    AdcaRegs.ADCPPB1CONFIG.bit.CONFIG = 3;  //PPB1 is associated with soc
    //set high and low limits
    AdcaRegs.ADCPPB1TRIPHI.bit.LIMITHI = 2000;
    AdcaRegs.ADCPPB1TRIPLO.bit.LIMITLO = 1000;
    //enable high and low limit events to generate interrupt
    AdcaRegs.ADCEVTINTSEL.bit.PPB1TRIPHI = 1;
    AdcaRegs.ADCEVTINTSEL.bit.PPB1TRIPLO = 1;

    AdcbRegs.ADCPPB1CONFIG.bit.CONFIG = 3;  //PPB1 is associated with soc
    //set high and low limits
    AdcbRegs.ADCPPB1TRIPHI.bit.LIMITHI = 2000;
    AdcbRegs.ADCPPB1TRIPLO.bit.LIMITLO = 1000;
    //enable high and low limit events to generate interrupt
    AdcbRegs.ADCEVTINTSEL.bit.PPB1TRIPHI = 1;
    AdcbRegs.ADCEVTINTSEL.bit.PPB1TRIPLO = 1;

    AdcdRegs.ADCPPB1CONFIG.bit.CONFIG = 3;  //PPB1 is associated with soc
    //set high and low limits
    AdcdRegs.ADCPPB1TRIPHI.bit.LIMITHI = 2000;
    AdcdRegs.ADCPPB1TRIPLO.bit.LIMITLO = 1000;
    //enable high and low limit events to generate interrupt
    AdcdRegs.ADCEVTINTSEL.bit.PPB1TRIPHI = 1;
    AdcdRegs.ADCEVTINTSEL.bit.PPB1TRIPLO = 1;

    EDIS;
}



uint16 adca_cnt = 0;

uint16 adca_ppb_cnt = 0;
uint16 adca_ppb_error = 0;

uint16 adcb_ppb_cnt = 0;
uint16 adcb_ppb_error = 0;

uint16 adcd_ppb_cnt = 0;
uint16 adcd_ppb_error = 0;



interrupt void adca1_isr(void)
{
    adca_cnt++;

    AdcaRegs.ADCINTFLGCLR.bit.ADCINT1 = 1; //clear INT1 flag
    PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;
}




interrupt void adca_ppb_isr(void)
{
    //warning, you are outside of PPB limits
    if(AdcaRegs.ADCEVTSTAT.bit.PPB1TRIPHI)
    {
        //clear the trip flag and continue
        AdcaRegs.ADCEVTCLR.bit.PPB1TRIPHI = 1;
        adca_ppb_error |= 0x02;
    }
    if(AdcaRegs.ADCEVTSTAT.bit.PPB1TRIPLO)
    {
        //clear the trip flag and continue
        AdcaRegs.ADCEVTCLR.bit.PPB1TRIPLO = 1;
        adca_ppb_error |= 0x01;
    }

    adca_ppb_cnt++;

    //disable adcevtint
    EALLOW;
    AdcaRegs.ADCEVTINTSEL.bit.PPB1TRIPHI = 0;
    AdcaRegs.ADCEVTINTSEL.bit.PPB1TRIPLO = 0;
    EDIS;

    PieCtrlRegs.PIEACK.all = PIEACK_GROUP10;
}




interrupt void adcb_ppb_isr(void)
{
    //warning, you are outside of PPB limits
    if(AdcbRegs.ADCEVTSTAT.bit.PPB1TRIPHI)
    {
        //clear the trip flag and continue
        AdcbRegs.ADCEVTCLR.bit.PPB1TRIPHI = 1;
        adcb_ppb_error |= 0x02;
    }
    if(AdcbRegs.ADCEVTSTAT.bit.PPB1TRIPLO)
    {
        //clear the trip flag and continue
        AdcbRegs.ADCEVTCLR.bit.PPB1TRIPLO = 1;
        adcb_ppb_error |= 0x01;
    }

    adcb_ppb_cnt++;

    //disable adcevtint
    EALLOW;
    AdcbRegs.ADCEVTINTSEL.bit.PPB1TRIPHI = 0;
    AdcbRegs.ADCEVTINTSEL.bit.PPB1TRIPLO = 0;
    EDIS;

    PieCtrlRegs.PIEACK.all = PIEACK_GROUP10;
}



interrupt void adcd_ppb_isr(void)
{
    //warning, you are outside of PPB limits
    if(AdcdRegs.ADCEVTSTAT.bit.PPB1TRIPHI)
    {
        //clear the trip flag and continue
        AdcdRegs.ADCEVTCLR.bit.PPB1TRIPHI = 1;
        adcd_ppb_error |= 0x02;
    }
    if(AdcdRegs.ADCEVTSTAT.bit.PPB1TRIPLO)
    {
        //clear the trip flag and continue
        AdcdRegs.ADCEVTCLR.bit.PPB1TRIPLO = 1;
        adcd_ppb_error |= 0x01;
    }

    adcd_ppb_cnt++;

    //disable adcevtint
    EALLOW;
    AdcdRegs.ADCEVTINTSEL.bit.PPB1TRIPHI = 0;
    AdcdRegs.ADCEVTINTSEL.bit.PPB1TRIPLO = 0;
    EDIS;

    PieCtrlRegs.PIEACK.all = PIEACK_GROUP10;
}
