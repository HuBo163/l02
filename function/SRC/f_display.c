#include"f_display.h"

#if 1
#define LOCALF
#define LOCALD extern
#else
#define LOCALF static
#define LOCALD static
#endif

LOCALF Uint16 ticker4LowerDisp;          // 运行时显示，最后2位不要更新过快
Uint16 curFcDispDigits;                  // 当前功能码的显示位数
// 十进制的个位、十位、百位、千位、万位
const int16 decNumber[5] = {1,      10,     100,    1000, 10000};
// 十六进制的个位、十位、百位、千位
const int16 hexNumber[4] = {0x0001, 0x0010, 0x0100, 0x1000};
const int16 deltaK[3] = {0, 1, -1};

Uint16  menu3Number;                     // 3级菜单显示的值
extern  enum MENU_LEVEL menuLevel;       // 当前菜单级别，即0,1,2,3级菜单
extern Uint16 funcCodeGradeCurMenuMode[FUNCCODE_GROUP_NUM];
extern const Uint16 funcCodeGradeAll[FUNCCODE_GROUP_NUM];

struct GROUP_DISPLAY
{
    Uint16 dispF;   // 功能码组F0, 显示F
    Uint16 disp0;   // 功能码组F0, 显示0
};
struct GROUP_DISPLAY groupDisplay;

struct CURRENT_FUNC_CODE
{
    Uint16 index;               // 当前功能码在funcCode.all[]数组的下标

    Uint16 group;               // 当前功能码的group
    Uint16 grade;               // 当前功能码的grade
};
struct CURRENT_FUNC_CODE curFc; // 当前功能码


#define DECIMAL_DISPLAY_UPDATE_TIME     6       // 0级菜单显示(运行时显示，停机时显示)小数点后2位显示缓冲更新时间，_*12ms
#define STOP_DISPLAY_NUM 16     // 停机时，LED显示参数的总数
#define RUN_DISPLAY_NUM  32     // 运行时，LED显示参数的总数
#define DECIMAL     0
#define HEX         1

// 是否可写。1-当前可写，0-当前不可写
#define IsWritable(attribute)                                       \
(((ATTRIBUTE_READ_AND_WRITE == (attribute).bit.writable) ||         \
 ((ATTRIBUTE_READ_ONLY_WHEN_RUN == (attribute).bit.writable) &&     \
 (!runFlag.bit.run)))                                               \
)                                                                   \

// 按下RUN键
LOCALD void MenuOnRun(void);

// 按下STOP键
LOCALD void MenuOnStop(void);

// 按下MF.K键
LOCALD void MenuOnMfk(void);


// 按下PRG键
LOCALD void Menu0OnPrg(void);
void Menu1OnPrg(void);
LOCALD void Menu2OnPrg(void);
LOCALD void Menu3OnPrg(void);

// 按下UP键
LOCALD void Menu0OnUp(void);    // 0级菜单下按键UP的函数
LOCALD void Menu1OnUp(void);    // 1级菜单下按键UP的函数
LOCALD void Menu2OnUp(void);    // 2级菜单下按键UP的函数
LOCALD void Menu3OnUp(void);    // 3级菜单下按键UP的函数

// 按下DOWN键
LOCALD void Menu0OnDown(void);
LOCALD void Menu1OnDown(void);
LOCALD void Menu2OnDown(void);
LOCALD void Menu3OnDown(void);

// 按下ENTER键
LOCALD void Menu0OnEnter(void);
LOCALD void Menu1OnEnter(void);
LOCALD void Menu2OnEnter(void);
LOCALD void Menu3OnEnter(void);

// 按下SHIFT键
LOCALD void Menu0OnShift(void);
LOCALD void Menu1OnShift(void);
LOCALD void Menu2OnShift(void);
LOCALD void Menu3OnShift(void);

// 按下QUICK键
LOCALD void MenuOnQuick(void);


LOCALD void Menu0OnUpDown(void);
LOCALD void Menu1OnUpDown(Uint16 flag);
LOCALD void Menu2OnUpDown(Uint16 flag);
LOCALD void Menu3OnUpDown(Uint16 flag);


// 显示缓冲更新
LOCALD void UpdateMenu0DisplayBuffer(void);
LOCALD void UpdateMenu1DisplayBuffer(void);
LOCALD void UpdateMenu2DisplayBuffer(void);
LOCALD void UpdateMenu3DisplayBuffer(void);
LOCALD void UpdateDisplayBufferAttribute(const Uint16 data, const union FUNC_ATTRIBUTE attribute);
void UpdateDisplayBufferVisualIoStatus(Uint32 value);
void UpdateDisplayBufferVisualDiFunc(Uint16 valueH, Uint32 valueL);
LOCALD void UpdateErrorDisplayBuffer(void);
LOCALD void UpdateTuneDisplayBuffer(void);


LOCALD void MenuPwdOnPrg(void);
LOCALD void MenuPwdHintOnUp(Uint16 flag);
LOCALD void MenuPwdHint2Input(void);
LOCALD void MenuPwdHintOnDown(Uint16 flag);
LOCALD void MenuPwdHintOnShift(void);
LOCALD void MenuPwdHintOnQuick(void);
LOCALD void UpdateMenuPwdHintDisplayBuffer(void);

LOCALD void MenuPwdInputOnPrg(void);
LOCALD void MenuPwdInputOnUp(void);
LOCALD void MenuPwdInputOnEnter(void);
LOCALD void MenuPwdInputOnDown(void);
LOCALD void MenuPwdInputOnShift(void);
LOCALD void MenuPwdInputOnQuick(void);
LOCALD void UpdateMenuPwdInputDisplayBuffer(void);

const sysMenu menu[MENU_LEVEL_NUM] =
{
// 0级菜单
    {Menu0OnPrg,        Menu0OnUpDown,      Menu0OnEnter,
     MenuOnMfk,         Menu0OnUpDown,      Menu0OnShift,
     MenuOnRun,         MenuOnStop,         MenuOnQuick,
     UpdateMenu0DisplayBuffer},
// 1级菜单
    {Menu1OnPrg,        Menu1OnUp,          Menu1OnEnter,
     MenuOnMfk,         Menu1OnDown,        Menu1OnShift,
     MenuOnRun,         MenuOnStop,         MenuOnQuick,
     UpdateMenu1DisplayBuffer},
// 2级菜单
    {Menu2OnPrg,        Menu2OnUp,          Menu2OnEnter,
     MenuOnMfk,         Menu2OnDown,        Menu2OnShift,
     MenuOnRun,         MenuOnStop,         MenuOnQuick,
     UpdateMenu2DisplayBuffer},
// 3级菜单
    {Menu3OnPrg,        Menu3OnUp,          Menu3OnEnter,
     MenuOnMfk,         Menu3OnDown,        Menu3OnShift,
     MenuOnRun,         MenuOnStop,         MenuOnQuick,
     UpdateMenu3DisplayBuffer},
// PwdHint菜单
    {MenuPwdOnPrg,      MenuPwdHint2Input,  MenuPwdHint2Input,
     MenuOnMfk,         MenuPwdHint2Input,  MenuPwdHintOnShift,
     MenuOnRun,         MenuOnStop,         MenuPwdHintOnQuick,
     UpdateMenuPwdHintDisplayBuffer},
// PwdInput菜单
    {MenuPwdOnPrg,      MenuPwdInputOnUp,   MenuPwdInputOnEnter,
     MenuOnMfk,         MenuPwdInputOnDown, MenuPwdInputOnShift,
     MenuOnRun,         MenuOnStop,         MenuPwdInputOnQuick,
     UpdateMenuPwdInputDisplayBuffer},
};

Uint16 GetDispDigits(Uint16 index);
Uint16 GroupUpDown(const Uint16 funcCodeGrade[], Uint16 group, Uint16 flag);
Uint16 ModifyFunccodeUpDown(Uint16 index, Uint16 *data, int16 delta);
Uint16 LimitOverTurnDeal(const Uint16 limit[], Uint16 data, Uint16 upper, Uint16 low, Uint16 flag);

// 可以与pDispValueU0形成结构体，但占用空间变大
union FUNC_ATTRIBUTE const dispAttributeU0[100] =
{
    0x094A & ~(1U<<11) | (1U<<12),     // 0, 运行频率
    0x0921 & ~(1U<<11) | (1U<<12),     // 1, 母线电压              0000 1001 0010 0001
    0x0912 & ~(1U<<11) | (1U<<12),     // 2, 母线电流               0000 1001 0001 0002
    0x0922 & ~(1U<<11) | (1U<<12),     // 3, 输出电压实时值    0000 1001 0010 0002
    0x0912 & ~(1U<<11) | (1U<<12),     // 4, 输出电流实时值

    0x0912 & ~(1U<<11) | (1U<<12),     // 5, 输出电流IA
    0x0912 & ~(1U<<11) | (1U<<12),     // 6, 输出电流IB
    0x0912 & ~(1U<<11) | (1U<<12),     // 7, 输出电流IC
    0x0922 & ~(1U<<11) | (1U<<12),     // 8, 输出电压UA
    0x0922 & ~(1U<<11) | (1U<<12),     // 9, 输出电压UB

    0x0922 & ~(1U<<11) | (1U<<12),     // 10, 输出电压UC
    0x2122 & ~(1U<<11) | (1U<<12),     // 11, 工作温度
    0x0140 & ~(1U<<11) | (1U<<12),     // 12, DI
    0x0140 & ~(1U<<11) | (1U<<12),     // 13, DO
    0x0140 & ~(1U<<11) | (1U<<12),     //

    0x0170 & ~(1U<<11) | (1U<<12),     //
    0x0170 & ~(1U<<11) | (1U<<12),     // 16,
    0x0140 & ~(1U<<11) | (1U<<12),     // 17,
    0x1142 & ~(1U<<11) | (1U<<12),     // 18,
    0x294A & ~(1U<<11) | (1U<<12),     // 19,
    0x0141 & ~(1U<<11) | (1U<<12),     // 20,

    0x2163 & ~(1U<<11) | (1U<<12),      // 21
    0x2163 & ~(1U<<11) | (1U<<12),      //
    0x2163 & ~(1U<<11) | (1U<<12),      //

    0x1D00 & ~(1U<<11) | (1U<<12),

    0x0140 & ~(1U<<11) | (1U<<12),     // 25,
    0x0141 & ~(1U<<11) | (1U<<12),     // 26,
    0x0148 & ~(1U<<11) | (1U<<12),     // 27,
    0x2972 & ~(1U<<11) | (1U<<12),     // 28,
    0x294A & ~(1U<<11) | (1U<<12),     // 29,

    0x094A & ~(1U<<11) | (1U<<12),     // 30,
    0x094A & ~(1U<<11) | (1U<<12),     // 31,
    0x0140 & ~(1U<<11) | (1U<<12),     // 32,
    0x0101 & ~(1U<<11) | (1U<<12),     // 33,
    0x0140 & ~(1U<<11) | (1U<<12),     // 34,

    0x2131 & ~(1U<<11) | (1U<<12),     // 35,
    0x0140 & ~(1U<<11) | (1U<<12),     // 36,
    0x2101 & ~(1U<<11) | (1U<<12),     // 37,
    0x0140 & ~(1U<<11) | (1U<<12),     // 38,
    0x0140 & ~(1U<<11) | (1U<<12),     // 39,

    0x0140 & ~(1U<<11) | (1U<<12),     // 40,
    0x0140 & ~(1U<<11) | (1U<<12),     // 41,
    0x0140 & ~(1U<<11) | (1U<<12),     // 42,
    0x0140 & ~(1U<<11) | (1U<<12),     // 33,
    0x0140 & ~(1U<<11) | (1U<<12),     // 44,

    0x0140 & ~(1U<<11) | (1U<<12),     // 45,
    0x0140 & ~(1U<<11) | (1U<<12),     // 46,
    0x0140 & ~(1U<<11) | (1U<<12),     // 47,
    0x0140 & ~(1U<<11) | (1U<<12),     // 48,
    0x0140 & ~(1U<<11) | (1U<<12),     // 49,

    0x1001,                             // 50
    0x1001,                             // 51
    0x1001,                             // 52
    0x1001,                             // 53
    0x1001,                             // 54
    0x1001,                             // 55
    0x1001,                             // 56
    0x1001,                             // 57
    0x0140 & ~(1U<<11) | (1U<<12),      // 58

    0x3172 & ~(1U<<11) | (1U<<12),      // 59
    0x3172 & ~(1U<<11) | (1U<<12),      // 60
    0x0140 & ~(1U<<11) | (1U<<12),      // 61
    0x0140 & ~(1U<<11) | (1U<<12),      // 62
    0x2972 & ~(1U<<11) | (1U<<12),      // 63
    0x2972 & ~(1U<<11) | (1U<<12),      // 64
    0x0140 & ~(1U<<11) | (1U<<12),      // 65
    0x0140 & ~(1U<<11) | (1U<<12),      // 65
};


//=====================================================================
// 停机监视参数
// 1.对应于运行参数中的序号
//=====================================================================
const Uint16 stopDispIndex[STOP_DISPLAY_NUM] =
{
    1,      // 0  设定频率
    2,      // 1  母线电压
    11,     // 2  母线电流
    12,
    13,

    5,      // 3  输出电压实时值
    6,      // 4  输出电流实时值

    7,      // 5  Ia
    8,      // 6  Ib
    9,      // 7  Ic
    10,     // 8  Ua
            // 9  Ub
    11,     // 10 Uc
            // 11 Temp
            // 12 DI
            // 13 DO
};

LOCALF Uint16 OverTurnDeal(Uint16 data, Uint16 upper, Uint16 lower, Uint16 flag);

//=====================================================================
//
// 所有菜单级别下，按下run键的处理
// run按键在 运行命令函数中处理，直接使用当前的按键。
//
//=====================================================================
LOCALF void MenuOnRun(void)
{
    ;
}


//=====================================================================
//
// 所有菜单级别下，按下stop键的处理
// run按键在 运行命令函数中处理，直接使用当前的按键。
//
//=====================================================================
LOCALF void MenuOnStop(void)
{
    ;
}



// quick处理
void MenuOnQuick(void)
{
    ;
}


//=====================================================================
//
// 翻转处理函数
//
// 输入:
//      data  -- 可能需要翻转的数据
//      upper -- 上限
//      lower -- 下限
//      flag  -- UP/DOWN标志
//
// 返回：
//      翻转之后的值。
//
//=====================================================================
LOCALF Uint16 OverTurnDeal(Uint16 data, Uint16 upper, Uint16 lower, Uint16 flag)
{
    if (ON_UP_KEY == flag)
    {
        if (upper == data)
        {
            data = lower;
        }
    }
    else
    {
        if (lower == data)
        {
            data = upper;
        }
    }

    return data;
}

//=====================================================================
//
// 功能码限幅处理
//
// 输入:
// signal--- 需要限幅处理的功能码的signal，
// data  --- 修改之前的数据
// upper --- 功能码上限
// lower --- 功能码下限
// delta --- delta>0, UP; delta<0, DOWN; delta==0, 检测当前值是否在限值范围内
//
// 返回：修改之后的值
//
// 注意：
// 在有符号时，要考虑以下情况：
//   upper = 30000, data = +25000, delta = +10000   ==> data = 30000
// 在无符号时，要考虑以下情况：
//   lower = 5,     data = 4,       delta = -0      ==> data = 5
//   lower = 5,     data = 6,       delta = -10     ==> data = 5
//   upper = 65535, data = 60000,   delta = +10000  ==> data = 65535
//
//=====================================================================
Uint16 LimitDeal(Uint16 signal, Uint16 data, Uint16 upper, Uint16 lower, int16 delta)
{
    int32 data1, upper1, lower1;

    if (signal) // 有符号
    {
        data1 = (int32)(int16)data;
        upper1 = (int32)(int16)upper;
        lower1 = (int32)(int16)lower;
    }
    else        // 无符号
    {
        data1 = (int32)data;
        upper1 = (int32)upper;
        lower1 = (int32)lower;
    }

// 相当于对原值也进行比较了
    data1 += delta;
    if (data1 > upper1)         // 上限处理
    {
        data1 = upper1;
    }
    if (data1 < lower1)         // 下限处理
    {
        data1 = lower1;
    }

    return (Uint16)data1;
}


LOCALF void Menu1OnUp(void)
{
    curFc.group = GroupUpDown(funcCodeGradeCurMenuMode, curFc.group, ON_UP_KEY);
     // 不记忆grade，在修改group时，grade就清零
     curFc.grade = 0;
}


LOCALF void Menu2OnUpDown(Uint16 flag)
{
   if (menuAttri[MENU_LEVEL_2].operateDigit >= 3)   // 修改currentGroup
    {
        Menu1OnUpDown(flag);
    }
    else                        // 修改currentGrade
    {
        int16 delta = 1;
        Uint16 tmp;


         tmp = OverTurnDeal(curFc.grade, funcCodeGradeCurMenuMode[curFc.group] - 1, 0, flag);
        if (curFc.grade == tmp)    // 没有翻转
        {
            if (1 == menuAttri[MENU_LEVEL_2].operateDigit)
                delta = 10;

            if (ON_DOWN_KEY == flag)
                delta = -delta;

           curFc.grade = LimitDeal(0, curFc.grade, funcCodeGradeCurMenuMode[curFc.group] - 1, 0, delta);
        }
        else
        {
            curFc.grade = tmp;
        }
    }
}

LOCALF void Menu2OnUp(void)
{
    Menu2OnUpDown(ON_UP_KEY);
}


LOCALF void Menu3OnUp(void)
{
    Menu3OnUpDown(ON_UP_KEY);
}



LOCALF void Menu1OnDown(void)
{
    curFc.group = GroupUpDown(funcCodeGradeCurMenuMode, curFc.group, ON_DOWN_KEY);

   // 不记忆grade，在修改group时，grade就清零
       curFc.grade = 0;
}


LOCALF void Menu2OnDown(void)
{
    Menu2OnUpDown(ON_DOWN_KEY);
}


LOCALF void Menu3OnDown(void)
{
    Menu3OnUpDown(ON_DOWN_KEY);
}


//=====================================================================
//
// 所有菜单级别下，按下MF.K键的处理
//
//=====================================================================
LOCALF void MenuOnMfk(void)
{

}


//=====================================================================
//
// menu0, 零级菜单
//
//=====================================================================
LOCALF void Menu0OnPrg(void)
{
    menuLevel = MENU_LEVEL_1;
    menuAttri[menuLevel].operateDigit = 0;
}


LOCALF void Menu0OnEnter(void)
{

}


void MenuModeSwitch(void)
{

}


LOCALF void Menu0OnUpDown(void)
{

}


//=====================================================================
//
// 0级菜单显示的循环移位函数。
// 输入:
//      flag -- 1 按键shift之后， 调用本函数
//              0 改变功能码之后，调用本函数
//
//
//=====================================================================
LOCALF void cycleShiftDeal(Uint16 flag)
{
    Uint16 *bit = &funcCode.code.dispParaStopBit;
    Uint32 para = funcCode.code.ledDispParaStop;
    Uint16 max = STOP_DISPLAY_NUM;

    if (runFlag.bit.run)
    {
        bit = &funcCode.code.dispParaRunBit;
        para = funcCode.code.ledDispParaRun1 + ((Uint32)funcCode.code.ledDispParaRun2 << 16);    //确定要显示32个状态中的哪些状态
        max = RUN_DISPLAY_NUM;
    }

    if (!para)  // 防止设置为0
    {
        para = 1;
    }

    if ((!flag) && (para & (0x1UL << *bit)))    // 改变功能码，且当前显示bit仍然要显示
        return;

    do
    {
        if (++(*bit) >= max)
        {
 /*           if (errorCode)  // 有故障
            {
                menu0DispStatus = MENU0_DISP_STATUS_ERROR;  // 进入故障/告警显示状态
            }

            if (tuneCmd)
            {
                menu0DispStatus = MENU0_DISP_STATUS_TUNE;   // 进入调谐显示
            }
*/
            *bit = 0;
        }
    }
    while (!(para & (0x1UL << *bit)));
}



LOCALF void Menu0OnShift(void)
{
    ticker4LowerDisp = 0;       // 0级菜单下按键shift，显示最后2位数字

   {
       cycleShiftDeal(1);      // 0级菜单显示的循环移位处理
   }

}

//=====================================================================
//
// menu1, 一级菜单
//
//=====================================================================
void Menu1OnPrg(void)
{
    menuLevel = MENU_LEVEL_0;
    ticker4LowerDisp = 0;   // 菜单级别重新置为0时，ticker清零
}

LOCALF void Menu1OnEnter()
{
    menuLevel = MENU_LEVEL_2;
    menuAttri[menuLevel].operateDigit = 0;
}

LOCALF void Menu1OnUpDown(Uint16 flag)
{

}


LOCALF void Menu1OnShift(void)
{
    ;
}


//=====================================================================
//
// menu2, 二级菜单
//
//=====================================================================
LOCALF void Menu2OnPrg(void)
{
    menuLevel = MENU_LEVEL_1;
    ticker4LowerDisp = 0;   // 菜单级别重新置为0时，ticker清零
}


LOCALF void Menu2OnEnter(void)
{

    menuAttri[MENU_LEVEL_2].operateDigit = 0;
    curFc.index = GetGradeIndex(curFc.group, curFc.grade);

    {
        menuLevel = MENU_LEVEL_3;

        menuAttri[MENU_LEVEL_3].operateDigit = 0;
        menu3Number = funcCode.all[curFc.index];

        // 确定功能码显示位数
        curFcDispDigits = GetDispDigits(curFc.index);
    }
}


LOCALF void Menu2OnShift(void)
{

}


//=====================================================================
//
// menu3, 三级菜单
//
//=====================================================================
LOCALF void Menu3OnPrg(void)
{
    menuLevel = MENU_LEVEL_2;
    ticker4LowerDisp = 0;   // 菜单级别重新置为0时，ticker清零
}


LOCALF void Menu3OnEnter(void)
{

    Uint16 writable = funcCodeAttribute[curFc.index].attribute.bit.writable;


   // 正在操作EEPROM，不响应"保存功能码"，但是响应其他按键。
 /*      if (FUNCCODE_RW_MODE_NO_OPERATION != funcCodeRwMode)
       {
           if ((FACTORY_PWD_INDEX != curFc.index)        // FF-00
               && (ATTRIBUTE_READ_ONLY_ANYTIME != writable)    // (任何时候都)只读的功能码
               )
           {
               return;
           }
       }*/

   // 对于运行中只读的功能码，在停机时修改了(但未按ENTER)，再运行，按ENTER，不响应。
   // 对于任何时候都只读的功能码，不要这么处理。
       if ((runFlag.bit.run)      // 运行
           && (ATTRIBUTE_READ_ONLY_WHEN_RUN == writable) // 运行时只读
           && (funcCode.all[curFc.index] != menu3Number) // 值被改变
           )
       {
           return;
       }

       else if (ATTRIBUTE_READ_ONLY_ANYTIME != writable) // 非 任何时候都只读 的功能码
       // 除上面几个功能码之外，修改的值都需要保存
       {
   // 某些互斥的功能码。ModifyFunccodeEnter()中会对funcCode赋值。
   // 须放在 功率改变处理 和 机型改变处理 后面。
          //ModifyFunccodeEnter(curFc.index, menu3Number);
         //      return;

           funcCode.all[curFc.index] = menu3Number; // RAM
         //  SaveOneFuncCode(curFc.index);  // 这时不用考虑与EEPROM中的值是否相同。
       }
       {
           menuLevel = MENU_LEVEL_2;
           menuAttri[MENU_LEVEL_2].operateDigit = 0;
       }
/*      Uint16 dataOld;

      dataOld = funcCode.all[index];
      funcCode.all[index] = dataNew;        // 保存到RAM


      if (dataOld != dataNew)
      {
          // 进入了某些功能码，还要修改一些变量
          // F0-08, 预置频率
          if ((GetCodeIndex(funcCode.code.presetFrq) == index) )
          {
              // 数字设定频率源，修改了预置频率，要相应修改设定频率。进入预置频率后enter就修改
           //   ResetUpDownFrq();
          }

      }

      if ((GetCodeIndex(funcCode.code.ledDispParaRun1) == index) ||
          (GetCodeIndex(funcCode.code.ledDispParaRun2) == index) ||
          (GetCodeIndex(funcCode.code.ledDispParaStop) == index)
          )
      {
   //       cycleShiftDeal(0);      // 0级菜单显示的循环移位处理
      }
      // 频率指令小数点
      else if (GetCodeIndex(funcCode.code.frqPoint) == index)
      {
          if (funcCode.code.maxFrq < 50 * decNumber[funcCode.code.frqPoint])
          {
              funcCode.code.maxFrq = 50 * decNumber[funcCode.code.frqPoint];

              // 某些功能码是其他功能码上下限的处理
          //    LimitOtherCodeDeal(MAX_FRQ_INDEX);   // 最大频率
          }
      }
      else
      {
    //      LimitOtherCodeDeal(index);      // 某些功能码是其他功能码上下限的处理
      }
*/
}



// 确认调谐命令是否有效
Uint16 ValidateTuneCmd(Uint16 value, Uint16 motorsN)
{
  return 0;
}



LOCALF void Menu3OnUpDown(Uint16 flag)
{
    int16 delta;
    Uint16 flag1 = 1;

    if (curFc.group >= FUNCCODE_GROUP_U0) // U组，显示
    {   // U组，3级菜单下，UP/DOWN自动到next grade，同时需要更新currentIndex
        Menu2OnUpDown(flag);
        curFc.index = GetGradeIndex(curFc.group, curFc.grade);

        flag1 = 0;
    }

    if (flag1)  // 真的需要UP/DOWN
    {
        // 十进制约束
        delta = decNumber[menuAttri[MENU_LEVEL_3].operateDigit];
        if (ON_DOWN_KEY == flag)
            delta = -delta;

        ModifyFunccodeUpDown(curFc.index, &menu3Number, delta);
    }
}


LOCALF void Menu3OnShift(void)
{
   Uint16 max = curFcDispDigits;

  if (!menuAttri[MENU_LEVEL_3].operateDigit)
      menuAttri[MENU_LEVEL_3].operateDigit = max - 1;
  else
      menuAttri[MENU_LEVEL_3].operateDigit--;
}


//=====================================================================
//
// 某些功能码的设定不能相同。目前280有DI端子，320还有主辅频率源
//
// 输入：
// index        -- 需要进行处理功能码的index
// funcIndex[]  -- 这些设定不能相同功能码的index数组
// number       -- funcIndex[]的长度
// data         -- 当前功能码，想设定的值
// upper        -- 功能码上限
// lower        -- 功能码下限
// delta        -- 功能码值当前可以增加的delta。正数--UP, 负数--DOWN，0--仅判断是否与其他功能码设定相同
//
// 返回:
//      delta不为0时，返回与其他功能码设定不同的数据。
//      delta为0时，相同则返回0，否则返回输入值。
//
//=====================================================================
Uint16 NoSameDeal(Uint16 index, const Uint16 funcIndex[], int16 number, int16 data, int16 upper, int16 lower, int16 delta)
{
  return 0;
}



// 十进制
void GetNumberDigit1(Uint16 digit[5], Uint16 number)
{
    Uint16 tmp;

    tmp = (Uint32)number * 52429 >> 19; // 52428.8 = 1/10*2^19
    digit[0] = number - tmp * 10;
    number = tmp;

    tmp = (Uint32)number * 52429 >> 19;
    digit[1] = number - tmp * 10;
    number = tmp;

    tmp = (Uint32)number * 52429 >> 19;
    digit[2] = number - tmp * 10;
    number = tmp;

    tmp = (Uint32)number * 52429 >> 19;
    digit[3] = number - tmp * 10;
    number = tmp;

    tmp = (Uint32)number * 52429 >> 19;
    digit[4] = number - tmp * 10;
    number = tmp;
}


// 十六进制
void GetNumberDigit2(Uint16 digit[5], Uint16 number)
{
    Uint16 tmp;

    tmp = number >> 4;
    digit[0] = number - (tmp << 4);
    number = tmp;

    tmp = number >> 4;
    digit[1] = number - (tmp << 4);
    number = tmp;

    tmp = number >> 4;
    digit[2] = number - (tmp << 4);
    number = tmp;

    tmp = number >> 4;
    digit[3] = number - (tmp << 4);
    number = tmp;

    // 赋值0,防止产生随机值
    digit[4] = 0;
}


// 获取16位数的每一位
// mode: 0--十进制, 1--十六进制
// 结果放在 digit[]中
// 返回该数字的位数
Uint16 GetNumberDigit(Uint16 digit[5], Uint16 number, Uint16 mode)
{
    if (!mode)          // 十进制
    {
        GetNumberDigit1(digit, number);
    }
    else                // 十六进制
    {
        GetNumberDigit2(digit, number);
    }

    if (digit[4])
            return 5;
    else if (digit[3])
        return 4;
    else if (digit[2])
        return 3;
    else if (digit[1])
        return 2;
    else //if (digit[0])
        return 1;
}


//=====================================================================
//
// 0级菜单下的显示缓冲更新函数, 12ms调用1次
// 运行/停机时LED显示，正在修改设定频率时，不显示数据最前面的0
// 显示的闪烁，暂时不使用滤波
//
//=====================================================================
LOCALF void UpdateMenu0DisplayBuffer(void)
{
    Uint16 menu0Number;             // 0级菜单显示的值
    union FUNC_ATTRIBUTE attributeMenu0;

    Uint16 bitDisp;

        if (runFlag.bit.run) // 运行时LED显示
        {
            bitDisp = funcCode.code.dispParaRunBit;
        }
        else                      // 停机时LED显示
        {
            if (funcCode.code.dispParaStopBit >= STOP_DISPLAY_NUM)
            {
                funcCode.code.dispParaStopBit = 0;
            }
            bitDisp = stopDispIndex[funcCode.code.dispParaStopBit];

            menuAttri[MENU_LEVEL_0].winkFlag = 0xf8; // 0级菜单下停机时全闪烁
        }

        attributeMenu0 = dispAttributeU0[bitDisp];

        // 运行频率、设定频率显示，小数点
/*        if (DISP_FRQ_RUN == bitDisp)
        {
            attributeMenu0.bit.point = funcCode.code.frqPoint;
        }
        // 小数点
        else if (DISP_OUT_CURRENT == bitDisp)  // 电压显示
        {
        //    if (invPara.type > invPara.pointLimit)
        //        attributeMenu0.bit.point--;
            attributeMenu0.bit.point =
        }
        else if (DISP_LOAD_SPEED == bitDisp) // 负载速度显示
        {
            attributeMenu0.bit.point = funcCode.code.speedDispPointPos; // 速度显示小数点位
        }
*/
        menu0Number = funcCode.code.u0[bitDisp];


        UpdateDisplayBufferAttribute(menu0Number, attributeMenu0);

  /*  else if (MENU0_DISP_STATUS_ERROR == menu0DispStatus)    // 故障/告警显示
    {
        UpdateErrorDisplayBuffer();
    }
    else if (MENU0_DISP_STATUS_TUNE == menu0DispStatus)     // 调谐显示
    {
        UpdateTuneDisplayBuffer();
    }*/
}


// 更新0级菜单显示状态
void UpdateMenu0DispStatus(void)
{
   //static Uint16 errorCodeOld4Menu0;
   // static Uint16 tuneCmdOld4Menu0;

}



//=====================================================================
//
// 根据currentGroup，更新显示的组，即groupDisplay
// F0,…,FE,FF,FP,A0,…,AF,B0,…,BF,C0,…,CF,
//
//=====================================================================
void UpdateGroupDisplay(Uint16 group)
{
    Uint16 currentGroupDispF;
    Uint16 currentGroupDisp0;

    currentGroupDispF = DISPLAY_F;
    currentGroupDisp0 = menu_group[group];

    groupDisplay.dispF = currentGroupDispF;
    groupDisplay.disp0 = currentGroupDisp0;


}
//=====================================================================
//
// 1级菜单下的显示缓冲更新函数
//
//=====================================================================
LOCALF void UpdateMenu1DisplayBuffer(void) // 1级菜单显示：@@@FX
{
    UpdateGroupDisplay(curFc.group);
    displayBuffer[0] = DISPLAY_CODE[DISPLAY_NULL];
    displayBuffer[1] = DISPLAY_CODE[DISPLAY_NULL];
    displayBuffer[2] = DISPLAY_CODE[DISPLAY_NULL];
    displayBuffer[3] = DISPLAY_CODE[groupDisplay.dispF];
    displayBuffer[4] = DISPLAY_CODE[groupDisplay.disp0];
}


//=====================================================================
//
// 2级菜单下的显示缓冲更新函数
//
//=====================================================================
LOCALF void UpdateMenu2DisplayBuffer(void) // 显示 FX-XX
{
    Uint16 digit[5];

    UpdateGroupDisplay(curFc.group);


    GetNumberDigit(digit, *(grade_adrress[curFc.group]+curFc.grade), DECIMAL);

    // 数码管显示
    displayBuffer[0] = DISPLAY_CODE[groupDisplay.dispF];
    displayBuffer[1] = DISPLAY_CODE[groupDisplay.disp0];
    displayBuffer[2] = DISPLAY_CODE[DISPLAY_LINE];
    displayBuffer[3] = DISPLAY_CODE[digit[1]];
    displayBuffer[4] = DISPLAY_CODE[digit[0]];

    menuAttri[MENU_LEVEL_2].winkFlag = 0x01 << (3 + menuAttri[MENU_LEVEL_2].operateDigit);
}


//=====================================================================
//
// 3级菜单下的显示缓冲更新函数
// 更新displayBuffer[]
//
//=====================================================================
LOCALF void UpdateMenu3DisplayBuffer(void)
{
    union FUNC_ATTRIBUTE attribute;
                  Uint16 tmp = menu3Number;

    attribute = funcCodeAttribute[curFc.index].attribute;


   if (FUNCCODE_GROUP_U0 == curFc.group)               // U0，显示
   {
       attribute = dispAttributeU0[curFc.grade];
       //funcCode.all[curFc.index] = *pDispValueU0[curFc.grade]; // UpdateU0Data()中更新
   }

   if (IsWritable(attribute))             // 当前可修改
   {
       menuAttri[MENU_LEVEL_3].winkFlag = 0x01 << (3 + menuAttri[MENU_LEVEL_3].operateDigit);
   }
   else                                    // 当前不可修改
   {
       tmp = funcCode.all[curFc.index];    // 随时更新显示为功能码的值
   }


    // 负载速度显示
/*   if(GetCodeIndex(funcCode.group.u0[DISP_LOAD_SPEED]) == curFc.index)
    {
     attribute.bit.point = funcCode.code.speedDispPointPos;
    }
*/
    UpdateDisplayBufferAttribute(tmp, attribute);
}


//=====================================================================
//
// 根据属性，更新显示数据缓冲的：负号、位数、小数点和单位
//
//=====================================================================
LOCALF void UpdateDisplayBufferAttribute(const Uint16 data, union FUNC_ATTRIBUTE attribute)
{
    static Uint16 digit[5];
       int16 i;
       Uint16 digits;              // data的位数
       Uint16 bMinus = 0;          // 是否显示符号标志，1-要显示负号
       Uint16 dataTmp;
       Uint16 a,b;
       Uint16 mode=0;

   // 1. 显示位数和显示负号
       // 有符号，且为负数，需要显示负号-
       if ((attribute.bit.signal) // 符号，unsignal->0; signal->1.
           && ((int16)(data) < 0))
       {
           dataTmp = -(int16)(data);
           bMinus = 1;

            // 为有符号数据且值为负且显示值超过4位
           if ((attribute.bit.point)
               && ((int16)data < (-9999))
               )
           {
               attribute.bit.point--;
               dataTmp = dataTmp/10;
           }
       }
       else
       {
           dataTmp = data;
       }

   // 获取每一位的显示值
       a = digit[1];   // 保存
       b = digit[0];

       mode = DECIMAL;
       if (ATTRIBUTE_MULTI_LIMIT_HEX == attribute.bit.multiLimit)  // 十六进制约束
       {
           mode = HEX;
       }
       digits = GetNumberDigit(digit, dataTmp, mode);

       if (ticker4LowerDisp)  // 非0级菜单，或者0级菜单下最后2位到了更新时间，或者0级菜单  按键shift
       {
           digit[1] = a;
           digit[0] = b;
       }

     if ((MENU_LEVEL_0 == menuLevel) // 0级菜单，不显示数字前面的零
       //    || ((MENU_LEVEL_3 == menuLevel))// && (curFc.group >= FUNCCODE_GROUP_U0)) // U组，显示
              )
       {
           if (mode == HEX)
           {
               if (attribute.bit.displayBits < DISPLAY_8LED_NUM)
               {
                   // 十六进制显示加前缀 H.
                   digits = attribute.bit.displayBits + 1;
                   digit[attribute.bit.displayBits] = DISPLAY_H;    // 显示H
                   attribute.bit.point = attribute.bit.displayBits; // 显示H.
               }
           }

           if (++ticker4LowerDisp >= DECIMAL_DISPLAY_UPDATE_TIME)  // 小数点后2位更新时间，_*12ms
           {
               ticker4LowerDisp = 0;
           }
       }
       else //if (MENU_LEVEL_3 == menuLevel)    // 0，3级菜单才会调用本函数
       {
           ticker4LowerDisp = 0;

           // 十六进制数据
           if (mode == HEX)
           {
               // 有空余数码管
               if (curFcDispDigits < DISPLAY_8LED_NUM)
               {
                   // 3级菜单的十六进制数据显示加前缀 H.
                   digits = curFcDispDigits + 1;          // 增加一位显示位
                   digit[curFcDispDigits] = DISPLAY_H;    // 显示数据为H
                   attribute.bit.point = curFcDispDigits; // 显示数据为H.
               }
               // 数码管显示已无空余，不显示H.
               else
               {
                   digits = curFcDispDigits;       // 3级菜单
               }
           }
           // 十进制数据
           else
           {
               digits = curFcDispDigits;          // 3级菜单
           }
       }

       if (attribute.bit.point >= digits)  // 至少显示到小数点位置
       {
           digits = attribute.bit.point + 1;
       }

       for (i = DISPLAY_8LED_NUM - 1; i >= 0; i--) // 显示位数
       {
           if (i < digits)
           {
               displayBuffer[(DISPLAY_8LED_NUM - 1) - i] = DISPLAY_CODE[digit[i]];
           }
           else
           {
               displayBuffer[(DISPLAY_8LED_NUM - 1) - i] = DISPLAY_CODE[DISPLAY_NULL];
           }
       }

       if (bMinus)                 // 显示负号-
       {
           int16 tmp = DISPLAY_8LED_NUM - 1 - digits;
           if (tmp < 0)
               tmp = 0;

           if (!digit[4])  // 最高位为0
           {
               displayBuffer[tmp] = DISPLAY_CODE[DISPLAY_LINE];
           }
           else
           {
               ///displayBuffer[(DISPLAY_8LED_NUM - 1)] &= DISPLAY_CODE[DISPLAY_DOT];// 最后1位LED显示小数点
               //displayBuffer[tmp] = DISPLAY_CODE[DISPLAY_LINE_1];
               //displayBuffer[tmp] &= 0xdf;
               //displayBuffer[tmp] ^= 0x40;
               //displayBuffer[tmp] ^= ~0xBf;
               //displayBuffer[tmp] ^= 0x02;
               attribute.bit.point = 0;
           }
       }


   // 2. 显示小数点
       if (attribute.bit.point)
       {
           displayBuffer[(DISPLAY_8LED_NUM - 1) - attribute.bit.point] &= DISPLAY_CODE[DISPLAY_DOT];
       }

   // 3. 显示单位
       if (attribute.bit.unit & (0x01U << ATTRIBUTE_UNIT_HZ_BIT))   // 显示单位,Hz
       {
           displayBuffer[5] &= LED_CODE[LED_HZ];   // 亮
       }

       if (attribute.bit.unit & (0x01U << ATTRIBUTE_UNIT_A_BIT))    // 显示单位,A
       {
           displayBuffer[5] &= LED_CODE[LED_A];
       }

       if (attribute.bit.unit & (0x01U << ATTRIBUTE_UNIT_V_BIT))    // 显示单位,V
       {
           displayBuffer[5] &= LED_CODE[LED_V];
       }
}


//=====================================================================
//
// 故障码显示
//
//=====================================================================
void UpdateErrorDisplayBuffer(void)
{

}


//=====================================================================
//
// 调谐显示
//
//=====================================================================
LOCALF void UpdateTuneDisplayBuffer(void)
{
    // 数码管显示
    displayBuffer[0] = DISPLAY_CODE[DISPLAY_NULL];
    displayBuffer[1] = DISPLAY_CODE[DISPLAY_T];
    displayBuffer[2] = DISPLAY_CODE[DISPLAY_U];
    displayBuffer[3] = DISPLAY_CODE[DISPLAY_N];
    displayBuffer[4] = DISPLAY_CODE[DISPLAY_E];
}


//=====================================================================
//
// 输入：
// *data  要修改的数据的当前值
//
// 输出：
// *data  修改之后的数据
//
// 参数：
// index  修改功能码的index
// delta  增量。正数，UP；负数，DOWN；0，通讯调用。
//
// 返回：
// COMM_ERR_NONE
// COMM_ERR_PARA       无效参数
// COMM_ERR_READ_ONLY  参数更改无效
//
//=====================================================================
Uint16 ModifyFunccodeUpDown(Uint16 index, Uint16 *data, int16 delta)
{

    union FUNC_ATTRIBUTE attribute = funcCodeAttribute[index].attribute;
    Uint16 upper;
    Uint16 lower;
  //  Uint16 tmp = *data;
  //  Uint16 flag = COMM_ERR_NONE;
   // int16 i;
  //  int16 flag1 = 0;
  //  Uint16 multiLimit;

// 参数读写特性，0x-参数只读，10-运行中只读，11-可以读写
    if (IsWritable(attribute))
    {
        upper = funcCodeAttribute[index].upper;
        if (attribute.bit.upperLimit)
            upper = funcCode.all[upper];

        // S曲线的起始段和结束段之和最大为1000.
        // sCurveStartPhaseTime + sCurveEndPhaseTime <= 100.0%
        if (index == GetCodeIndex(funcCode.code.sCurveStartPhaseTime))
        {
            upper = 1000 - funcCode.code.sCurveEndPhaseTime;
        }
        else if (index == GetCodeIndex(funcCode.code.sCurveEndPhaseTime))
        {
            upper = 1000 - funcCode.code.sCurveStartPhaseTime;
        }

        lower = funcCodeAttribute[index].lower;
        if (attribute.bit.lowerLimit)
            lower = funcCode.all[lower];



        *data = LimitDeal(attribute.bit.signal, *data, upper, lower, delta);


        // 最大频率，不同频率指令小数点的范围应该不同。
        if (GetCodeIndex(funcCode.code.maxFrq) == index)
        {
            lower = 50 * decNumber[funcCode.code.frqPoint];
        }
    }
    return 0;
}




//=====================================================================
//
// 参数：
// index  要修改功能码的index
// *data  数据
//
// 返回：
// COMM_ERR_NONE
// COMM_ERR_PARA       无效参数
//
//=====================================================================
Uint16 ModifyFunccodeEnter(Uint16 index, Uint16 dataNew)
{
  return 0;
}


//=====================================================================
//
// 显示数据处理，目前2ms调用1次
//
//=====================================================================
#define OUT_TORQUE_FRQ_DISP_FILTER_TIME 75  // 输出频率滤波时间系数
//LOCALF LowPassFilter torQueFrqDispLpf = LPF_DEFALUTS;
void DispDataDeal(void)
{

}




// 0级菜单下增加菜单级别
void Menu0AddMenuLevel(void)
{

}


//=====================================================================
//
// 确定功能码的显示位数
//
// 实际显示位数，应该为:
// 1. 无符号
//    上限数值的位数
// 2. 有符号
//    上限和下限数值的绝对值的大值的位数
//
//=====================================================================
Uint16 GetDispDigits(Uint16 index)
{
    Uint16 upper;
    Uint16 lower;
    Uint16 value;
    Uint16 digits;
    Uint16 mode;
    Uint16 digit[5];

    const FUNCCODE_ATTRIBUTE *p = &funcCodeAttribute[index];

// 获得实际的上限

// 获取上限数值
    while (p->attribute.bit.upperLimit)
    {
        p = &funcCodeAttribute[p->upper];
    }
    upper = p->upper;

    if (!p->attribute.bit.signal)   // 无符号
    {
        value = upper;
    }
    else                            // 有符号
    {
        // 获取下限数值
        while (p->attribute.bit.lowerLimit)
        {
            p = &funcCodeAttribute[p->lower];
        }
        lower = p->lower;

        // 取绝对值
        upper = ABS_INT16((int16)upper);
        lower = ABS_INT16((int16)lower);

        // 取绝对值的大值
        value = (upper > lower) ? upper : lower;
    }

// 获取上限的位数
    mode = DECIMAL;

    if (ATTRIBUTE_MULTI_LIMIT_HEX == p->attribute.bit.multiLimit)   // 十六进制约束
    {
        mode = HEX;
    }

    digits = GetNumberDigit(digit, value, mode);

    return digits;
}


// group的UP/DOWN
Uint16 GroupUpDown(const Uint16 funcCodeGrade[], Uint16 group, Uint16 flag)
{
    group = LimitOverTurnDeal(funcCodeGrade, group, FUNCCODE_GROUP_NUM, FUNCCODE_GROUP_F0, flag);
     return group;
}


// 用户定制功能码菜单模式的处理
void DealUserMenuModeGroupGrade(Uint16 flag)
{

}


// data的范围是 [low, upper-1]
Uint16 LimitOverTurnDeal(const Uint16 limit[], Uint16 data, Uint16 upper, Uint16 low, Uint16 flag)
{
    Uint16 loopNumber = 0;

   do
   {
       data += deltaK[flag];

       if ((int16)data >= (int16)upper)
       {
           data = low;
       }
       else if ((int16)data < (int16)low)
       {
           data = upper - 1;
       }

       loopNumber++;
   }
   while ((!limit[data])
       && (loopNumber < upper - low)
       );

   return data;
}


// 校验菜单
#define CHECK_MENU_MODE_NUMBER_ONCE     70      // 1拍内的循环次数
void DealCheckMenuModeGroupGrade(Uint16 flag)
{

}



// 菜单模式处理
// 更新funcCodeGradeCurrentMenuMode
void MenuModeDeal(void)
{

}


void UpdataFuncCodeGrade(Uint16 funcCodeGrade[])
{

}


//====================================================================
// 按位(个位、十位、百位、千位、万位)
// 每位仅能为0, 1
// 例如，功能码A6-06(虚拟VDI端子功能码设定有效状态)，
// 功能码显示为 11101，转换为 29。
//====================================================================
Uint16 FcDigit2Bin(Uint16 value)
{
  return 0;
}

//====================================================================
//
// 性能调试的功能码组CF、UF组的个数处理
//
//====================================================================
void MotorDebugFcDeal(void)
{

}


//====================================================================
//
// 上电时的菜单初始化
//
//====================================================================
void MenuInit(void)
{
    menuAttri[MENU_LEVEL_0].winkFlag = 0x00F8;
    // 默认会选择第一级索引
  //  menuModeTmp = MENU_MODE_BASE;
 //   menuMode = menuModeTmp;
    MenuModeDeal();
 //   menuLevel = MENU_LEVEL_0;
}


LOCALD void MenuPwdOnPrg(void){}
LOCALD void MenuPwdHint2Input(void){}
LOCALD void MenuPwdInputOnEnter(void){}
LOCALD void MenuPwdInputOnUp(void){}
LOCALD void MenuPwdInputOnDown(void){};
LOCALD void MenuPwdHintOnShift(void){};
LOCALD void MenuPwdInputOnShift(void){}
LOCALD void UpdateMenuPwdHintDisplayBuffer(void){}
LOCALD void UpdateMenuPwdInputDisplayBuffer(void){}


LOCALD void MenuPwdHintOnQuick(void)
{
}
LOCALD void MenuPwdInputOnQuick(void)
{
}

