/*********************************************************************
 * initCpuBase.c
 *
 *  Created on:       2020年3月12日
 *      Author:       dfs_hb
 * Description:       初始化CPU
 *     History:       VER:1.0
 *
 *********************************************************************/

#include"InitCpuBase.h"
#include"f_data_enterchange.h"
#include "f_SFRA.h"
#include "Flash.h"
#include "motor_protect.h"

void InitSysXINT(void);
void InitSysINTEnable(void);
void InitSysGpio(void);

interrupt void xint1_isr(void);
interrupt void xint2_isr(void);
interrupt void xint3_isr(void);
interrupt void xint4_isr(void);
interrupt void xint5_isr(void);

extern Uint16 keyFunc;

Uint16 inv_shot_flag=0;
Uint16 ref_shot_flag=0;

/*void  IGBTFaultISR(void)//gpio isr
{

}*/

/*******************************************************************************************
Function Name:   ConfigureHardware
Version:         V1.0
Input:
Output:
Return:
Description:    初始化硬件配置
History:
*******************************************************************************************/
void ConfigureHardware(void)
{

    /*初始化硬件*/
    InitCpuBase();

    InitEPWM();
    InitPFCEPWM();

    Init_pfc(PhaseTypeRSTPWMCmp,pfc_ADCSample);
    Initmotor_Ptr(MtrCtrl);
    InitPFC_Ptr(pfc_controlfunction);

    InitAdc();

    spi_fifo_init();

    eMBInit( MB_RTU, 0x01, 1, 9600, MB_PAR_NONE);
    eMBEnable();

    Systerm_TickInit();

  //  CLA_configClaMemory();
 //   CLA_initCpu1Cla1();

    InitSysXINT();
    InitSysINTEnable();
 //   Flash_Init();

}

/*******************************************************************************************
Function Name:  SysInit
Version:         V1.0
Input:
Output:
Return:
Description:     系统初始化
History:
*******************************************************************************************/
void SysInit(void)
{
    /*关全局中断*/
    DisGlobalIRQ();

    /* 硬件配置 */
    ConfigureHardware();


    /*开全局中断 */
    EneGlobalIRQ();
}



/*******************************************************************************************
Function Name:   InitCpuBase
Version:         V1.0
Input:
Output:
Return:
Description:     初始化CPU
History:
*******************************************************************************************/
void InitCpuBase(void)
{
    InitSysCtrl();

    DINT;

    InitPieCtrl();

    IER = 0x0000;
    IFR = 0x0000;

    InitPieVectTable();

    /* init gpio */
    InitSysGpio();

}



/*******************************************************************************************
Function Name:   EneGlobalIRQ
Version:         V1.0
Input:
Output:
Return:
Description:    全局中断允许
History:
*******************************************************************************************/
void EneGlobalIRQ(void)
{
    EINT;
}


/*******************************************************************************************
Function Name:   DisGlobalIRQ
Version:         V1.0
Input:
Output:
Return:
Description:    全局中断关闭
History:
*******************************************************************************************/
void DisGlobalIRQ(void)
{
    DINT;
}



/*******************************************************************************************
Function Name:   InitSysGpio
Version:         V1.0
Input:
Output:
Return:
Description:    初始化IO
History:
*******************************************************************************************/
void InitSysGpio(void)
{
    EALLOW;


     GPIO_SetupPinMux(94,0,0);// set 94 pin to gpio          //PWM使能信号
     GPIO_SetupPinOptions(94,1,0);//set 94 pin to output

     GPIO_SetupPinOptions(93, GPIO_OUTPUT, GPIO_PUSHPULL);   //PFC PWM使能信号
     GPIO_SetupPinMux(93, GPIO_MUX_CPU1, 0);

     GPIO_SetupPinMux(68,0,0);// set 70 pin to gpio         //PWM锁存复位信号
     GPIO_SetupPinOptions(68,1,0);//set 70 pin to output

    // GPIO_SetupPinMux(19,0,0);// set 70 pin to gpio      //
    // GPIO_SetupPinOptions(19,0,0);//set 70 pin to output

     GPIO_SetupPinOptions(66, GPIO_OUTPUT, GPIO_PUSHPULL); //变频器输出故障反馈信号
     GPIO_SetupPinMux(66, GPIO_MUX_CPU1, 0);
     GPIO_SetupPinOptions(64, GPIO_OUTPUT, GPIO_PUSHPULL); //变频器输出故障反馈信号
     GPIO_SetupPinMux(64, GPIO_MUX_CPU1, 0);
     GPIO_SetupPinOptions(76, GPIO_OUTPUT, GPIO_PUSHPULL); //整流器快速故障复位信号
     GPIO_SetupPinMux(76, GPIO_MUX_CPU1, 0);
     GPIO_SetupPinOptions(69, GPIO_OUTPUT, GPIO_PUSHPULL); //变频器快速故障复位信号
     GPIO_SetupPinMux(69, GPIO_MUX_CPU1, 0);

     GPIO_SetupPinOptions(72, GPIO_OUTPUT, GPIO_PUSHPULL); //变频器快速故障复位信号
     GPIO_SetupPinMux(72, GPIO_MUX_CPU1, 0);


     GPIO_SetupPinOptions(20, GPIO_OUTPUT, GPIO_PUSHPULL); //预充电继电器
     GPIO_SetupPinMux(20, GPIO_MUX_CPU1, 0);

     GPIO_SetupPinOptions(24, GPIO_OUTPUT, GPIO_PUSHPULL); //预充电继电器
     GPIO_SetupPinMux(24, GPIO_MUX_CPU1, 0);

     GPIO_SetupPinOptions(25, GPIO_OUTPUT, GPIO_PUSHPULL); //预充电继电器
     GPIO_SetupPinMux(25, GPIO_MUX_CPU1, 0);

     GPIO_SetupPinOptions(71, GPIO_OUTPUT, GPIO_PUSHPULL); //辅助供电
     GPIO_SetupPinMux(71, GPIO_MUX_CPU1, 0);

     GPIO_SetupPinOptions(44, GPIO_INPUT, GPIO_PULLUP);   //变频器输入启动信号
     GPIO_SetupPinMux(44, GPIO_MUX_CPU1, 0);

     GPIO_SetupPinOptions(44, GPIO_INPUT, GPIO_PULLUP);   //变频器输入启动信号
     GPIO_SetupPinMux(44, GPIO_MUX_CPU1, 0);

     GPIO_SetupPinOptions(44, GPIO_INPUT, GPIO_PULLUP);   //变频器输入启动信号
     GPIO_SetupPinMux(44, GPIO_MUX_CPU1, 0);

     GPIO_SetupPinOptions(75, GPIO_INPUT, GPIO_PULLUP);   //变频器快速故障信号
     GPIO_SetupPinMux(75, GPIO_MUX_CPU1, 0);

     GPIO_SetupPinOptions(74, GPIO_INPUT, GPIO_PULLUP);   //变频器快速故障信号
     GPIO_SetupPinMux(74, GPIO_MUX_CPU1, 0);

     GPIO_SetupPinOptions(22, GPIO_INPUT, GPIO_PULLUP);   //变频器快速故障信号
     GPIO_SetupPinMux(22, GPIO_MUX_CPU1, 0);

     GPIO_SetupPinOptions(23, GPIO_INPUT, GPIO_PULLUP);   //变频器快速故障信号
     GPIO_SetupPinMux(23, GPIO_MUX_CPU1, 0);

     EDIS;
}



void InitSysTimers(void)
{

    // init cpu timer0
    InitCpuTimers();
    ConfigCpuTimer(&CpuTimer0, 200, 1000);
    CpuTimer0Regs.TCR.all = 0x4001;

}

//void InitSysWotchDog(void)
//{
//
//    // init wotch dog block
//    EALLOW;
//    //WdRegs.SCSR.all = BIT1;// enable watchdog isr
//    WdRegs.SCSR.all = 0;//enable servicedog mode
//    EDIS;
//
//}

extern  void GPIO_SetupXINT1Gpio(Uint16 pin);
extern  void GPIO_SetupXINT2Gpio(Uint16 pin);
extern  void GPIO_SetupXINT3Gpio(Uint16 pin);
extern  void GPIO_SetupXINT4Gpio(Uint16 pin);
extern  void GPIO_SetupXINT5Gpio(Uint16 pin);

void InitSysXINT(void)
{

    EALLOW;
    //    GpioCtrlRegs.GPBMUX1.bit.GPIO44 = 0;  // All GPIO
    //    GpioCtrlRegs.GPBDIR.bit.GPIO44 = 1;   // All outputs

    PieVectTable.XINT1_INT = &xint1_isr;
    PieVectTable.XINT2_INT = &xint2_isr;
    PieVectTable.XINT3_INT = &xint3_isr;
    PieVectTable.XINT4_INT = &xint4_isr;
    PieVectTable.XINT5_INT = &xint5_isr;

    PieCtrlRegs.PIECTRL.bit.ENPIE = 1;
    PieCtrlRegs.PIEIER1.bit.INTx4 = 1;    //XINT1
    PieCtrlRegs.PIEIER1.bit.INTx5 = 1;    //XINT2
    PieCtrlRegs.PIEIER12.bit.INTx1 = 1;   //XINT3
    PieCtrlRegs.PIEIER12.bit.INTx2 = 1;   //XINT4
    PieCtrlRegs.PIEIER12.bit.INTx3 = 1;   //XINT5

    GpioCtrlRegs.GPAMUX2.bit.GPIO19 = 0;
    GpioCtrlRegs.GPADIR.bit.GPIO19 = 0;
    GpioCtrlRegs.GPAQSEL2.bit.GPIO19= 2;

    GpioCtrlRegs.GPAMUX2.bit.GPIO22 = 0;
    GpioCtrlRegs.GPADIR.bit.GPIO22 = 0;
    GpioCtrlRegs.GPAQSEL2.bit.GPIO22= 2;


    GpioCtrlRegs.GPAMUX2.bit.GPIO23 = 0;
    GpioCtrlRegs.GPADIR.bit.GPIO23 = 0;
    GpioCtrlRegs.GPAQSEL2.bit.GPIO23= 2;
    GpioCtrlRegs.GPACTRL.bit.QUALPRD2 = 0X08;

    GpioCtrlRegs.GPCMUX1.bit.GPIO74 = 0;
    GpioCtrlRegs.GPCDIR.bit.GPIO74= 0;
    GpioCtrlRegs.GPCQSEL1.bit.GPIO74= 2;


    GpioCtrlRegs.GPCMUX1.bit.GPIO75 = 0;
    GpioCtrlRegs.GPCDIR.bit.GPIO75= 0;
    GpioCtrlRegs.GPCQSEL1.bit.GPIO75= 2;
    GpioCtrlRegs.GPCCTRL.bit.QUALPRD1 = 0X08;

    GPIO_SetupXINT1Gpio(22);
    GPIO_SetupXINT2Gpio(23);
    GPIO_SetupXINT3Gpio(74);
    GPIO_SetupXINT4Gpio(75);
    GPIO_SetupXINT5Gpio(19);

    XintRegs.XINT1CR.bit.POLARITY  = 0;
    XintRegs.XINT2CR.bit.POLARITY  = 0;
    XintRegs.XINT3CR.bit.POLARITY  = 0;
    XintRegs.XINT4CR.bit.POLARITY  = 0;
    XintRegs.XINT5CR.bit.POLARITY  = 0;

    XintRegs.XINT1CR.bit.ENABLE = 1;
    XintRegs.XINT2CR.bit.ENABLE = 1;
    XintRegs.XINT3CR.bit.ENABLE = 1;
    XintRegs.XINT4CR.bit.ENABLE = 1;
    XintRegs.XINT5CR.bit.ENABLE = 1;
    EDIS;
}


void spi_init()
{
    SpiaRegs.SPICCR.all =0x000F;                 // Reset on, rising edge, 16-bit char bits
    SpiaRegs.SPICTL.all =0x0006;                 // Enable master mode, normal phase,
                                                 // enable talk, and SPI int disabled.
    SpiaRegs.SPIBRR.all =0x007F;
    SpiaRegs.SPICCR.all =0x009F;                 // Relinquish SPI from Reset
    SpiaRegs.SPIPRI.bit.FREE = 1;                // Set so breakpoints don't disturb xmission
}



void InitSysSpi(void)
{
    spi_fifo_init();   // Initialize the Spi FIFO
    spi_init();       // init SPI
}

Uint16 spi_xmit(Uint16 a)
{
    Uint16 rdata = 0;

    // Transmit data
    SpiaRegs.SPITXBUF=a;
    // Wait until data is received
    while(SpiaRegs.SPIFFRX.bit.RXFFST !=1) { }
    // Check against sent data
    rdata = SpiaRegs.SPIRXBUF;

    return(rdata);
}

extern union RUN_FLAG runFlag;
void InitSysINTEnable(void)
{
    /*************************************************************/
    /* init isr block */
  /*  PieCtrlRegs.PIECTRL.bit.ENPIE = 1;           // Enable the PIE block
    PieCtrlRegs.PIEIER1.bit.INTx1 = 1;           // Enable PIE Gropu 1 INT7 for Timer 0 Interrupt
    PieCtrlRegs.PIEIER1.bit.INTx9 = 1;           // Enable PIE Gropu 1 INT8 for wacth dog Interrupt
    PieCtrlRegs.PIEIER1.bit.INTx4 = 1;           // Enable PIE Gropu 1 INT4 for XINT1_INT
    PieCtrlRegs.PIEIER1.bit.INTx5 = 1;           // Enable PIE Gropu 1 INT5 for XINT2_INT
    PieCtrlRegs.PIEIER12.bit.INTx1 = 0;          // Enable PIE Gropu 12 INT1 for XINT3_INT
    PieCtrlRegs.PIEIER12.bit.INTx2 = 1;          // Enable PIE Gropu 12 INT2 for XINT4_INT
    PieCtrlRegs.PIEIER12.bit.INTx3 = 1;          // Enable PIE Gropu 12 INT3 for XINT5_INT
  */
  //  PieCtrlRegs.PIEIER1.bit.INTx4 = 1;           // Enable PIE Gropu 1 INT4 for XINT1_INT
    IER |= M_INT1;                               // Enable CPU INT1
    IER |= M_INT9;                               // Enable CPU INT12/   */
    IER |= M_INT12;
    EINT;                                        // Enable Global Interrupts
    ERTM;                                        // Enable Global realtime __interrupt DBGM
    /*************************************************************/
}


void InitSysWotchDogStart(void)
{
    /*************************************************************/
    /* start wotch dog */
    // Reset the watchdog counter
    ServiceDog();

    EALLOW;
    //WdRegs.WDCR.all = 0x0028;
    WdRegs.WDCR.all = 0x002f;
    EDIS;
    /*************************************************************/
}


interrupt void xint1_isr(void)
{
    PwmOff();
    PwmPFCOff();

    // Acknowledge this interrupt to get more from group 1
    Set_INVFault_flag();
    PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;
}

interrupt void xint2_isr(void)
{
    PwmOff();
    PwmPFCOff();
    Set_PfcOff();
    Set_INVFault_flag();

    // Acknowledge this interrupt to get more from group 1
    PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;
}


interrupt void xint3_isr(void)
{
     PwmOff();
     PwmPFCOff();
     Set_PfcOff();
     Set_PFCFault_flag();

    // Acknowledge this interrupt to get more from group 1
    PieCtrlRegs.PIEACK.all = PIEACK_GROUP12;
}

interrupt void xint4_isr(void)
{

    PwmOff();
    PwmPFCOff();
    Set_PfcOff();
    Set_PFCFault_flag();

    // Acknowledge this interrupt to get more from group 1
    PieCtrlRegs.PIEACK.all = PIEACK_GROUP12;
}


interrupt void xint5_isr(void)
{

    PwmOff();
    PwmPFCOff();
    Set_PfcOff();
    Set_SoftShot_flag();

    PieCtrlRegs.PIEACK.all = PIEACK_GROUP12;
}

