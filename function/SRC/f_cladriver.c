/*
 * CLA_Init.c
 *
 *  Created on:
 *      Author: ljl
 */

#include "f_cladriver.h"
#include "cla_divide_shared.h"

#pragma DATA_SECTION("Cla1ToCpuMsgRAM");
unsigned char shot_flag=0;

//
// CLA_configClaMemory - Configure CLA memory sections
//
void CLA_configClaMemory(void)
{
    extern uint32_t Cla1funcsRunStart, Cla1funcsLoadStart, Cla1funcsLoadSize;
    EALLOW;

#ifdef _FLASH
    //
    // Copy over code from FLASH to RAM
    //
    memcpy((uint32_t *)&Cla1funcsRunStart, (uint32_t *)&Cla1funcsLoadStart,
           (uint32_t)&Cla1funcsLoadSize);
#endif //_FLASH

    //
    // Initialize and wait for CLA1ToCPUMsgRAM
    //
    MemCfgRegs.MSGxINIT.bit.INIT_CLA1TOCPU = 1;
    while(MemCfgRegs.MSGxINITDONE.bit.INITDONE_CLA1TOCPU != 1){};

    //
    // Initialize and wait for CPUToCLA1MsgRAM
    //
    MemCfgRegs.MSGxINIT.bit.INIT_CPUTOCLA1 = 1;
    while(MemCfgRegs.MSGxINITDONE.bit.INITDONE_CPUTOCLA1 != 1){};

    //
    // Select LS5RAM to be the programming space for the CLA
    // First configure the CLA to be the master for LS5 and then
    // set the space to be a program block
    //
    MemCfgRegs.LSxMSEL.bit.MSEL_LS4 = 1;
    MemCfgRegs.LSxCLAPGM.bit.CLAPGM_LS4 = 1;
    MemCfgRegs.LSxMSEL.bit.MSEL_LS5 = 1;
    MemCfgRegs.LSxCLAPGM.bit.CLAPGM_LS5 = 1;


    //
    // Next configure LS0RAM and LS1RAM as data spaces for the CLA
    // First configure the CLA to be the master for LS0(1) and then
    // set the spaces to be code blocks
    //
    MemCfgRegs.LSxMSEL.bit.MSEL_LS0 = 1;
    MemCfgRegs.LSxCLAPGM.bit.CLAPGM_LS0 = 0;

    MemCfgRegs.LSxMSEL.bit.MSEL_LS1 = 1;
    MemCfgRegs.LSxCLAPGM.bit.CLAPGM_LS1 = 0;

    EDIS;
}



//
// CLA_initCpu1Cla1 - Initialize CLA1 task vectors and end of task interrupts
//
void CLA_initCpu1Cla1(void)
{
    //
    // Compute all CLA task vectors
    // On Type-1 CLAs the MVECT registers accept full 16-bit task addresses as
    // opposed to offsets used on older Type-0 CLAs
    //
    EALLOW;
    Cla1Regs.MVECT1 = (uint16_t)(&Cla1Task1);
    Cla1Regs.MVECT2 = (uint16_t)(&Cla1Task2);
    Cla1Regs.MVECT3 = (uint16_t)(&Cla1Task3);
    Cla1Regs.MVECT4 = (uint16_t)(&Cla1Task4);
    Cla1Regs.MVECT5 = (uint16_t)(&Cla1Task5);
    Cla1Regs.MVECT6 = (uint16_t)(&Cla1Task6);
    Cla1Regs.MVECT7 = (uint16_t)(&Cla1Task7);
    Cla1Regs.MVECT8 = (uint16_t)(&Cla1Task8);

    //
    // Enable the IACK instruction to start a task on CLA in software
    // for all  8 CLA tasks. Also, globally enable all 8 tasks (or a
    // subset of tasks) by writing to their respective bits in the
    // MIER register
    //
    Cla1Regs.MCTL.bit.IACKE = 1;
    Cla1Regs.MIER.all = 0x00FF;

    //
    // Configure the vectors for the end-of-task interrupt for all
    // 8 tasks
    //
    PieVectTable.CLA1_1_INT = &cla1Isr1;
    PieVectTable.CLA1_2_INT = &cla1Isr2;
    PieVectTable.CLA1_3_INT = &cla1Isr3;
    PieVectTable.CLA1_4_INT = &cla1Isr4;
    PieVectTable.CLA1_5_INT = &cla1Isr5;
    PieVectTable.CLA1_6_INT = &cla1Isr6;
    PieVectTable.CLA1_7_INT = &cla1Isr7;
    PieVectTable.CLA1_8_INT = &cla1Isr8;

    //
    // Enable CLA interrupts at the group and subgroup levels
    //
    DmaClaSrcSelRegs.CLA1TASKSRCSEL1.bit.TASK1 = 5;        //触发方式由ADCAEVT 触发
    DmaClaSrcSelRegs.CLA1TASKSRCSEL1.bit.TASK2 = 10;       //触发方式由ADCBEVT 触发
    DmaClaSrcSelRegs.CLA1TASKSRCSEL1.bit.TASK3 = 15;       //触发方式由ADCDEVT 触发
    //DmaClaSrcSelRegs.CLA1TASKSRCSEL1.bit.TASK4 = 32;     //触发方式由ADCAEVT 触发
    PieCtrlRegs.PIEIER11.all = 0xFFFF;
    IER |= (M_INT11 );
}


void Gpio_select(void)
{
    EALLOW;
//    GpioCtrlRegs.GPBMUX1.bit.GPIO44 = 0;  // All GPIO
//    GpioCtrlRegs.GPBDIR.bit.GPIO44 = 1;   // All outputs

    PieCtrlRegs.PIECTRL.bit.ENPIE = 1;
    PieCtrlRegs.PIEIER1.bit.INTx4 = 1;    //XINT1
    PieCtrlRegs.PIEIER1.bit.INTx5 = 1;    //XINT2
    PieCtrlRegs.PIEIER12.bit.INTx1 = 1;    //XINT3
    PieCtrlRegs.PIEIER12.bit.INTx2 = 1;    //XINT4

    GpioCtrlRegs.GPAMUX2.bit.GPIO22 = 0;
    GpioCtrlRegs.GPADIR.bit.GPIO22 = 0;
    GpioCtrlRegs.GPAQSEL2.bit.GPIO22= 2;


    GpioCtrlRegs.GPAMUX2.bit.GPIO23 = 0;
    GpioCtrlRegs.GPADIR.bit.GPIO23 = 0;
    GpioCtrlRegs.GPAQSEL2.bit.GPIO23= 2;
    GpioCtrlRegs.GPACTRL.bit.QUALPRD2 = 0X08;

    GpioCtrlRegs.GPCMUX1.bit.GPIO74 = 0;
    GpioCtrlRegs.GPCDIR.bit.GPIO74= 0;
    GpioCtrlRegs.GPCQSEL1.bit.GPIO74= 2;


    GpioCtrlRegs.GPCMUX1.bit.GPIO75 = 0;
    GpioCtrlRegs.GPCDIR.bit.GPIO75= 0;
    GpioCtrlRegs.GPCQSEL1.bit.GPIO75= 2;
    GpioCtrlRegs.GPCCTRL.bit.QUALPRD1 = 0X08;

    GPIO_SetupXINT1Gpio(22);
    GPIO_SetupXINT2Gpio(23);
    GPIO_SetupXINT3Gpio(74);
    GPIO_SetupXINT4Gpio(75);

    XintRegs.XINT1CR.bit.POLARITY  = 1;
    XintRegs.XINT2CR.bit.POLARITY  = 1;
    XintRegs.XINT3CR.bit.POLARITY  = 1;
    XintRegs.XINT4CR.bit.POLARITY  = 1;

    XintRegs.XINT1CR.bit.ENABLE = 1;
    XintRegs.XINT2CR.bit.ENABLE = 1;
    XintRegs.XINT3CR.bit.ENABLE = 1;
    XintRegs.XINT4CR.bit.ENABLE = 1;
    EDIS;
}
//
// cla1Isr1 - CLA1 ISR 1
//
__interrupt void cla1Isr1 ()
{
    //
    // Acknowledge the end-of-task interrupt for task 1
    //
    PieCtrlRegs.PIEACK.all = M_INT11;

    //
    // Uncomment to halt debugger and stop here
    //
//    asm(" ESTOP0");
}


void CLA_runTest(void)
{
//    int16_t i, error;
//
//    //
//    // Initialize the CPUToCLA1MsgRam variables here
//    //
//    PIBYTWO = 1.570796327;
//
//    for(i=0; i < BUFFER_SIZE; i++)
//    {
//        fVal = (float)((BUFFER_SIZE/2) - i);
//
//        Cla1ForceTask1andWait();
//
//        y[i] = fResult;
//        error = fabs(atan_expected[i]-y[i]);
//
//        if(error < 0.01)
//        {
//            pass++;
//        }
//        else
//        {
//            fail++;
//        }
//    }

}


//
// cla1Isr2 - CLA1 ISR 2
//
__interrupt void cla1Isr2 ()
{
    PieCtrlRegs.PIEACK.all = M_INT11;
//    asm(" ESTOP0");
}

//
// cla1Isr3 - CLA1 ISR 3
//
__interrupt void cla1Isr3 ()
{
    PieCtrlRegs.PIEACK.all = M_INT11;
//    asm(" ESTOP0");
}

//
// cla1Isr4 - CLA1 ISR 4
//
__interrupt void cla1Isr4 ()
{
    PieCtrlRegs.PIEACK.all = M_INT11;
//    asm(" ESTOP0");
}

//
// cla1Isr5 - CLA1 ISR 5
//
__interrupt void cla1Isr5 ()
{
    asm(" ESTOP0");
}

//
// cla1Isr6 - CLA1 ISR 6
//
__interrupt void cla1Isr6 ()
{
    asm(" ESTOP0");
}

//
// cla1Isr7 - CLA1 ISR 7
//
__interrupt void cla1Isr7 ()
{
    asm(" ESTOP0");
}

//
// cla1Isr8 - CLA1 ISR 8
//
__interrupt void cla1Isr8 ()
{
    //
    // Acknowledge the end-of-task interrupt for task 8
    //
//    PieCtrlRegs.PIEACK.all = M_INT11;

    //
    // Uncomment to halt debugger and stop here
    //
    asm(" ESTOP0");
}

