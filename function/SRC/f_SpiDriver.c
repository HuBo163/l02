#include"f_SpiDriver.h"


// spi_fifo_init - Initialize SPIA FIFO
//
void spi_fifo_init()
{
    // Initialize SPI FIFO registers

    GPIO_SetupPinOptions(12, GPIO_OUTPUT, GPIO_PUSHPULL);
    GPIO_SetupPinMux(12, GPIO_MUX_CPU1, 0);

    InitSpiaGpio();

    SpiaRegs.SPIFFTX.all = 0xE040;
    SpiaRegs.SPIFFRX.all = 0x405F;
    SpiaRegs.SPIFFRX.all = 0x205F;
    SpiaRegs.SPIFFCT.all = 0x0000;

    //
    // Initialize core SPI registers
    //
    InitSpi();
}



