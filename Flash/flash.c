/*
 * flash.c
 *
 *  Created on: 2021��7��10��
 *      Author: ljl
 */


#include "Flash.h"

#define Bootloader (void(*)(void))0x80000

void Flash_Init(void)
{
     Fapi_StatusType oReturnCheck;
     Uint16 au32DataBuffer[2];
     Uint16 au16DataBuffer[2];
     EALLOW;
     oReturnCheck = Fapi_initializeAPI(F021_CPU0_W0_BASE_ADDRESS, 200);
     if(oReturnCheck != Fapi_Status_Success)
     {
 //        EDIS;
 //        return 1;
 //        Example_Error(oReturnCheck);
     }

     oReturnCheck = Fapi_setActiveFlashBank(Fapi_FlashBank0);
     if(oReturnCheck != Fapi_Status_Success)
     {
 //        EDIS;
 //        return 2;
 //        Example_Error(oReturnCheck);
     }
     EDIS;

     au32DataBuffer[0] = *((Uint32 *)(FLASHB));

     if(au32DataBuffer[0] != 0xAAAAAAAA)
     {
         au16DataBuffer[0] = 0xAAAA;
         au16DataBuffer[1] = 0xAAAA;
         oReturnCheck = Fapi_issueProgrammingCommand((Uint32 *)FLASHB,au16DataBuffer, 2,
         0, 0, Fapi_AutoEccGeneration);
         while (Fapi_checkFsmForReady() != Fapi_Status_FsmReady){}
         if(oReturnCheck != Fapi_Status_Success)
         {
         }
     }
}
void Jump_Bootloader(void)
{
    Fapi_StatusType oReturnCheck;
    EALLOW;

    oReturnCheck = Fapi_issueAsyncCommandWithAddress(Fapi_EraseSector, (uint32 *)FLASHB);
    while(Fapi_checkFsmForReady() != Fapi_Status_FsmReady){}
    EDIS;
    if(oReturnCheck == Fapi_Status_Success)
    {
        (*Bootloader)();
    }
}
