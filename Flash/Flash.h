/*
 * Flash.h
 *
 *  Created on: 2021��7��10��
 *      Author: ljl
 */

#ifndef FLASH_FLASH_H_
#define FLASH_FLASH_H_
#include "F28x_Project.h"
#include "F021_F2837xS_C28x.h"

#define   FLASHA    0x080000  /* on-chip Flash */
#define   FLASHB    0x082000  /* on-chip Flash */
#define   FLASHC    0x084000  /* on-chip Flash */
#define   FLASHD    0x086000  /* on-chip Flash */
#define   FLASHE    0x088000  /* on-chip Flash */
#define   FLASHF    0x090000  /* on-chip Flash */
#define   FLASHG    0x098000  /* on-chip Flash */
#define   FLASHH    0x0A0000  /* on-chip Flash */
#define   FLASHI    0x0A8000  /* on-chip Flash */
#define   FLASHJ    0x0B0000  /* on-chip Flash */
#define   FLASHK    0x0B8000  /* on-chip Flash */
#define   FLASHL    0x0BA000  /* on-chip Flash */
#define   FLASHM    0x0BC000  /* on-chip Flash */
#define   FLASHN    0x0BE000  /* on-chip Flash */


void Flash_Init(void);
void Jump_Bootloader(void);
#endif /* FLASH_FLASH_H_ */
